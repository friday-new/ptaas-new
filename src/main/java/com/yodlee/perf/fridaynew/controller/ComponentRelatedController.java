/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */

package com.yodlee.perf.fridaynew.controller;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.yodlee.perf.fridaynew.configuration.ConfigurationClass;
import com.yodlee.perf.fridaynew.model.Components;
import com.yodlee.perf.fridaynew.model.Configfeaturekey;
import com.yodlee.perf.fridaynew.model.Data;
import com.yodlee.perf.fridaynew.model.Dbupgrade;
import com.yodlee.perf.fridaynew.model.Execmetricsummary;
import com.yodlee.perf.fridaynew.model.Exectracking;
import com.yodlee.perf.fridaynew.model.Gatherer;
import com.yodlee.perf.fridaynew.model.GetDBObject;
import com.yodlee.perf.fridaynew.model.Jmeter;
import com.yodlee.perf.fridaynew.model.Mq;
import com.yodlee.perf.fridaynew.model.Oltp;
import com.yodlee.perf.fridaynew.model.Simulator;
import com.yodlee.perf.fridaynew.model.Taasexectracking;
import com.yodlee.perf.fridaynew.model.Testcaseverdict;
import com.yodlee.perf.fridaynew.model.UpdateFeatureKey;
import com.yodlee.perf.fridaynew.model.request.CreateExecFolder;
import com.yodlee.perf.fridaynew.model.request.UpdatePassword;
import com.yodlee.perf.fridaynew.model.response.ExecFolderRsp;
import com.yodlee.perf.fridaynew.model.response.FolderCreationResponse;
import com.yodlee.perf.fridaynew.model.response.InstanceResponse;
import com.yodlee.perf.fridaynew.service.ServiceClass;
import com.yodlee.perf.fridaynew.service.ServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

//@Api(value = "Ptaas Component Related API Info", description = "Component related API for the Team Reference", produces = "application/json")
@RestController
public class ComponentRelatedController {

	@Autowired
	private ServiceInterface serviceInterface;
	@Autowired
	private ConfigurationClass configurationClass;

	/*
	 * @ApiOperation(value =
	 * "Create Intance Reference API to create an Object for running the test in ptaas"
	 * , notes = "Verify the object in the database, " +
	 * "if the object is not used by any other test it will provide instance reference along with object details like Protocol,IP,Port"
	 * )
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, message = "Object created successfully", response =
	 * InstanceResponse.class),
	 * 
	 * @ApiResponse(code = 400, message = "Given Object is not proper", response =
	 * Map.class),
	 * 
	 * @ApiResponse(code = 403, message = "Given Object is already in use", response
	 * = Map.class),
	 * 
	 * @ApiResponse(code = 404, message = "Given Object is not present", response =
	 * Map.class) })
	 */
	@GetMapping("createInstanceRef/{instanceName}")
	public ResponseEntity<Object> createInstanceRef(@PathVariable String instanceName) {

		String str[] = instanceName.split("_");
		String myComponent = ControllerClass.allObjects.get(str[0]);

		if (myComponent == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the correct input");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}
		if (myComponent.equals("Y")) {
			Components components = serviceInterface.getComponentsObjName(instanceName);
			if (components == null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "No " + str[0] + " Object is present with given Reference");
				return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}
			if (components.getInstanceinuse().equals("Y")) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", str[0] + " Object is already in use");
				return new ResponseEntity<>(map, HttpStatus.FORBIDDEN);
			}
			components.setInstanceinuse("Y");
			components.setInstanceref(instanceName + "_" + System.currentTimeMillis());
			components = serviceInterface.addComponents(components);
			InstanceResponse instanceResponse = new InstanceResponse();
			instanceResponse.setServiceip(components.getServiceip());
			instanceResponse.setInstanceref(components.getInstanceref());
			instanceResponse.setServiceport(components.getServiceport());
			instanceResponse.setVipip(components.getVip());
			instanceResponse.setVipport(components.getViport());
			instanceResponse.setProtocol(components.getProtocol());
			return new ResponseEntity<>(instanceResponse, HttpStatus.OK);
		} else {
			switch (str[0]) {
				case "Oltp": {
					Oltp oltp = serviceInterface.getOltpObjName(instanceName);
					if (oltp == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No OLTP Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else {
						if (oltp.getOltpinuse().equals("Y")) {
							Map<String, String> map = new HashMap<String, String>();
							map.put("message", str[0] + " Object is already in use");
							return new ResponseEntity<>(map, HttpStatus.FORBIDDEN);
						}
						Dbupgrade dbupgrade = serviceInterface.getByDbname(oltp.getOltpschema());
						if (dbupgrade != null) {
							if (dbupgrade.getObjectinuse().equals("Y")) {
								Map<String, String> map = new HashMap<String, String>();
								map.put("message", "DB Upgrade in Process, Please try after some time");
								return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
							}
						}
						oltp.setOltpinuse("Y");
						oltp.setOltpref(instanceName + "_" + System.currentTimeMillis());
						oltp = serviceInterface.addOltp(oltp);
						InstanceResponse instanceResponse = new InstanceResponse();
						instanceResponse.setServiceip(oltp.getOltpipaddr());
						instanceResponse.setInstanceref(oltp.getOltpref());
						instanceResponse.setServiceport(oltp.getOltpport());
						return new ResponseEntity<>(instanceResponse, HttpStatus.OK);
					}
				}
				case "Gatherer":
					Gatherer gatherer = serviceInterface.getGathererObjName(instanceName);
					if (gatherer == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No Gatherer Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else {
						if (gatherer.getGinuse().equals("Y")) {
							Map<String, String> map = new HashMap<String, String>();
							map.put("message", str[0] + " Object is already in use");
							return new ResponseEntity<>(map, HttpStatus.FORBIDDEN);
						}
						gatherer.setGinuse("Y");
						gatherer.setGref(instanceName + "_" + System.currentTimeMillis());
						gatherer = serviceInterface.addGatherer(gatherer);
						InstanceResponse instanceResponse = new InstanceResponse();
						instanceResponse.setServiceip(gatherer.getGipaddr());
						instanceResponse.setInstanceref(gatherer.getGref());
						instanceResponse.setServiceport(gatherer.getGport());
						return new ResponseEntity<>(instanceResponse, HttpStatus.OK);
					}
				case "Mq":
					Mq mq = serviceInterface.getMqObjName(instanceName);
					if (mq == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No MQ Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else {
						if (mq.getMqinuse().equals("Y")) {
							Map<String, String> map = new HashMap<String, String>();
							map.put("message", str[0] + " Object is already in use");
							return new ResponseEntity<>(map, HttpStatus.FORBIDDEN);
						}
						mq.setMqinuse("Y");
						mq.setMqref(instanceName + "_" + System.currentTimeMillis());
						mq = serviceInterface.addMq(mq);
						InstanceResponse instanceResponse = new InstanceResponse();
						instanceResponse.setServiceip(mq.getMqipaddr());
						instanceResponse.setInstanceref(mq.getMqref());
						instanceResponse.setServiceport(mq.getMqport());
						return new ResponseEntity<>(instanceResponse, HttpStatus.OK);
					}
				case "Simulator":
					Simulator simulator = serviceInterface.getSimulatorObjName(instanceName);
					if (simulator == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No Simulator Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else {
						if (simulator.getSimulatorInUse().equals("Y")) {
							Map<String, String> map = new HashMap<String, String>();
							map.put("message", str[0] + " Object is already in use");
							return new ResponseEntity<>(map, HttpStatus.FORBIDDEN);
						}
						simulator.setSimulatorInUse("Y");
						simulator.setSimulatorInstanceRef(instanceName + "_" + System.currentTimeMillis());
						simulator = serviceInterface.addSimulator(simulator);
						InstanceResponse instanceResponse = new InstanceResponse();
						instanceResponse.setServiceip(simulator.getSimulatorIp());
						instanceResponse.setInstanceref(simulator.getSimulatorInstanceRef());
						instanceResponse.setServiceport(simulator.getSimulatorPort());
						instanceResponse.setVipip(simulator.getDockerVip());
						instanceResponse.setVipport(simulator.getDockerVipPort());
						instanceResponse.setProtocol(simulator.getProtocol());
						return new ResponseEntity<>(instanceResponse, HttpStatus.OK);
					}
				default: {
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please provide the correct input");
					return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
				}
			}
		}
	}

	/*
	 * @ApiOperation(value =
	 * "Get Intance Reference API to get the Instance Ref for an object", notes =
	 * "Verify the object in the database, " +
	 * "and returns the Instance Reference value if it.")
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, message =
	 * "Instance Reference returned successfully", response = Map.class),
	 * 
	 * @ApiResponse(code = 400, message = "Given Instance Name is not proper",
	 * response = Map.class),
	 * 
	 * @ApiResponse(code = 404, message = "Given Instance Name is not present",
	 * response = Map.class) })
	 */
	@GetMapping("getInstanceRef/{instanceName}")
	public ResponseEntity<Object> getInstanceRef(@PathVariable String instanceName) {

		String str[] = instanceName.split("_");
		String myComponent = ControllerClass.allObjects.get(str[0]);

		if (myComponent == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the correct input");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}
		if (myComponent.equals("Y")) {
			Components components = serviceInterface.getComponentsObjName(instanceName);
			if (components == null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "No " + str[0] + " Object is present with given Name");
				return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", components.getInstanceref());
			return new ResponseEntity<>(map, HttpStatus.OK);
		} else {
			switch (str[0]) {
				case "Oltp": {
					Oltp oltp = serviceInterface.getOltpObjName(instanceName);
					if (oltp == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Name");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", oltp.getOltpref());
					return new ResponseEntity<>(map, HttpStatus.OK);
				}
				case "Gatherer": {
					Gatherer gatherer = serviceInterface.getGathererObjName(instanceName);
					if (gatherer == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Name");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", gatherer.getGref());
					return new ResponseEntity<>(map, HttpStatus.OK);
				}
				case "Mq": {
					Mq mq = serviceInterface.getMqObjName(instanceName);
					if (mq == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Name");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", mq.getMqref());
					return new ResponseEntity<>(map, HttpStatus.OK);
				}
				case "Simulator": {
					Simulator simulator = serviceInterface.getSimulatorObjName(instanceName);
					if (simulator == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Name");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", simulator.getSimulatorInstanceRef());
					return new ResponseEntity<>(map, HttpStatus.OK);
				}
				default: {
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please provide the correct input");
					return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
				}
			}
		}
	}

	/*
	 * @ApiOperation(value =
	 * "Delete Intance Reference API to Delete an Object after the completion of Test"
	 * , notes = "Verify the object in the database, " +
	 * "and delete the reference of an object if it was created earlier")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, message =
	 * "Object deleted successfully", response = Map.class),
	 * 
	 * @ApiResponse(code = 400, message = "Given Object is not proper", response =
	 * Map.class),
	 * 
	 * @ApiResponse(code = 404, message = "Given Object is not present", response =
	 * Map.class) })
	 */
	@GetMapping("deleteInstanceRef/{instanceRef}")
	public ResponseEntity<Object> deleteInstanceRef(@PathVariable String instanceRef) {

		String str[] = instanceRef.split("_");
		String myComponent = ControllerClass.allObjects.get(str[0]);

		if (myComponent == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the correct input");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}
		if (myComponent.equals("Y")) {
			Components components = serviceInterface.getComponentsObjRef(instanceRef);
			if (components == null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "No " + str[0] + " Object is present with given Reference");
				return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}
			components.setInstanceinuse("N");
			components.setInstanceref(null);

			Decoder decoder = Base64.getDecoder();
			byte[] decryptedUserName = decoder.decode(components.getUsername());
			byte[] decryptedPassword = decoder.decode(components.getPassword());

			String userName = new String(decryptedUserName);
			String passWord = new String(decryptedPassword);

			try {
				Runtime.getRuntime()
						.exec("/backup/Users/caradhya/RootUserFiles/ResiliencyRelatedController/deleteAllResiliency.sh "
								+ userName + " " + components.getServiceip() + " " + passWord);
				Thread.sleep(5000);
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
			components = serviceInterface.addComponents(components);
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", str[0] + " Object deleted successfully");
			return new ResponseEntity<>(map, HttpStatus.OK);
		} else {
			switch (str[0]) {
				case "Oltp": {
					Oltp oltp = serviceInterface.getOltpObjRef(instanceRef);
					if (oltp == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}

					List<UpdateFeatureKey> updateFeatureKeyList = serviceInterface
							.getAllUpdateFeatureKey(oltp.getOltpref());

					if (updateFeatureKeyList.size() > 0 || updateFeatureKeyList != null) {

						RestTemplate restTemplate = new RestTemplate();
						Map<String, String> args = new HashMap<String, String>();
						for (UpdateFeatureKey updateFeatureKey : updateFeatureKeyList) {

							switch (updateFeatureKey.getTableName()) {

								case "PARAM_ACL":
									args.put("keyValue", updateFeatureKey.getValue());
									args.put("keyId", updateFeatureKey.getKeyId().toString());
									restTemplate.postForObject("http://localhost:" + oltp.getPort() + "/updateParamAcl",
											args, String.class);
									break;

								case "PARAM_KEY":
									args.put("keyValue", updateFeatureKey.getValue());
									args.put("keyId", updateFeatureKey.getKeyId().toString());
									restTemplate.postForObject("http://localhost:" + oltp.getPort() + "/updateParamKey",
											args, String.class);
									break;

								case "COB_PARAM":
									args.put("keyValue", updateFeatureKey.getValue());
									args.put("keyId", updateFeatureKey.getKeyId().toString());
									restTemplate.postForObject("http://localhost:" + oltp.getPort() + "/updateCobParam",
											args, String.class);
									break;

								case "COBRAND_ACL_VALUE":
									args.put("keyValue", updateFeatureKey.getValue());
									args.put("keyId", updateFeatureKey.getKeyId().toString());
									restTemplate.postForObject(
											"http://localhost:" + oltp.getPort() + "/updateCobrandAclValue", args,
											String.class);
									break;

								case "ATTRIBUTE_COB_CONFIG":
									args.put("keyValue", updateFeatureKey.getValue());
									args.put("keyId", updateFeatureKey.getKeyId().toString());
									restTemplate.postForObject(
											"http://localhost:" + oltp.getPort() + "/updateAttributeCobConfig", args,
											String.class);
									break;

								default:
									Map<String, String> map = new HashMap<String, String>();
									map.put("message", "Please provide the Correct Table Name");
									return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
							}

							updateFeatureKey.setRevertTime(System.currentTimeMillis());
							serviceInterface.saveUpdateFeatureKey(updateFeatureKey);
						}
					}

					List<Configfeaturekey> configfeaturekeyList = serviceInterface
							.getAllConfigfeaturekey(oltp.getOltpref());

					if (configfeaturekeyList.size() > 0 || configfeaturekeyList != null) {

						RestTemplate restTemplate = new RestTemplate();
						Map<String, String> args = new HashMap<String, String>();

						for (Configfeaturekey configfeaturekey : configfeaturekeyList) {

							args.put("configGroupId", configfeaturekey.getConfiggroupid());
							args.put("configKey", configfeaturekey.getConfigkey());
							args.put("configValue", configfeaturekey.getConfigvalue());
							restTemplate.postForObject("http://localhost:" + oltp.getPort() + "/updateConfigValue",
									args, String.class);
							configfeaturekey.setReverttime(System.currentTimeMillis());
							serviceInterface.saveConfigfeaturekey(configfeaturekey);

						}
					}
					oltp.setOltpref(null);
					oltp.setOltpinuse("N");
					oltp = serviceInterface.addOltp(oltp);
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", str[0] + " Object deleted successfully");
					return new ResponseEntity<>(map, HttpStatus.OK);
				}
				case "Gatherer": {
					Gatherer gatherer = serviceInterface.getGathererObjRef(instanceRef);
					if (gatherer == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Name");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					gatherer.setGref(null);
					gatherer.setGinuse("N");
					gatherer = serviceInterface.addGatherer(gatherer);
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", str[0] + " Object deleted successfully");
					return new ResponseEntity<>(map, HttpStatus.OK);
				}
				case "Mq": {
					Mq mq = serviceInterface.getMqObjRef(instanceRef);
					if (mq == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Name");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					mq.setMqref(null);
					mq.setMqinuse("N");
					mq = serviceInterface.addMq(mq);
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", str[0] + " Object deleted successfully");
					return new ResponseEntity<>(map, HttpStatus.OK);
				}
				case "Simulator": {
					Simulator simulator = serviceInterface.getSimulatorObjRef(instanceRef);
					if (simulator == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Name");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					simulator.setSimulatorInstanceRef(null);
					simulator.setSimulatorInUse("N");
					simulator = serviceInterface.addSimulator(simulator);
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", str[0] + " Object deleted successfully");
					return new ResponseEntity<>(map, HttpStatus.OK);
				}
				default: {
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please provide the correct input");
					return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
				}
			}
		}
	}

	/*
	 * @ApiOperation(value =
	 * "Create Execution Folder API to create the folder in 192.168.84.188 Box",
	 * notes = "Verify input provided with the Standards," +
	 * "and create the folder in 188 and returns the path and url of the location for the current test"
	 * )
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, message = "Folder created Successfully", response =
	 * ExecFolderRsp.class),
	 * 
	 * @ApiResponse(code = 400, message =
	 * "Jmeter/version/portfolio/environment/testcaseid/trackingid is not proper",
	 * response = Map.class) })
	 */
	@PostMapping("createExecFolder")
	public ResponseEntity<Object> createExecFolder(@RequestBody CreateExecFolder createExecFolder) {

		Jmeter jmeter = serviceInterface.getJmeterByname(createExecFolder.getJmeterinstancename());

		if (jmeter == null)
			return new ResponseEntity<>("Given Jmeter is not present", HttpStatus.BAD_REQUEST);

		String regex = "^[a-zA-Z0-9_-]*$";

		if (!(configurationClass.getVersion().contains(createExecFolder.getVersion()))) {
			FolderCreationResponse folderCreationResponse = new FolderCreationResponse();
			folderCreationResponse.setStr("Entered version is not Present, Available Versions displayed in message");
			folderCreationResponse.setMessage(configurationClass.getVersion());
			return new ResponseEntity<>(folderCreationResponse, HttpStatus.BAD_REQUEST);
		}

		if (!(configurationClass.getPortfolio().contains(createExecFolder.getPortfolio()))) {
			FolderCreationResponse folderCreationResponse = new FolderCreationResponse();
			folderCreationResponse
					.setStr("Entered portfolio is not Present, Available Portfolios displayed in message");
			folderCreationResponse.setMessage(configurationClass.getPortfolio());
			return new ResponseEntity<>(folderCreationResponse, HttpStatus.BAD_REQUEST);
		}

		if (!(createExecFolder.getEnvironment().equalsIgnoreCase("cobrand")
				|| createExecFolder.getEnvironment().equalsIgnoreCase("subbrand")))
			return new ResponseEntity<>("Please provide the correct Environment", HttpStatus.BAD_REQUEST);

		if (createExecFolder.getEnvironment().equalsIgnoreCase("cobrand"))
			createExecFolder.setEnvironment("cobrand");
		else
			createExecFolder.setEnvironment("subbrand");

		if (!createExecFolder.getTestcaseid().matches(regex))
			return new ResponseEntity<>("TestCaseIDs don't support special charcters, Only - and _ are allowed",
					HttpStatus.BAD_REQUEST);

		Exectracking dummy = null;
		try {
			dummy = serviceInterface.getExectracking(createExecFolder.getUniqueId());
		} catch (NoSuchElementException e) {

		}

		if (dummy != null)
			return new ResponseEntity<>("Please provide the unique Id for the Tracking purpose",
					HttpStatus.BAD_REQUEST);

		ServiceClass serviceClass = new ServiceClass();

		String temp = serviceClass.executeShellCommand("ls -ltr /backup/projects/" + createExecFolder.getVersion() + "/"
				+ createExecFolder.getPortfolio() + "/" + createExecFolder.getTestcaseid());

		if (temp.length() == 0) {
			String createdPath = serviceClass.executeShellCommand(
					"sh /backup/Users/caradhya/RootUserFiles/create.sh " + createExecFolder.getVersion() + "/"
							+ createExecFolder.getPortfolio() + "/" + createExecFolder.getTestcaseid());
			createdPath = createdPath.replaceAll("\\r\\n|\\r|\\n", "");
			System.out.println(createdPath);
		}

		String path = serviceClass
				.executeShellCommand("sh /backup/Users/caradhya/RootUserFiles/ExecFolder.sh /backup/projects/"
						+ createExecFolder.getVersion() + "/" + createExecFolder.getPortfolio() + "/"
						+ createExecFolder.getTestcaseid() + "/" + createExecFolder.getEnvironment() + " "
						+ createExecFolder.getUser());

		path = path.replaceAll("\\r\\n|\\r|\\n", "");

		String finalstring = path.replaceFirst("/backup/", "");

		ExecFolderRsp execFolderRsp = new ExecFolderRsp();
		execFolderRsp.setFolderPath(path);
		execFolderRsp.setTestResultsUrl("http://192.168.84.188/" + finalstring + "/TestResults.txt");

		Exectracking exectracking = new Exectracking();
		exectracking.setExecfolder(path);
		exectracking.setExectrackingid(createExecFolder.getUniqueId());
		exectracking.setIsjtlcopied(0);
		exectracking.setJmeterref(jmeter.getInstancename());
		exectracking.setJtllocation(createExecFolder.getJtlfilepath());
		exectracking.setCreated(System.currentTimeMillis());
		exectracking.setTestcaseid(createExecFolder.getTestcaseid());
		serviceInterface.addExectracking(exectracking);

		return new ResponseEntity<>(execFolderRsp, HttpStatus.OK);
	}

	/*
	 * @ApiOperation(value =
	 * "Get Execution Folder API returns the Path & URL of the Execution based on Execution tracking ID"
	 * , notes = "Verify the Execution " + "Tracking and return the Path and URL")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, message = "200 OK", response
	 * = ExecFolderRsp.class) })
	 */
	@GetMapping("getExecFolder/{execTrackingId}")
	public ExecFolderRsp getExecFolder(@PathVariable String execTrackingId) {

		ExecFolderRsp execFolderRsp = new ExecFolderRsp();

		Exectracking exectracking = null;
		try {
			exectracking = serviceInterface.getExectracking(execTrackingId);
		} catch (NoSuchElementException e) {
			execFolderRsp.setFolderPath("No Data Found");
			execFolderRsp.setTestResultsUrl("No Data Found");
			return execFolderRsp;
		}

		List<Execmetricsummary> execmetricsummaryList = serviceInterface
				.getExecmetricsummarybyexectracking(execTrackingId);
		if (execmetricsummaryList.size() == 0 || execmetricsummaryList == null)
			execFolderRsp.setVerdict("Fail");
		else {
			Execmetricsummary execmetricsummary = execmetricsummaryList.get(0);
			execFolderRsp.setVerdict(execmetricsummary.getFinalverdict());
		}

		if (exectracking.getIsjtlcopied() == 0) {

			Jmeter jmeter = serviceInterface.getJmeterByname(exectracking.getJmeterref());

			try {
				Runtime.getRuntime()
						.exec("/backup/Users/caradhya/RootUserFiles/CopyFile.sh " + jmeter.getUsername() + " "
								+ jmeter.getServiceip() + " " + jmeter.getPassword() + " "
								+ exectracking.getJtllocation() + " " + exectracking.getExecfolder() + "/Reports");

			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				TimeUnit.SECONDS.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			Process process = null;
			String art = null, rc = null, st = null, et = null, completeMainString = null, ec = null;

			try {
				process = Runtime.getRuntime().exec("sh /backup/Users/caradhya/RootUserFiles/GenerateJtltoHtml.sh "
						+ exectracking.getExecfolder() + " " + execTrackingId);

				InputStream inputStream = process.getInputStream();
				int i = 0;
				StringBuffer stringBuffer = new StringBuffer();

				while ((i = inputStream.read()) != -1)
					stringBuffer.append((char) i);
				completeMainString = stringBuffer.toString();

				String[] temp = completeMainString.split("\n");

				for (String string : temp) {
					if (string.regionMatches(0, "AvgRspTime", 0, 10))
						art = string.split("=")[1];
					if (string.regionMatches(0, "RequestCount", 0, 12))
						rc = string.split("=")[1];
					if (string.regionMatches(0, "startTime", 0, 9))
						st = string.split("=")[1];
					if (string.regionMatches(0, "endTime", 0, 7))
						et = string.split("=")[1];
					if (string.regionMatches(0, "ErrorCount", 0, 10))
						ec = string.split("=")[1];
				}

				Double avgRspTime = Double.parseDouble(art.replaceAll("\\r\\n|\\r|\\n", ""));
				Long requestCount = Long.parseLong(rc.replaceAll("\\r\\n|\\r|\\n", ""));
				Long startTime = Long.parseLong(st.replaceAll("\\r\\n|\\r|\\n", ""));
				Long endTime = Long.parseLong(et.replaceAll("\\r\\n|\\r|\\n", ""));
				Long errorCount = Long.parseLong(ec.replaceAll("\\r\\n|\\r|\\n", ""));

				DecimalFormat decimalFormat = new DecimalFormat("#.##");
				Double errorPercentage = (errorCount.doubleValue() / requestCount.doubleValue()) * 100;
				String errorFormat = decimalFormat.format(errorPercentage) + " %";
				Double tps = requestCount.doubleValue() / ((endTime.doubleValue() - startTime.doubleValue()) / 1000);

				avgRspTime = Double.valueOf(decimalFormat.format(avgRspTime));
				tps = Double.valueOf(decimalFormat.format(tps));

				Map<String, String> map = new HashMap<String, String>();

				map.put("avgRspTime", avgRspTime.toString());
				map.put("tps", tps.toString());

				Map<String, Data> result = new HashMap<String, Data>();

				ObjectMapper objectMapper = new ObjectMapper();
				String json = objectMapper.writeValueAsString(map);

				Execmetricsummary tempExecmetricsummaryForTestCaseVerdict = execmetricsummaryList.get(0);
				String branch = tempExecmetricsummaryForTestCaseVerdict.getBranch();

				Testcaseverdict testcaseverdict = null;

				String[] objectNameArrays = tempExecmetricsummaryForTestCaseVerdict.getObjectname().split("_");

				if (branch != null)
					testcaseverdict = serviceInterface.getTestcaseverdict(exectracking.getTestcaseid(),
							objectNameArrays[0], 0L, 1L, tempExecmetricsummaryForTestCaseVerdict.getBranch());
				else
					testcaseverdict = serviceInterface.getTestcaseverdict(exectracking.getTestcaseid(),
							tempExecmetricsummaryForTestCaseVerdict.getObjectname(), 0L, 1L, "npr");

				if (testcaseverdict == null) {
					result.put("avgRspTime", new Data(avgRspTime.toString() + " mS", "Fail"));
					result.put("tps", new Data(tps.toString(), "Fail"));
				} else {
					Gson gson = new Gson();
					@SuppressWarnings("unchecked")
					Map<String, List<String>> mainTpsfromDB = gson.fromJson(testcaseverdict.getTpsverdict(), Map.class);

					ServiceClass serviceClass = new ServiceClass();

					result.put("avgRspTime", new Data(avgRspTime.toString() + " mS",
							serviceClass.compareArrayNumbers(avgRspTime.toString(), mainTpsfromDB.get("avgRspTime"))));
					result.put("tps", new Data(tps.toString(),
							serviceClass.compareArrayNumbers(tps.toString(), mainTpsfromDB.get("tps"))));
				}

				for (Execmetricsummary tempExecmetricsummary : execmetricsummaryList) {
					tempExecmetricsummary.setTps(json);
					tempExecmetricsummary.setTpsverdict(objectMapper.writeValueAsString(result));
					tempExecmetricsummary.setPercentageerror(errorFormat);
					serviceInterface.addExecmetricsummary(tempExecmetricsummary);
				}

				RestTemplate restTemplate = new RestTemplate();
				restTemplate.getForObject("http://localhost:8181/generateTestReport/" + execTrackingId, String.class);

			} catch (Exception e2) {
				e2.printStackTrace();
			}

			try {
				Runtime.getRuntime()
						.exec("/backup/Users/caradhya/RootUserFiles/DeleteJtlAfterExecution.sh " + jmeter.getUsername()
								+ " " + jmeter.getServiceip() + " " + jmeter.getPassword() + " "
								+ exectracking.getJtllocation());

			} catch (IOException e) {
				e.printStackTrace();
			}

			execFolderRsp.setFolderPath(exectracking.getExecfolder());
			execFolderRsp.setTestResultsUrl("http://192.168.84.188/"
					+ exectracking.getExecfolder().replaceFirst("/backup/", "") + "/TestResults.txt");
			execFolderRsp.setMetricSummary("http://192.168.84.188/"
					+ exectracking.getExecfolder().replaceFirst("/backup/", "") + "/TestResults.html");
			exectracking.setIsjtlcopied(1);
			serviceInterface.addExectracking(exectracking);

		} else {

			execFolderRsp.setFolderPath(exectracking.getExecfolder());
			execFolderRsp.setTestResultsUrl("http://192.168.84.188/"
					+ exectracking.getExecfolder().replaceFirst("/backup/", "") + "/TestResults.txt");
			execFolderRsp.setMetricSummary("http://192.168.84.188/"
					+ exectracking.getExecfolder().replaceFirst("/backup/", "") + "/TestResults.html");
		}
		return execFolderRsp;
	}

	@PostMapping("getDB")
	public ResponseEntity<Object> getDB(@RequestBody GetDBObject getDBObject) {

		String user;
		if (getDBObject.getUser() == null)
			user = "pal";
		else
			user = getDBObject.getUser().toLowerCase();

		List<Oltp> oltpList = serviceInterface.getOltpforTaas(getDBObject.getIp(), getDBObject.getPort(),
				getDBObject.getProjectName(), user);

		if (oltpList.size() != 0) {

			Oltp oltp = oltpList.get(0);
			oltp.setIsassigned("Y");
			oltp = serviceInterface.addOltp(oltp);
			getDBObject.setInstanceName(oltp.getOltpname());

			Taasexectracking taasexectracking = new Taasexectracking();
			taasexectracking.setCreated(System.currentTimeMillis());
			taasexectracking.setInstancename(oltp.getOltpname());
			taasexectracking.setIsdeleted("N");
			taasexectracking.setOltpip(oltp.getOltpipaddr());
			taasexectracking.setOltpport(oltp.getPort());
			taasexectracking.setOltpusername(oltp.getOltpusername());
			taasexectracking.setProjectname(oltp.getProjectname());
			serviceInterface.saveTaasexecTracking(taasexectracking);

			return new ResponseEntity<>(getDBObject, HttpStatus.OK);
		} else {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "No oltp Object found");

			return new ResponseEntity<Object>(map, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("ungetDB/{oltpname}")
	public ResponseEntity<Object> ungetDB(@PathVariable String oltpname) {

		Oltp oltp = serviceInterface.getOltpObjName(oltpname);
		if (oltp != null) {
			oltp.setIsassigned("N");
			oltp = serviceInterface.addOltp(oltp);

			Taasexectracking taasexectracking = serviceInterface.getTaasexectracking(oltpname);
			if (taasexectracking == null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "No Execution Tracking Object found");
				return new ResponseEntity<Object>(map, HttpStatus.NOT_FOUND);
			}
			taasexectracking.setDeletiontime(System.currentTimeMillis());
			taasexectracking.setIsdeleted("Y");

			serviceInterface.saveTaasexecTracking(taasexectracking);

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "No oltp Object found");

			return new ResponseEntity<Object>(map, HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("updatePassword")
	public ResponseEntity<Object> updatePassword(@RequestBody UpdatePassword updatePassword) {

		String userName = updatePassword.getUserName();
		String passWord = updatePassword.getPassWord();
		String confirmPassWord = updatePassword.getConfirmPassWord();

		Map<String, String> map = new HashMap<String, String>();

		if (!passWord.equals(confirmPassWord)) {
			map.put("message", "passwords are incorrect");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}

		Encoder encoder = Base64.getEncoder();
		String encryptedUserName = encoder.encodeToString(userName.getBytes());
		String encryptedPassword = encoder.encodeToString(confirmPassWord.getBytes());

		List<Components> componentsList = serviceInterface.updateUserName(encryptedUserName);
		List<Components> newComponentsList = new ArrayList<Components>();

		if (componentsList != null) {

			for (Components components : componentsList) {
				components.setPassword(encryptedPassword);
				newComponentsList.add(components);
			}

			for (Components components : newComponentsList) {
				serviceInterface.addComponents(components);
			}
		}
		map.put("message", "PassWord got updated to the " + updatePassword.getUserName());

		return new ResponseEntity<Object>(map, HttpStatus.CREATED);
	}

	@PostMapping("addUserNamePassword")
	public ResponseEntity<Object> addUserNamePassword(@RequestBody UpdatePassword updatePassword) {

		String userName = updatePassword.getUserName();
		String passWord = updatePassword.getPassWord();
		String confirmPassWord = updatePassword.getConfirmPassWord();

		Map<String, String> map = new HashMap<String, String>();

		if (!passWord.equals(confirmPassWord)) {
			map.put("message", "passwords are incorrect");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}

		Components components = serviceInterface.getComponentsObjName(updatePassword.getInstanceName());

		if (components != null) {
			Encoder encoder = Base64.getEncoder();
			String encryptedUserName = encoder.encodeToString(userName.getBytes());
			String encryptedPassword = encoder.encodeToString(confirmPassWord.getBytes());
			components.setUsername(encryptedUserName);
			components.setPassword(encryptedPassword);
			serviceInterface.addComponents(components);
		}

		map.put("message", "UserName and PassWord got updated to the " + updatePassword.getUserName());

		return new ResponseEntity<Object>(map, HttpStatus.CREATED);
	}

}
