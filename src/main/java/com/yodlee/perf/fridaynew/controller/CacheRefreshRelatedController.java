/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */

package com.yodlee.perf.fridaynew.controller;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.yodlee.perf.fridaynew.model.CacheRefreshDetails;
import com.yodlee.perf.fridaynew.model.ChangeGathererModel;
import com.yodlee.perf.fridaynew.model.Components;
import com.yodlee.perf.fridaynew.model.Gatherer;
import com.yodlee.perf.fridaynew.model.Mq;
import com.yodlee.perf.fridaynew.model.Oltp;
import com.yodlee.perf.fridaynew.model.UpdateCacheRefreshModel;
import com.yodlee.perf.fridaynew.model.request.ZabbixReportReq;
import com.yodlee.perf.fridaynew.service.ServiceClass;
import com.yodlee.perf.fridaynew.service.ServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CacheRefreshRelatedController {

	@Autowired
	private ServiceInterface serviceInterface;

	@GetMapping("copyDefalultGathererFile/{object}")
	public ResponseEntity<Object> copyDefalultGathererFile(@PathVariable String object) {

		String str[] = object.split("_");

		if (!str[0].equals("Gatherer"))
			return new ResponseEntity<>("Please provide correct input.", HttpStatus.BAD_REQUEST);
		else {

			Gatherer gatherer = serviceInterface.getGathererObjRef(object);
			if (gatherer == null)
				return new ResponseEntity<>("Given Gatherer does't exists.", HttpStatus.BAD_REQUEST);
			else {
				try {
					Runtime.getRuntime()
							.exec("/backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/copyDefalultGathererFile.sh "
									+ gatherer.getGuser() + " " + gatherer.getGipaddr() + " " + gatherer.getGpasswd()
									+ " /opt/ctier/dap/conf");
				} catch (IOException e) {
					e.printStackTrace();
				}

				return new ResponseEntity<>("Defalut config file is copied", HttpStatus.CREATED);
			}
		}

	}

	@PostMapping("updateGathererFile")
	public ResponseEntity<Object> updateGathererFile(@RequestBody ChangeGathererModel changeGathererModel) {

		String str[] = changeGathererModel.getObject().split("_");

		if (!str[0].equals("Gatherer"))
			return new ResponseEntity<>("Please provide correct input.", HttpStatus.BAD_REQUEST);
		else {
			Gatherer gatherer = serviceInterface.getGathererObjRef(changeGathererModel.getObject());

			String fileName;
			String folder;
			if (changeGathererModel.getFilename() == null) {
				fileName = "Config1.properties";
				folder = "/opt/ctier/dap/conf/";
			} else {
				fileName = changeGathererModel.getFilename();
				folder = changeGathererModel.getFolderPath();
			}
			if (gatherer == null)
				return new ResponseEntity<>("Given Gatherer does't exists.", HttpStatus.BAD_REQUEST);
			else {
				Process p = null;
				int i;

				switch (changeGathererModel.getKey()) {
					case "Bank_Number_of_Days":
					case "Card_Number_of_Days":
					case "Inv_Number_of_Days":
						if (changeGathererModel.getValue() == null) {
							Oltp oltp = serviceInterface.getOltpObjRef(changeGathererModel.getOltpRef());
							if (oltp == null) {
								return new ResponseEntity<>("Given OLTP doesn't exist", HttpStatus.BAD_REQUEST);
							}

							RestTemplate restTemplate = new RestTemplate();

							String url = "http://localhost:" + oltp.getPort() + "/getCacheRefreshDetails/"
									+ changeGathererModel.getLoginName() + "/" + changeGathererModel.getSiteId();

							CacheRefreshDetails cacheRefreshDetails = restTemplate.getForObject(url,
									CacheRefreshDetails.class);

							String dateString = cacheRefreshDetails.getStartDate();

							SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");

							DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

							String currentDate = dateFormat.format(new Date());

							try {
								Date date1 = myFormat.parse(dateString);
								Date date2 = myFormat.parse(currentDate);
								long diff = date2.getTime() - date1.getTime();
								Long longValue = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
								changeGathererModel.setValue(longValue.toString());
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
				}

				try {
					p = Runtime.getRuntime()
							.exec("/backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/checkgathererproperty.sh "
									+ gatherer.getGuser() + " " + gatherer.getGipaddr() + " " + gatherer.getGpasswd()
									+ " " + changeGathererModel.getKey() + " " + folder + " " + fileName);

					InputStream is = p.getInputStream();
					int j = 0;
					StringBuffer sb = new StringBuffer();
					String temp = null;
					while ((i = is.read()) != -1)
						sb.append((char) i);
					temp = sb.toString();
					String myString = changeGathererModel.getKey();
					String[] finalString = null;
					String[] temp1 = temp.split("\n");
					for (String dummy : temp1) {
						if (dummy.contains(myString)) {
							finalString = dummy.split("=");
							j = finalString.length;
							if (j == 2)
								break;
						}
					}

					if (j != 2)
						return new ResponseEntity<>("Please provide the valid key", HttpStatus.BAD_REQUEST);
					else {
						Runtime.getRuntime()
								.exec("/backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/updategatherer.sh "
										+ gatherer.getGuser() + " " + gatherer.getGipaddr() + " "
										+ gatherer.getGpasswd() + " " + changeGathererModel.getKey() + " "
										+ changeGathererModel.getValue() + " " + folder + " " + fileName);
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return new ResponseEntity<>("Modification is completed", HttpStatus.OK);
		}
	}

	@GetMapping("getMqstatus/{object}")
	public ResponseEntity<Object> getMqstatus(@PathVariable String object) {
		String[] str = object.split("_");

		Map<String, Integer> queuestatus = new HashMap<String, Integer>();

		if (!str[0].equals("Mq")) {
			return new ResponseEntity<>("Please provide correct input.", HttpStatus.BAD_REQUEST);
		}
		Mq mq = serviceInterface.getMqObjRef(object);
		if (mq == null) {
			return new ResponseEntity<>("Given Mq doesn't exist", HttpStatus.BAD_REQUEST);
		}

		String[] allqueues = { "ALERTS", "ALERTS_FAST_QUEUE", "CACHEREQUEST", "CACHERESPONSE", "INSTANTREQUEST",
				"INSTANTRESPONSE", "MDBDeadLetterQueue", "MFAREQUEST", "MFA_APP_REQUEST", "MFA_GATH_RESPONSE",
				"MFAREQUEST_NEW", "TOKENREQUEST", "MFARESPONSE", "PAYMENTREQUEST", "PAYMENTRESPONSE", "YTASK",
				"TANDEMQ", "WEBHOOKS", "WEBHOOKS_MFA", "MFA_GATHERER_RESPONSE" };

		String combinedString = null;

		for (String dummy : allqueues) {
			if (combinedString == null)
				combinedString = dummy;
			else
				combinedString = combinedString + " " + dummy;
		}
		ServiceClass serviceClass = new ServiceClass();
		String mainString = serviceClass
				.executeShellCommand("/backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/Mqmoniter.sh "
						+ mq.getMqipaddr() + " " + mq.getMqname() + " " + combinedString);

		String stringarr[] = mainString.split("\n");

		for (String queues : allqueues) {

			for (int i = 0; i < stringarr.length; i++) {
				if (stringarr[i].regionMatches(0, queues, 0, queues.length())) {
					queuestatus.put(queues, Integer.valueOf(Integer.parseInt(
							stringarr[i].replaceAll("\\r\\n|\\r|\\n", "").replaceAll(queues, "").replace("=", ""))));
					break;
				}
				queuestatus.put(queues, Integer.valueOf(-1));
			}
		}

		return new ResponseEntity<>(queuestatus, HttpStatus.OK);
	}

	@GetMapping("clearMq/{object}")
	public ResponseEntity<Object> clearMq(@PathVariable String object) {
		String[] str = object.split("_");

		if (!str[0].equals("Mq")) {
			return new ResponseEntity<>("Please provide correct input.", HttpStatus.BAD_REQUEST);
		}
		Mq mq = serviceInterface.getMqObjRef(object);
		if (mq == null) {
			return new ResponseEntity<>("Given Mq doesn't exist", HttpStatus.BAD_REQUEST);
		}

		String[] allqueues = { "CACHEREQUEST", "CACHERESPONSE", "INSTANTREQUEST", "INSTANTRESPONSE",
				"MDBDeadLetterQueue", "MFAREQUEST", "MFA_APP_REQUEST", "MFA_GATH_RESPONSE", "MFAREQUEST_NEW",
				"TOKENREQUEST", "MFARESPONSE", "PAYMENTREQUEST", "PAYMENTRESPONSE", "YTASK", "TANDEMQ",
				"MFA_GATHERER_RESPONSE" };

		String combinedString = null;

		for (String dummy : allqueues) {
			if (combinedString == null)
				combinedString = dummy;
			else
				combinedString = combinedString + " " + dummy;
		}
		ServiceClass serviceClass = new ServiceClass();
		String mainString = serviceClass
				.executeShellCommand("/backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/clearMq.sh "
						+ mq.getMqipaddr() + " " + mq.getMqname() + " " + combinedString);

		String stringarr[] = mainString.split("\n");

		for (int i = 0; i < stringarr.length; i++) {
			if (stringarr[i].regionMatches(0, "done", 0, 4))
				break;
		}

		return new ResponseEntity<>("done", HttpStatus.OK);
	}

	@PostMapping("getTelnetStats")
	public ResponseEntity<Object> getTelnetStats(@RequestBody ZabbixReportReq zabbixReportReq) {

		String[] str = zabbixReportReq.getInstanceRef().split("_");
		Components components = null;

		switch (str[0]) {

			case "Cacheserver":
				components = serviceInterface.getComponentsObjRef(zabbixReportReq.getInstanceRef());
				if (components == null) {
					return new ResponseEntity<>("Given Cacheserver doesn't exist", HttpStatus.BAD_REQUEST);
				}
				break;

			case "Dbfiler":
				components = serviceInterface.getComponentsObjRef(zabbixReportReq.getInstanceRef());
				if (components == null) {
					return new ResponseEntity<>("Given DBFiler doesn't exist", HttpStatus.BAD_REQUEST);
				}
				break;

			default:
				return new ResponseEntity<>("Given Object doesn't exist", HttpStatus.BAD_REQUEST);
		}

		ServiceClass serviceClass = new ServiceClass();

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(components.getUsername());
		byte[] decryptedPassword = decoder.decode(components.getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		String string = serviceClass.collectTelnetStats(userName, components.getServiceip(), passWord,
				components.getControlport(), components.getInstancename(), zabbixReportReq.getDestination());

		if (!string.equals("Completed")) {
			return new ResponseEntity<>("Couldn't update the Telnet file", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>("Updated the Telnet file", HttpStatus.CREATED);
	}

	@PostMapping("collectServerStats")
	public ResponseEntity<Object> collectServerStats(@RequestBody ZabbixReportReq zabbixReportReq) {

		String[] str = zabbixReportReq.getOltpRef().split("_");

		if (!str[0].equals("Oltp")) {
			return new ResponseEntity<>("Please provide correct input.", HttpStatus.BAD_REQUEST);
		}
		Oltp oltp = serviceInterface.getOltpObjRef(zabbixReportReq.getOltpRef());
		if (oltp == null) {
			return new ResponseEntity<>("Given OLTP doesn't exist", HttpStatus.BAD_REQUEST);
		}

		Components components = serviceInterface.getComponentsObjRef(zabbixReportReq.getInstanceRef());
		if (components == null) {
			return new ResponseEntity<>("Given Instance doesn't exist", HttpStatus.BAD_REQUEST);
		}

		ServiceClass serviceClass = new ServiceClass();
		String mainString = serviceClass
				.executeShellCommand("/backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/collectServerstats.sh "
						+ oltp.getOltpusername() + " " + oltp.getOltpschema() + " " + oltp.getOltppasswd());

		serviceClass.reportUpdate(mainString,
				zabbixReportReq.getDestination() + "/Reports/" + components.getInstancename() + "_ServerStats.txt");

		return new ResponseEntity<>("collected the Server Stats", HttpStatus.OK);
	}

	@PostMapping("sdgOffCacheRefresh")
	public ResponseEntity<Object> sdgOffCacheRefresh(@RequestBody UpdateCacheRefreshModel updateCacheRefreshModel) {
		Map<String, String> map = new HashMap<String, String>();
		if (updateCacheRefreshModel.getInstanceRef() == null || updateCacheRefreshModel.getUserNamePattern() == null) {
			map.put("message", "Please provide correct input.");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}

		String str[] = updateCacheRefreshModel.getInstanceRef().split("_");
		if (!str[0].equals("Oltp")) {
			map.put("message", "Please provide correct input.");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}

		Oltp oltp = serviceInterface.getOltpObjRef(updateCacheRefreshModel.getInstanceRef());
		if (oltp == null) {
			map.put("message", "Give OLTP doesn't exist");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}

		Process process = null;
		String mainString = null;

		try {
			process = Runtime.getRuntime()
					.exec("sh /backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/SDGOffAllUsers.sh "
							+ oltp.getOltpusername() + " " + oltp.getOltpschema() + " " + oltp.getOltppasswd() + " "
							+ updateCacheRefreshModel.getUserNamePattern());
			InputStream inputStream = process.getInputStream();

			int i = 0;
			StringBuffer stringBuffer = new StringBuffer();

			while ((i = inputStream.read()) != -1)
				stringBuffer.append((char) i);
			mainString = stringBuffer.toString();

		} catch (IOException e) {
			e.printStackTrace();
		}

		if (mainString.equals("True"))
			System.out.println(mainString);

		map.put("message", "Users are updated");
		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	@PostMapping("sdgOnCacheRefresh")
	public ResponseEntity<Object> sdgOnCacheRefresh(@RequestBody UpdateCacheRefreshModel updateCacheRefreshModel) {
		Map<String, String> map = new HashMap<String, String>();
		if (updateCacheRefreshModel.getInstanceRef() == null || updateCacheRefreshModel.getUserNamePattern() == null) {
			map.put("message", "Please provide correct input.");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}

		String str[] = updateCacheRefreshModel.getInstanceRef().split("_");
		if (!str[0].equals("Oltp")) {
			map.put("message", "Please provide correct input.");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}

		Oltp oltp = serviceInterface.getOltpObjRef(updateCacheRefreshModel.getInstanceRef());
		if (oltp == null) {
			map.put("message", "Give OLTP doesn't exist");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}

		Process process = null;
		String mainString = null;

		try {
			process = Runtime.getRuntime()
					.exec("sh /backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/SDGOnAllUsers.sh "
							+ oltp.getOltpusername() + " " + oltp.getOltpschema() + " " + oltp.getOltppasswd() + " "
							+ updateCacheRefreshModel.getUserNamePattern());
			InputStream inputStream = process.getInputStream();

			int i = 0;
			StringBuffer stringBuffer = new StringBuffer();

			while ((i = inputStream.read()) != -1)
				stringBuffer.append((char) i);
			mainString = stringBuffer.toString();

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(mainString);

		map.put("message", "Users are updated");
		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	@PostMapping("declobCacheRefresh")
	public ResponseEntity<Object> declobCacheRefresh(@RequestBody UpdateCacheRefreshModel updateCacheRefreshModel) {
		Map<String, String> map = new HashMap<String, String>();
		if (updateCacheRefreshModel.getInstanceRef() == null || updateCacheRefreshModel.getUserNamePattern() == null) {
			map.put("message", "Please provide correct input.");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}

		return null;
	}

	@PostMapping("purgeGathererLogs")
	public ResponseEntity<Object> purgeGathererLogs(@RequestBody Map<String, String> map) {
		Map<String, String> responseMap = new HashMap<String, String>();

		String folderPath = map.get("folderPath");
		String folderName = map.get("folderName");

		if (folderPath == null || folderName == null || map.get("instanceRef") == null) {
			responseMap.put("message", "Please provide the proper Input");
			return new ResponseEntity<>(responseMap, HttpStatus.BAD_REQUEST);
		}

		Gatherer gatherer = serviceInterface.getGathererObjRef(map.get("instanceRef"));

		if (gatherer == null) {
			responseMap.put("message", "No Gatherer Object is present with the given refrence");
			return new ResponseEntity<>(responseMap, HttpStatus.BAD_REQUEST);
		}

		try {
			Runtime.getRuntime()
					.exec("/backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/clearGathererLogs.sh "
							+ gatherer.getGuser() + " " + gatherer.getGipaddr() + " " + gatherer.getGpasswd() + " "
							+ folderPath + " " + folderName);
		} catch (IOException e) {
			e.printStackTrace();
		}

		responseMap.put("message", "Gatherer Logs deleted.");

		return new ResponseEntity<>(responseMap, HttpStatus.OK);
	}

	@GetMapping("getCacheRefreshDetails/{oltpRef}/{loginName}/{siteId}")
	public ResponseEntity<Object> getCacheRefreshDetails(@PathVariable String oltpRef, @PathVariable String loginName,
			@PathVariable Long siteId) {

		Oltp oltp = serviceInterface.getOltpObjRef(oltpRef);
		if (oltp == null) {
			return new ResponseEntity<>("Give OLTP doesn't exist", HttpStatus.BAD_REQUEST);
		}

		RestTemplate restTemplate = new RestTemplate();

		String url = "http://localhost:" + oltp.getPort() + "/getCacheRefreshDetails/" + loginName + "/" + siteId;

		CacheRefreshDetails cacheRefreshDetails = restTemplate.getForObject(url, CacheRefreshDetails.class);

		String string = cacheRefreshDetails.getStartDate();

		String arr[] = string.split("-");

		String finalDate = arr[2] + "/" + arr[1] + "/" + arr[0];

		cacheRefreshDetails.setStartDate(finalDate);

		return new ResponseEntity<Object>(cacheRefreshDetails, HttpStatus.OK);
	}

	@GetMapping("updateServerStats/{oltpRef}/{serverStatsId1}/{serverStatsId2}/{cobrandId}")
	public ResponseEntity<Object> updateServerStats(@PathVariable String oltpRef, @PathVariable Long serverStatsId1,
			@PathVariable Long serverStatsId2, @PathVariable Long cobrandId) {

		Oltp oltp = serviceInterface.getOltpObjRef(oltpRef);
		if (oltp == null) {
			return new ResponseEntity<>("Give OLTP doesn't exist", HttpStatus.BAD_REQUEST);
		}

		RestTemplate restTemplate = new RestTemplate();

		String url = "http://localhost:" + oltp.getPort() + "/updateServerStats/" + serverStatsId1 + "/"
				+ serverStatsId2 + "/" + cobrandId;

		Integer integer = restTemplate.getForObject(url, Integer.class);

		return new ResponseEntity<Object>(integer, HttpStatus.OK);
	}

	@GetMapping("loadMQ/{object}")
	public ResponseEntity<Object> loadMQ(@PathVariable String object) {

		String[] str = object.split("_");

		if (!str[0].equals("Mq")) {
			return new ResponseEntity<>("Please provide correct input.", HttpStatus.BAD_REQUEST);
		}

		Mq mq = serviceInterface.getMqObjRef(object);
		if (mq == null) {
			return new ResponseEntity<>("Given Mq doesn't exist", HttpStatus.BAD_REQUEST);
		}

		ServiceClass serviceClass = new ServiceClass();
		String mainString = serviceClass
				.executeShellCommand("/backup/Users/caradhya/RootUserFiles/loadMQRequests.sh mqm " + mq.getMqipaddr()
						+ " mqm " + mq.getMqname());

		String[] myString = mainString.split("\n");

		boolean completed = false;

		for (String string : myString) {
			if (string.regionMatches(0, "done", 0, 4))
				completed = true;
			else
				completed = false;

			if (completed)
				break;

		}

		return new ResponseEntity<>(completed, HttpStatus.OK);
	}

	@PostMapping("writeMQData")
	public String writeMQData(@RequestBody Map<String, String> map) {

		String path = map.get("folderpath");
		String queueName = map.get("queueName");
		String curDepth = map.get("curDepth");

		ServiceClass serviceClass = new ServiceClass();

		serviceClass.reportUpdate(queueName + "," + curDepth, path + "/Reports/MqData.txt");

		return queueName + "," + curDepth + "," + path;
	}

}
