/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */

package com.yodlee.perf.fridaynew.controller;

import java.io.IOException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.Map;

import com.yodlee.perf.fridaynew.model.Components;
import com.yodlee.perf.fridaynew.model.Gatherer;
import com.yodlee.perf.fridaynew.model.Mq;
import com.yodlee.perf.fridaynew.model.Oltp;
import com.yodlee.perf.fridaynew.model.Simulator;
import com.yodlee.perf.fridaynew.model.request.ResiliencyRequest;
import com.yodlee.perf.fridaynew.service.ServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResiliencyRelatedController {

	@Autowired
	private ServiceInterface serviceInterface;

	@PostMapping("addIpPacketLoss")
	public ResponseEntity<Object> addIpPacketLoss(@RequestBody ResiliencyRequest resiliencyRequest) {

		String sourceInstanceRef = resiliencyRequest.getSourceInstanceRef();
		String destinationInstanceRef = resiliencyRequest.getDestinationInstanceRef();

		String sourceInstanceRefStr[] = sourceInstanceRef.split("_");
		String destinationInstanceRefStr[] = destinationInstanceRef.split("_");

		String sourceName = ControllerClass.allObjects.get(sourceInstanceRefStr[0]);
		if (sourceName == null)
			return new ResponseEntity<>("Please Provide the correct source input", HttpStatus.BAD_REQUEST);

		String destinationName = ControllerClass.allObjects.get(destinationInstanceRefStr[0]);
		if (destinationName == null)
			return new ResponseEntity<>("Please Provide the correct destination input", HttpStatus.BAD_REQUEST);

		Components sourceComponents = serviceInterface.getComponentsObjRef(sourceInstanceRef);
		if (sourceComponents == null)
			return new ResponseEntity<>(
					"No " + sourceInstanceRefStr[0] + " Source Object is present with given Reference",
					HttpStatus.NOT_FOUND);

		Map<String, String> destinationServiceIp = new HashMap<String, String>();

		if (destinationName.equals("Y")) {
			Components components = serviceInterface.getComponentsObjRef(destinationInstanceRef);
			if (components == null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "No " + destinationInstanceRefStr[0] + " Object is present with given Reference");
				return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			} else
				destinationServiceIp.put("serviceIp", components.getServiceip());
		} else {
			switch (destinationInstanceRefStr[0]) {
				case "Oltp": {
					Oltp oltp = serviceInterface.getOltpObjRef(destinationInstanceRef);
					if (oltp == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No OLTP Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else
						destinationServiceIp.put("serviceIp", oltp.getOltpipaddr());
				}
				case "Gatherer":
					Gatherer gatherer = serviceInterface.getGathererObjRef(destinationInstanceRef);
					if (gatherer == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No Gatherer Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else
						destinationServiceIp.put("serviceIp", gatherer.getGipaddr());
				case "Mq":
					Mq mq = serviceInterface.getMqObjRef(destinationInstanceRef);
					if (mq == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No MQ Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else
						destinationServiceIp.put("serviceIp", mq.getMqipaddr());
				case "Simulator":
					Simulator simulator = serviceInterface.getSimulatorObjRef(destinationInstanceRef);
					if (simulator == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No Simulator Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else
						destinationServiceIp.put("serviceIp", simulator.getSimulatorIp());
				default: {
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please provide the correct input");
					return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
				}
			}
		}

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(sourceComponents.getUsername());
		byte[] decryptedPassword = decoder.decode(sourceComponents.getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		try {
			Runtime.getRuntime()
					.exec("/backup/Users/caradhya/RootUserFiles/ResiliencyRelatedController/addIpPacketLoss.sh "
							+ userName + " " + sourceComponents.getServiceip() + " " + passWord + " "
							+ destinationServiceIp.get("serviceIp"));
			Thread.sleep(5000);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>("PacketLoss is added to source IP", HttpStatus.OK);
	}

	@PostMapping("deleteIpPacketLoss")
	public ResponseEntity<Object> deleteIpPacketLoss(@RequestBody ResiliencyRequest resiliencyRequest) {

		String sourceInstanceRef = resiliencyRequest.getSourceInstanceRef();
		String destinationInstanceRef = resiliencyRequest.getDestinationInstanceRef();

		String sourceInstanceRefStr[] = sourceInstanceRef.split("_");
		String destinationInstanceRefStr[] = destinationInstanceRef.split("_");

		String sourceName = ControllerClass.allObjects.get(sourceInstanceRefStr[0]);
		if (sourceName == null)
			return new ResponseEntity<>("Please Provide the correct source input", HttpStatus.BAD_REQUEST);

		String destinationName = ControllerClass.allObjects.get(destinationInstanceRefStr[0]);
		if (destinationName == null)
			return new ResponseEntity<>("Please Provide the correct destination input", HttpStatus.BAD_REQUEST);

		Components sourceComponents = serviceInterface.getComponentsObjRef(sourceInstanceRef);
		if (sourceComponents == null)
			return new ResponseEntity<>(
					"No " + sourceInstanceRefStr[0] + " Source Object is present with given Reference",
					HttpStatus.NOT_FOUND);

		Map<String, String> destinationServiceIp = new HashMap<String, String>();

		if (destinationName.equals("Y")) {
			Components components = serviceInterface.getComponentsObjRef(destinationInstanceRef);
			if (components == null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "No " + destinationInstanceRefStr[0] + " Object is present with given Reference");
				return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			} else
				destinationServiceIp.put("serviceIp", components.getServiceip());
		} else {
			switch (destinationInstanceRefStr[0]) {
				case "Oltp": {
					Oltp oltp = serviceInterface.getOltpObjRef(destinationInstanceRef);
					if (oltp == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No OLTP Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else
						destinationServiceIp.put("serviceIp", oltp.getOltpipaddr());
				}
				case "Gatherer":
					Gatherer gatherer = serviceInterface.getGathererObjRef(destinationInstanceRef);
					if (gatherer == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No Gatherer Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else
						destinationServiceIp.put("serviceIp", gatherer.getGipaddr());
				case "Mq":
					Mq mq = serviceInterface.getMqObjRef(destinationInstanceRef);
					if (mq == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No MQ Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else
						destinationServiceIp.put("serviceIp", mq.getMqipaddr());
				case "Simulator":
					Simulator simulator = serviceInterface.getSimulatorObjRef(destinationInstanceRef);
					if (simulator == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No Simulator Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					} else
						destinationServiceIp.put("serviceIp", simulator.getSimulatorIp());
				default: {
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please provide the correct input");
					return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
				}
			}
		}

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(sourceComponents.getUsername());
		byte[] decryptedPassword = decoder.decode(sourceComponents.getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		try {
			Runtime.getRuntime()
					.exec("/backup/Users/caradhya/RootUserFiles/ResiliencyRelatedController/deleteIpPacketLoss.sh "
							+ userName + " " + sourceComponents.getServiceip() + " " + passWord + " "
							+ destinationServiceIp.get("serviceIp"));
			Thread.sleep(5000);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>("Deleted the IpPacket Loss case in source IP", HttpStatus.OK);
	}

}
