/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.controller;

import java.util.ArrayList;
import java.util.List;

import com.yodlee.perf.fridaynew.model.Testcaseverdict;
import com.yodlee.perf.fridaynew.model.request.TestcaseverdictRequest;
import com.yodlee.perf.fridaynew.service.ServiceClass;
import com.yodlee.perf.fridaynew.service.ServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("TestcaseVerdictDetails")
public class TestcaseverdictController {

	@Autowired
	private ServiceInterface serviceInterface;

	@GetMapping("{id}")
	public TestcaseverdictRequest getTestcaseVerdictById(@PathVariable Long id) {
		Testcaseverdict testcaseverdict = serviceInterface.getTestcaseVerdictById(id);
		if (testcaseverdict == null)
			return null;
		else {
			ServiceClass serviceClass = new ServiceClass();
			return serviceClass.convertTestcaseverdictToTestcaseverdictRequest(testcaseverdict, null);
		}
	}

	@GetMapping("")
	public List<TestcaseverdictRequest> getTestcaseVerdictDetails(@RequestParam String testcaseid,
			@RequestParam(required = false) String instanceType) {

		List<Testcaseverdict> testcaseverdictList = null;

		if (instanceType == null) {
			testcaseverdictList = serviceInterface.getTestcaseVerdictByTestcaseId(testcaseid);

			if (testcaseverdictList != null) {
				ServiceClass serviceClass = new ServiceClass();
				List<TestcaseverdictRequest> testcaseverdictRequestList = new ArrayList<TestcaseverdictRequest>();
				for (Testcaseverdict testcaseverdict : testcaseverdictList) {
					testcaseverdictRequestList
							.add(serviceClass.convertTestcaseverdictToTestcaseverdictRequest(testcaseverdict, null));
				}
				return testcaseverdictRequestList;
			}
		} else {
			testcaseverdictList = serviceInterface.getTestcaseVerdictByTestcaseIdAndInstanceType(testcaseid,
					instanceType);

			if (testcaseverdictList != null) {
				ServiceClass serviceClass = new ServiceClass();
				List<TestcaseverdictRequest> testcaseverdictRequestList = new ArrayList<TestcaseverdictRequest>();
				for (Testcaseverdict testcaseverdict : testcaseverdictList) {
					testcaseverdictRequestList
							.add(serviceClass.convertTestcaseverdictToTestcaseverdictRequest(testcaseverdict, null));
				}
				return testcaseverdictRequestList;
			}
		}
		return null;
	}

	@DeleteMapping("{id}")
	public void deleteTestcaseVerdictById(@PathVariable Long id) {

		Testcaseverdict testcaseverdict = serviceInterface.getTestcaseVerdictById(id);
		if (testcaseverdict != null) {
			testcaseverdict.setActive(0L);
			serviceInterface.addTestcaseverdict(testcaseverdict);
		}
	}

	@PostMapping("")
	public TestcaseverdictRequest addTestcaseVerdict(@RequestBody TestcaseverdictRequest testcaseverdictRequest) {

		Testcaseverdict oldTestcaseverdict = serviceInterface.getTestcaseverdict(testcaseverdictRequest.getTestCaseId(),
				testcaseverdictRequest.getInstanceType(), testcaseverdictRequest.getInterval(), 1L,
				testcaseverdictRequest.getBranch());
		if (oldTestcaseverdict != null)
			return null;
		else {
			ServiceClass serviceClass = new ServiceClass();
			Testcaseverdict testcaseverdict = serviceClass
					.convertTestcaseverdictRequestToTestcaseverdict(testcaseverdictRequest, null);
			serviceInterface.addTestcaseverdict(testcaseverdict);
			return testcaseverdictRequest;
		}
	}

	@PutMapping("")
	public TestcaseverdictRequest editTestcaseVerdict(@RequestBody TestcaseverdictRequest testcaseverdictRequest) {

		Testcaseverdict oldTestcaseverdict = serviceInterface.getTestcaseverdict(testcaseverdictRequest.getTestCaseId(),
				testcaseverdictRequest.getInstanceType(), testcaseverdictRequest.getInterval(), 1L,
				testcaseverdictRequest.getBranch());
		if (oldTestcaseverdict == null)
			return null;
		else {
			ServiceClass serviceClass = new ServiceClass();
			oldTestcaseverdict = serviceClass.convertTestcaseverdictRequestToTestcaseverdict(testcaseverdictRequest,
					oldTestcaseverdict);
			serviceInterface.addTestcaseverdict(oldTestcaseverdict);
			return testcaseverdictRequest;
		}
	}
}
