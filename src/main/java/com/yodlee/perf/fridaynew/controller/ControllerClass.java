/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */

package com.yodlee.perf.fridaynew.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yodlee.perf.fridaynew.model.References;

public class ControllerClass {

	static List<References> getAllObjects;
	/*
	 * Load the Instance name and component info
	 */
	static Map<String, String> allObjects;
	/*
	 * Load the log related info for the component
	 */
	static Map<String, String> logRelated;

	public static void setMetod(List<References> ref) {
		getAllObjects = ref;
		Map<String, String> allobjects = new HashMap<String, String>();
		for (References references : getAllObjects) {
			allobjects.put(references.getName(), references.getIscomponent());
		}
		allObjects = allobjects;
		Map<String, String> logrelated = new HashMap<String, String>();
		for (References references : getAllObjects) {
			logrelated.put(references.getName(), references.getComponentrelated());
		}
		logRelated = logrelated;
	}

}
