/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */

package com.yodlee.perf.fridaynew.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yodlee.perf.fridaynew.configuration.ConfigurationClass;
import com.yodlee.perf.fridaynew.model.Components;
import com.yodlee.perf.fridaynew.model.Data;
import com.yodlee.perf.fridaynew.model.Execmetricsummary;
import com.yodlee.perf.fridaynew.model.Exectracking;
import com.yodlee.perf.fridaynew.model.GenerateHTML;
import com.yodlee.perf.fridaynew.model.Oltp;
import com.yodlee.perf.fridaynew.model.Testcaseverdict;
import com.yodlee.perf.fridaynew.model.VerifyMetricsRequest;
import com.yodlee.perf.fridaynew.model.request.AwrRequest;
import com.yodlee.perf.fridaynew.model.request.CollectMetricsRequest;
import com.yodlee.perf.fridaynew.model.request.ZabbixReportReq;
import com.yodlee.perf.fridaynew.service.ServiceClass;
import com.yodlee.perf.fridaynew.service.ServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "PTAAS Common API Related Info", description = "PTAAS Common API Related info for the Team Reference")
@RestController
public class CommonApiRelatedController {

	@Autowired
	private ServiceInterface serviceInterface;
	@Autowired
	private ConfigurationClass configurationClass;

	@PostMapping("generateZabbixReport")
	public ResponseEntity<Object> generateZabbixReport(@RequestBody ZabbixReportReq zabbixReportReq) {

		String str[] = zabbixReportReq.getInstanceRef().split("_");

		String myComponent = ControllerClass.allObjects.get(str[0]);
		if (myComponent == null)
			return new ResponseEntity<>("Please Provide the correct input", HttpStatus.BAD_REQUEST);

		Components components = serviceInterface.getComponentsObjRef(zabbixReportReq.getInstanceRef());
		if (components == null)
			return new ResponseEntity<>("No " + str[0] + " Object is present with given Reference",
					HttpStatus.NOT_FOUND);

		ServiceClass serviceClass = new ServiceClass();

		String completeString = serviceClass.zabbixReportGenerate(components.getZabbixelementid(),
				zabbixReportReq.getStartTime(), zabbixReportReq.getEndTime(), components.getInstancename(),
				zabbixReportReq.getDestination());

		if (!completeString.equalsIgnoreCase("Completed"))
			return new ResponseEntity<>("Zabbix reports are not generated", HttpStatus.BAD_REQUEST);

		Exectracking exectracking = serviceInterface.getExectrackingbyfoldername(zabbixReportReq.getDestination());
		if (exectracking != null) {

			List<Execmetricsummary> listexecmetricsummary = serviceInterface
					.getExecmetricsummarybytestcaseexectrackingobjectname(exectracking.getTestcaseid(),
							exectracking.getExectrackingid(), components.getInstancename());

			if (listexecmetricsummary.size() == 0 || listexecmetricsummary == null) {
				Execmetricsummary execmetricsummary = new Execmetricsummary();
				execmetricsummary.setAwrlink(zabbixReportReq.getAwrPath());
				execmetricsummary.setTestcaseid(exectracking.getTestcaseid());
				execmetricsummary.setExectracking(exectracking.getExectrackingid());
				execmetricsummary.setObjectname(components.getInstancename());
				execmetricsummary.setCreated(exectracking.getCreated());
				String temp = zabbixReportReq.getStartTime().replaceAll("%3A", ":");
				String datetoformat = temp.replaceAll("\\+", " ");
				execmetricsummary.setTcstarttime(serviceClass.tsToSec8601(datetoformat));
				temp = zabbixReportReq.getEndTime().replaceAll("%3A", ":");
				datetoformat = temp.replaceAll("\\+", " ");
				execmetricsummary.setTcendtime(serviceClass.tsToSec8601(datetoformat));
				execmetricsummary.setZabbixreportlink(
						"http://192.168.136.136/zabbix/screens.php?elementid=" + components.getZabbixelementid()
								+ "&from=" + zabbixReportReq.getStartTime() + "&to=" + zabbixReportReq.getEndTime());
				execmetricsummary.setZabbixelementid(components.getZabbixelementid().longValue());
				execmetricsummary.setInterval(0L);
				execmetricsummary = serviceInterface.addExecmetricsummary(execmetricsummary);
			}
		}
		return new ResponseEntity<>("Zabbix reports are generated", HttpStatus.OK);
	}

	@PostMapping("updateTestReport")
	public ResponseEntity<Object> updateTestReport(@RequestBody ZabbixReportReq zabbixReportReq) {

		ServiceClass serviceClass = new ServiceClass();
		String str[] = zabbixReportReq.getInstanceRef().split("_");

		if (zabbixReportReq.getOltpRef() != null) {
			Oltp oltp = serviceInterface.getOltpObjRef(zabbixReportReq.getOltpRef());
			serviceClass.reportUpdate("OLTP NAME = " + oltp.getOltpname(),
					zabbixReportReq.getDestination() + "/TestResults.txt");
		}

		if (zabbixReportReq.getAwrPath() != null) {
			serviceClass.reportUpdate("AWR Report = " + zabbixReportReq.getAwrPath(),
					zabbixReportReq.getDestination() + "/TestResults.txt");
		}
		String myComponent = ControllerClass.allObjects.get(str[0]);
		if (myComponent == null)
			return new ResponseEntity<>("Please Provide the correct input", HttpStatus.BAD_REQUEST);

		Components components = serviceInterface.getComponentsObjRef(zabbixReportReq.getInstanceRef());
		if (components == null)
			return new ResponseEntity<>("No " + str[0] + " Object is present with given Reference",
					HttpStatus.NOT_FOUND);

		String mainString = components.getInstancename() + " (" + components.getInstanceref()
				+ ") = http://192.168.136.136/zabbix/screens.php?elementid=" + components.getZabbixelementid()
				+ "&from=" + zabbixReportReq.getStartTime() + "&to=" + zabbixReportReq.getEndTime();

		serviceClass.reportUpdate(mainString, zabbixReportReq.getDestination() + "/TestResults.txt");

		Long interval = 0L;

		if (zabbixReportReq.getInterval() != null && zabbixReportReq.getInterval() >= 0)
			interval = zabbixReportReq.getInterval().longValue();

		Exectracking exectracking = serviceInterface.getExectrackingbyfoldername(zabbixReportReq.getDestination());

		try {
			GenerateHTML.generateMetrics(exectracking.getExecfolder() + "/TestResults.html",
					exectracking.getExectrackingid(), zabbixReportReq.getInstanceRef(), exectracking.getTestcaseid(),
					interval);
		} catch (Exception e) {

		}

		return new ResponseEntity<>("Zabbix reports are Updated", HttpStatus.OK);
	}

	@PostMapping("updateSystemSpecification")
	public ResponseEntity<Object> updateSystemSpecification(@RequestBody ZabbixReportReq zabbixReportReq) {

		String str[] = zabbixReportReq.getInstanceRef().split("_");
		ServiceClass serviceClass = new ServiceClass();

		String myComponent = ControllerClass.allObjects.get(str[0]);
		if (myComponent == null)
			return new ResponseEntity<>("Please Provide the correct input", HttpStatus.BAD_REQUEST);

		Components components = serviceInterface.getComponentsObjRef(zabbixReportReq.getInstanceRef());
		if (components == null)
			return new ResponseEntity<>("No " + str[0] + " Object is present with given Reference",
					HttpStatus.NOT_FOUND);

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(components.getUsername());
		byte[] decryptedPassword = decoder.decode(components.getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		Map<String, String> map = serviceClass.specificationUpdate(userName, components.getServiceip(), passWord,
				zabbixReportReq.getDestination(), components.getInstancename());

		if (!map.get("status").equalsIgnoreCase("Completed"))
			return new ResponseEntity<>("Couldn't update the Specifications file", HttpStatus.BAD_REQUEST);

		Long cpu = Long.parseLong(map.get("cpu").replaceAll("\\r\\n|\\r|\\n", ""));

		Long memory = Long.parseLong(map.get("memory").replaceAll("\\r\\n|\\r|\\n", ""));

		String buildInfo = null;

		try {
			if (components.getSutversion() != null && components.getIsdocker().equals("N"))
				buildInfo = serviceClass.updateBuildInfoForNonDocker(userName, components.getServiceip(), passWord,
						components.getSutversion());
		} catch (Exception e) {
			buildInfo = serviceClass.updateBuildInfoForDocker(userName, components.getServiceip(), passWord,
					components.getSutversion());
		}

		if (components.getSutversion() != null && components.getIsdocker().equals("Y"))
			buildInfo = serviceClass.updateBuildInfoForDocker(userName, components.getServiceip(), passWord,
					components.getSutversion());

		Exectracking exectracking = serviceInterface.getExectrackingbyfoldername(zabbixReportReq.getDestination());
		if (exectracking != null) {
			List<Execmetricsummary> listexecmetricsummary = serviceInterface
					.getExecmetricsummarybytestcaseexectrackingobjectname(exectracking.getTestcaseid(),
							exectracking.getExectrackingid(), components.getInstancename());
			if (listexecmetricsummary.size() != 0) {
				for (Execmetricsummary execmetricsummary : listexecmetricsummary) {
					execmetricsummary.setCpucount(cpu);
					execmetricsummary.setMemorycount(memory);
					execmetricsummary.setBuild(buildInfo);
					execmetricsummary = serviceInterface.addExecmetricsummary(execmetricsummary);
				}
			}
		}
		return new ResponseEntity<>("Updated the Specifications file", HttpStatus.OK);
	}

	@GetMapping("generateSnap/{object}")
	public ResponseEntity<Object> generateSnap(@PathVariable String object) {

		String str[] = object.split("_");
		String mainString = null;

		if (!str[0].equals("Oltp"))
			return new ResponseEntity<>("Please provide correct input.", HttpStatus.BAD_REQUEST);
		else {

			Oltp oltp = serviceInterface.getOltpObjRef(object);
			if (oltp == null)
				return new ResponseEntity<>("Given oltp does't exists.", HttpStatus.BAD_REQUEST);
			else {
				Process p = null;
				try {
					p = Runtime.getRuntime().exec("sh /backup/Users/caradhya/RootUserFiles/Generate_AWRSnap.sh "
							+ oltp.getOltpusername() + " " + oltp.getOltpschema() + " " + oltp.getOltppasswd());

					InputStream is = p.getInputStream();

					int i = 0;
					StringBuffer sb = new StringBuffer();
					String temp = null;
					while ((i = is.read()) != -1)
						sb.append((char) i);
					temp = sb.toString();
					String[] temp1 = temp.split("\n");
					String[] temp2 = temp1[temp1.length - 1].split(" ");
					mainString = temp2[temp2.length - 1];

				} catch (IOException e) {
					e.printStackTrace();
				}

				Map<String, Integer> map = new HashMap<String, Integer>();
				map.put("snapId", Integer.parseInt(mainString));
				return new ResponseEntity<>(map, HttpStatus.OK);
			}
		}
	}

	@PostMapping("generateAwr")
	public ResponseEntity<Object> generateAwr(@RequestBody AwrRequest awrrequest) {

		String mainString = null;

		String str[] = awrrequest.getOltpref().split("_");

		if (!str[0].equals("Oltp"))
			return new ResponseEntity<>("Please provide correct input.", HttpStatus.BAD_REQUEST);
		else {
			Oltp oltp = serviceInterface.getOltpObjRef(awrrequest.getOltpref());
			if (oltp == null)
				return new ResponseEntity<>("Given oltp does't exists.", HttpStatus.BAD_REQUEST);
			else {
				String filename = "awrrpt_" + oltp.getOltpschema() + "_" + awrrequest.getStartid() + "_"
						+ awrrequest.getEndid() + ".html";
				String folder = awrrequest.getReportpath() + "/Reports";

				ServiceClass serviceClass = new ServiceClass();

				String temp = serviceClass.executeShellCommand("sh /backup/Users/caradhya/RootUserFiles/GenerateAWR.sh "
						+ oltp.getOltpusername() + " " + oltp.getOltpschema() + " " + oltp.getOltppasswd() + " "
						+ awrrequest.getStartid() + " " + awrrequest.getEndid() + " " + filename + " " + folder);

				if (configurationClass.isPrintLog())
					System.out.println(temp);

				String finalstring = awrrequest.getReportpath().replaceFirst("/backup/", "");

				mainString = "http://192.168.84.188/" + finalstring + "/Reports/" + filename;

				Map<String, String> map = new HashMap<String, String>();
				map.put("awrUrl", mainString);

				serviceClass.collectFTSQueries(oltp.getOltpusername(), oltp.getOltpschema(), oltp.getOltppasswd(),
						awrrequest.getStartid(), awrrequest.getEndid(), folder);

				return new ResponseEntity<>(map, HttpStatus.OK);
			}
		}
	}

	@GetMapping("getZabbixTime")
	public ResponseEntity<Object> getZabbixTime() {

		ServiceClass serviceClass = new ServiceClass();
		String temp = serviceClass.executeShellCommand(
				"/backup/Users/caradhya/RootUserFiles/getZabbixtime.sh ptaas 192.168.136.136 ptaas");
		List<String> arraylist = new ArrayList<String>();
		String arr[] = temp.split("\n");
		for (int i = 0; i < arr.length; i++)
			arraylist.add(arr[i].replaceAll("\\r\\n|\\r|\\n", ""));
		String num = arraylist.get(arraylist.size() - 2);
		String epochTime = num.replaceAll("%3A", ":");
		String datetoformat = epochTime.replaceAll("\\+", " ");
		Map<String, String> map = new HashMap<String, String>();
		map.put("zabbixTime", num);
		map.put("epochTime", serviceClass.tsToSec8601(datetoformat).toString());
		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	@PostMapping("collectMetrics")
	public ResponseEntity<Object> collectMetrics(@RequestBody CollectMetricsRequest collectMetricsRequest) {

		/*
		 * Return bad request if Start time and duration is not given when interval is
		 * provided.
		 */

		if (collectMetricsRequest.getInterval() != null) {
			if (collectMetricsRequest.getDuration() == null || collectMetricsRequest.getDeltastarttime() == null)
				return new ResponseEntity<>("Please Provide the correct starttime and duration for interval",
						HttpStatus.BAD_REQUEST);
		}

		String str[] = collectMetricsRequest.getInstanceref().split("_");

		String myComponent = ControllerClass.allObjects.get(str[0]);
		if (myComponent == null)
			return new ResponseEntity<>("Please Provide the correct input", HttpStatus.BAD_REQUEST);

		Components components = serviceInterface.getComponentsObjRef(collectMetricsRequest.getInstanceref());
		if (components == null)
			return new ResponseEntity<>("No " + str[0] + " Object is present with given Reference",
					HttpStatus.NOT_FOUND);

		/*
		 * Assuming initial interval as 0
		 */
		Integer interval = 0;

		/*
		 * Assuming initial delta start time as 600 secs
		 */
		Integer delta = 600;

		/*
		 * If initial startup time given consider that one
		 */
		if (collectMetricsRequest.getDeltastarttime() != null)
			delta = collectMetricsRequest.getDeltastarttime();

		/*
		 * Assuming initial duration as 600 secs
		 */
		Integer duration = 3000;

		/*
		 * If initial Duration given consider that one
		 */
		if (collectMetricsRequest.getDuration() != null)
			duration = collectMetricsRequest.getDuration();

		/*
		 * Get the Execmetricsummary from the table
		 */
		Execmetricsummary execmetricsummary = serviceInterface
				.getExecmetricsummarybytestcaseexectrackingobjectnameinterval(collectMetricsRequest.getTestcaseid(),
						collectMetricsRequest.getUniqueid(), components.getInstancename(), interval.longValue());

		if (execmetricsummary == null)
			return new ResponseEntity<>(
					"No Execution metrics is present for the given testcaseId,uniqueId and instanceref",
					HttpStatus.NOT_FOUND);
		if (collectMetricsRequest.getInterval() > 0 && collectMetricsRequest.getInterval() != null)
			interval = collectMetricsRequest.getInterval();

		/*
		 * Collect the Metrics between start time and end time(startTime+duration)
		 */
		Long metricsStart = execmetricsummary.getTcstarttime() + delta;
		Long metricsEnd = metricsStart + duration;
		execmetricsummary.setMetricsstart(metricsStart);
		execmetricsummary.setMetricsend(metricsEnd);

		RestTemplate restTemplate = new RestTemplate();
		Map<String, String> args = new HashMap<String, String>();
		args.put("hostname", components.getZabbixhostname());
		args.put("starttime", metricsStart.toString());
		args.put("endtime", metricsEnd.toString());

		String vmcpu = null;
		String processcpu = null;
		String memory = null;
		String heapused = null;
		String jvmThroughPut = null;

		try {
			vmcpu = restTemplate.postForObject("http://localhost:8182/getVmCpu", args, String.class);
		} catch (HttpServerErrorException e) {
		}

		try {
			processcpu = restTemplate.postForObject("http://localhost:8182/getProcessCpu", args, String.class);
		} catch (HttpServerErrorException e) {
		}
		try {
			memory = restTemplate.postForObject("http://localhost:8182/getMemory", args, String.class);
		} catch (HttpServerErrorException e) {
		}
		try {
			heapused = restTemplate.postForObject("http://localhost:8182/getHeapUsed", args, String.class);
		} catch (HttpServerErrorException e) {
		}

		if (components.getGctype() != null) {
			try {
				args.put("gcType", components.getGctype());
				jvmThroughPut = restTemplate.postForObject("http://localhost:8182/getGCThroughput", args, String.class);
			} catch (HttpServerErrorException e) {
			}
		}
		execmetricsummary.setVmcpu(vmcpu);
		execmetricsummary.setProcesscpu(processcpu);
		execmetricsummary.setMemory(memory);
		execmetricsummary.setHeapused(heapused);
		execmetricsummary.setGcthroughput(jvmThroughPut);

		if (interval > 0) {

			Execmetricsummary newExecmetricsummary = new Execmetricsummary();
			newExecmetricsummary.setTestcaseid(execmetricsummary.getTestcaseid());
			newExecmetricsummary.setExectracking(execmetricsummary.getExectracking());
			newExecmetricsummary.setObjectname(execmetricsummary.getObjectname());
			newExecmetricsummary.setCreated(execmetricsummary.getCreated());
			newExecmetricsummary.setTcstarttime(execmetricsummary.getTcstarttime());
			newExecmetricsummary.setTcendtime(execmetricsummary.getTcendtime());
			newExecmetricsummary.setZabbixelementid(execmetricsummary.getZabbixelementid());
			newExecmetricsummary.setZabbixreportlink(execmetricsummary.getZabbixreportlink());
			newExecmetricsummary.setAwrlink(execmetricsummary.getAwrlink());
			newExecmetricsummary.setCpucount(execmetricsummary.getCpucount());
			newExecmetricsummary.setMemorycount(execmetricsummary.getMemorycount());
			newExecmetricsummary.setInterval(interval.longValue());
			newExecmetricsummary.setMetricsstart(execmetricsummary.getMetricsstart());
			newExecmetricsummary.setMetricsend(execmetricsummary.getMetricsend());
			newExecmetricsummary.setVmcpu(vmcpu);
			newExecmetricsummary.setMemory(memory);
			newExecmetricsummary.setProcesscpu(processcpu);
			newExecmetricsummary.setGcthroughput(jvmThroughPut);
			newExecmetricsummary.setHeapused(heapused);
			newExecmetricsummary.setTps(execmetricsummary.getTps());
			newExecmetricsummary.setPercentageerror(execmetricsummary.getPercentageerror());
			newExecmetricsummary.setBuild(execmetricsummary.getBuild());

			serviceInterface.addExecmetricsummary(newExecmetricsummary);

			return new ResponseEntity<>("Collect Metrics API updated", HttpStatus.OK);
		}
		execmetricsummary = serviceInterface.addExecmetricsummary(execmetricsummary);
		return new ResponseEntity<>("Collect Metrics API updated", HttpStatus.OK);
	}

	@PostMapping("verifyMetrics")
	public ResponseEntity<Object> verifyMetrics(@RequestBody VerifyMetricsRequest verifyMetricsRequest) {

		Components components = serviceInterface.getComponentsObjRef(verifyMetricsRequest.getInstanceref());
		if (components == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Given InstanceRef is not present");
			return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
		}

		String instanceArr[] = components.getInstancename().split("_");
		String instance = instanceArr[0];
		ServiceClass serviceClass = new ServiceClass();
		ObjectMapper Obj = new ObjectMapper();
		String cpuJson, memoryJson, processCpuJson, heapUsedJson;
		cpuJson = memoryJson = processCpuJson = heapUsedJson = null;
		Map<String, Map<String, Data>> result = null;
		Execmetricsummary execmetricsummary = null;

		if (verifyMetricsRequest.getInterval() == null) {

			execmetricsummary = serviceInterface.getExecmetricsummarybytestcaseexectrackingobjectnameinterval(
					verifyMetricsRequest.getTestcaseid(), verifyMetricsRequest.getUniqueid(),
					components.getInstancename(), 0L);

			Testcaseverdict testcaseverdict = serviceInterface.getTestcaseverdict(verifyMetricsRequest.getTestcaseid(),
					instance, 0L, 1L, verifyMetricsRequest.getBranch());

			if (execmetricsummary == null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "Please provide the correct data for comparision");
				return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}

			result = serviceClass.compareWithTestData(execmetricsummary, testcaseverdict);
			if (testcaseverdict != null)
				execmetricsummary.setTestcaseverdictid(testcaseverdict.getTestcaseverdictid());
		} else {
			if (verifyMetricsRequest.getInterval().size() == 0 || verifyMetricsRequest.getInterval().size() == 1) {
				Long intervalRef = null;
				if (verifyMetricsRequest.getInterval().size() == 1)
					intervalRef = Long.parseLong(verifyMetricsRequest.getInterval().get(0));
				else
					intervalRef = 0L;

				execmetricsummary = serviceInterface.getExecmetricsummarybytestcaseexectrackingobjectnameinterval(
						verifyMetricsRequest.getTestcaseid(), verifyMetricsRequest.getUniqueid(),
						components.getInstancename(), intervalRef);

				Testcaseverdict testcaseverdict = serviceInterface.getTestcaseverdict(
						verifyMetricsRequest.getTestcaseid(), instance, intervalRef, 1L,
						verifyMetricsRequest.getBranch());

				if (execmetricsummary == null) {
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please provide the correct data for comparision");
					return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
				}

				result = serviceClass.compareWithTestData(execmetricsummary, testcaseverdict);
				if (testcaseverdict != null)
					execmetricsummary.setTestcaseverdictid(testcaseverdict.getTestcaseverdictid());
			}

			if (verifyMetricsRequest.getInterval().size() > 2) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "Please provide only 2 inputs to compare");
				return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}

			if (verifyMetricsRequest.getInterval().size() == 2) {

				Long minInterval = null, maxInterval = null;
				List<String> longValues = verifyMetricsRequest.getInterval();
				Long val1 = Long.parseLong(longValues.get(0));
				Long val2 = Long.parseLong(longValues.get(1));
				if (val1 == val2) {
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please provide 2 different data to compare");
					return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
				}
				if (val1 > val2) {
					minInterval = val2;
					maxInterval = val1;
				} else {
					minInterval = val1;
					maxInterval = val2;
				}

				execmetricsummary = serviceInterface.getExecmetricsummarybytestcaseexectrackingobjectnameinterval(
						verifyMetricsRequest.getTestcaseid(), verifyMetricsRequest.getUniqueid(),
						components.getInstancename(), maxInterval);

				Execmetricsummary mainExecmetricsummary = serviceInterface
						.getExecmetricsummarybytestcaseexectrackingobjectnameinterval(
								verifyMetricsRequest.getTestcaseid(), verifyMetricsRequest.getUniqueid(),
								components.getInstancename(), minInterval);

				if (execmetricsummary == null || mainExecmetricsummary == null) {
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Records are not present to compare");
					return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
				}

				result = serviceClass.compareWithExecData(execmetricsummary, mainExecmetricsummary);
				execmetricsummary.setTestcaseverdictid(mainExecmetricsummary.getExecmetricsummaryid());

			}
		}
		try {
			cpuJson = Obj.writeValueAsString(result.get("vmcpu"));
			memoryJson = Obj.writeValueAsString(result.get("memory"));
			processCpuJson = Obj.writeValueAsString(result.get("processCpu"));
			heapUsedJson = Obj.writeValueAsString(result.get("heapUsed"));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		execmetricsummary.setVmcpuverdict(cpuJson);
		execmetricsummary.setMemoryverdict(memoryJson);
		execmetricsummary.setProcesscpuverdict(processCpuJson);
		execmetricsummary.setHeapusedverdict(heapUsedJson);
		execmetricsummary.setBranch(verifyMetricsRequest.getBranch());
		execmetricsummary.setGcthroughputverdict(execmetricsummary.getGcthroughput());

		serviceInterface.addExecmetricsummary(execmetricsummary);
		return new ResponseEntity<>("Verification Completed", HttpStatus.OK);

	}

	@GetMapping("generateTestReport/{uniqueId}")
	public ResponseEntity<Object> generateTestReport(@PathVariable String uniqueId) {

		Exectracking exectracking = null;
		try {
			exectracking = serviceInterface.getExectracking(uniqueId);
		} catch (NoSuchElementException e) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "No Execution is present with the given tracking Id");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}

		GenerateHTML.createFile(exectracking.getExecfolder() + "/TestResults.html");
		GenerateHTML.generateSummary(exectracking.getExecfolder() + "/TestResults.html", uniqueId);

		Map<String, String> map = new HashMap<String, String>();
		map.put("message", "Generate HTML Report Completed");

		return new ResponseEntity<>(map, HttpStatus.CREATED);

	}

}
