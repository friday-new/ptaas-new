/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */

package com.yodlee.perf.fridaynew.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yodlee.perf.fridaynew.model.AttributeData;
import com.yodlee.perf.fridaynew.model.CobParam;
import com.yodlee.perf.fridaynew.model.CobrandAclValue;
import com.yodlee.perf.fridaynew.model.Oltp;
import com.yodlee.perf.fridaynew.model.ParamAcl;
import com.yodlee.perf.fridaynew.model.ParamKey;
import com.yodlee.perf.fridaynew.model.Sqlverdict;
import com.yodlee.perf.fridaynew.model.UpdateFeatureKey;
import com.yodlee.perf.fridaynew.model.request.FeatureKeyObject;
import com.yodlee.perf.fridaynew.service.ServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RestController
public class OltpFeatureRelatedController {

	@Autowired
	private ServiceInterface serviceInterface;

	@PostMapping("showFeatureKey")
	public ResponseEntity<Object> showFeatureKey(@RequestBody FeatureKeyObject featureKeyObject) {

		Oltp oltp = serviceInterface.getOltpObjRef(featureKeyObject.getOltpRef());
		if (oltp == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "The oltp Object doesn't exists with the given refrence");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}
		if ("Y".equalsIgnoreCase(oltp.getIsconfig()) || oltp.getIsconfig() == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the OLTP refrence");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}

		String url;
		RestTemplate restTemplate = new RestTemplate();
		Map<String, String> args = new HashMap<String, String>();
		args.put("keyId", featureKeyObject.getKeyId().toString());

		switch (featureKeyObject.getTableName().toUpperCase()) {

			case "PARAM_ACL":
				url = "http://localhost:" + oltp.getPort() + "/getParamAcl";
				break;

			case "PARAM_KEY":
				url = "http://localhost:" + oltp.getPort() + "/getParamKey";
				break;

			case "COB_PARAM":
				if (featureKeyObject.getCobrandId() == null) {
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please provide the Cobrand Id");
					return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
				}
				url = "http://localhost:" + oltp.getPort() + "/getCobParam";
				args.put("cobrandId", featureKeyObject.getCobrandId().toString());
				break;

			case "COBRAND_ACL_VALUE":
				if (featureKeyObject.getCobrandId() == null) {
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please provide the Cobrand Id");
					return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
				}
				args.put("cobrandId", featureKeyObject.getCobrandId().toString());
				url = "http://localhost:" + oltp.getPort() + "/getCobrandAclValue";
				break;

			case "ATTRIBUTE_COB_CONFIG":
				if (featureKeyObject.getCobrandId() == null) {
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please provide the Cobrand Id");
					return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
				}
				args.put("cobrandId", featureKeyObject.getCobrandId().toString());
				url = "http://localhost:" + oltp.getPort() + "/getAttributeCobConfig";
				break;

			default:
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "Please provide the Correct Table Name");
				return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}

		String result = null;
		try {
			result = restTemplate.postForObject(url, args, String.class);
		} catch (HttpClientErrorException e) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "No Values present for the given parameters");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@GetMapping("loadDefaultFeatureKeys/{oltpRef}")
	public ResponseEntity<Object> loadDefaultFeatureKeys(@PathVariable String oltpRef) {

		Oltp oltp = serviceInterface.getOltpObjRef(oltpRef);
		if (oltp == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "The oltp Object doesn't exists with the given refrence");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}
		if ("Y".equalsIgnoreCase(oltp.getIsconfig()) || oltp.getIsconfig() == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the OLTP refrence");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}

		String url = "http://localhost:" + oltp.getPort() + "/resetToDefault";
		RestTemplate restTemplate = new RestTemplate();
		String output = restTemplate.getForObject(url, String.class);

		return new ResponseEntity<>(output, HttpStatus.OK);
	}

	@PostMapping("updateFeatureKeys")
	public ResponseEntity<Object> updateFeatureKeys(@RequestBody FeatureKeyObject featureKeyObject) {

		Oltp oltp = serviceInterface.getOltpObjRef(featureKeyObject.getOltpRef());
		if (oltp == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "The oltp Object doesn't exists with the given refrence");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}
		if ("Y".equalsIgnoreCase(oltp.getIsconfig()) || oltp.getIsconfig() == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the OLTP refrence");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}

		String url;
		RestTemplate restTemplate = new RestTemplate();
		Map<String, String> args = new HashMap<String, String>();

		UpdateFeatureKey updateFeatureKey = new UpdateFeatureKey();

		try {
			switch (featureKeyObject.getTableName().toUpperCase()) {

				case "PARAM_ACL":
					url = "http://localhost:" + oltp.getPort() + "/getParamAcl";
					args.put("keyId", featureKeyObject.getKeyId().toString());
					ParamAcl paramAcl = restTemplate.postForObject(url, args, ParamAcl.class);
					updateFeatureKey.setTableName("PARAM_ACL");
					updateFeatureKey.setValue(paramAcl.getAclValue());
					updateFeatureKey.setKeyId(paramAcl.getParamAclId());
					args.put("keyValue", featureKeyObject.getKeyValue());
					paramAcl = restTemplate.postForObject("http://localhost:" + oltp.getPort() + "/updateParamAcl",
							args, ParamAcl.class);
					break;

				case "PARAM_KEY":
					url = "http://localhost:" + oltp.getPort() + "/getParamKey";
					args.put("keyId", featureKeyObject.getKeyId().toString());
					ParamKey paramKey = restTemplate.postForObject(url, args, ParamKey.class);
					updateFeatureKey.setTableName("PARAM_KEY");
					updateFeatureKey.setValue(paramKey.getDefaultValue());
					updateFeatureKey.setKeyId(paramKey.getParamKeyId());
					args.put("keyValue", featureKeyObject.getKeyValue());
					paramKey = restTemplate.postForObject("http://localhost:" + oltp.getPort() + "/updateParamKey",
							args, ParamKey.class);
					break;

				case "COB_PARAM":
					if (featureKeyObject.getCobrandId() == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "Please provide the Cobrand Id");
						return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
					}
					url = "http://localhost:" + oltp.getPort() + "/getCobParam";
					args.put("keyId", featureKeyObject.getKeyId().toString());
					args.put("cobrandId", featureKeyObject.getCobrandId().toString());
					CobParam cobParam = restTemplate.postForObject(url, args, CobParam.class);
					updateFeatureKey.setTableName("COB_PARAM");
					updateFeatureKey.setValue(cobParam.getParamValue());
					updateFeatureKey.setKeyId(cobParam.getCobParamId());
					args.put("keyValue", featureKeyObject.getKeyValue());
					args.put("keyId", cobParam.getCobParamId().toString());
					cobParam = restTemplate.postForObject("http://localhost:" + oltp.getPort() + "/updateCobParam",
							args, CobParam.class);
					break;

				case "COBRAND_ACL_VALUE":
					if (featureKeyObject.getCobrandId() == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "Please provide the Cobrand Id");
						return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
					}
					args.put("cobrandId", featureKeyObject.getCobrandId().toString());
					args.put("keyId", featureKeyObject.getKeyId().toString());
					url = "http://localhost:" + oltp.getPort() + "/getCobrandAclValue";
					CobrandAclValue cobrandAclValue = restTemplate.postForObject(url, args, CobrandAclValue.class);
					updateFeatureKey.setTableName("COBRAND_ACL_VALUE");
					updateFeatureKey.setValue(cobrandAclValue.getAclValue());
					updateFeatureKey.setKeyId(cobrandAclValue.getCobrandAclValueId());
					args.put("keyValue", featureKeyObject.getKeyValue());
					args.put("keyId", cobrandAclValue.getCobrandAclValueId().toString());
					cobrandAclValue = restTemplate.postForObject(
							"http://localhost:" + oltp.getPort() + "/updateCobrandAclValue", args,
							CobrandAclValue.class);
					break;

				case "ATTRIBUTE_COB_CONFIG":
					if (featureKeyObject.getCobrandId() == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "Please provide the Cobrand Id");
						return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
					}
					args.put("cobrandId", featureKeyObject.getCobrandId().toString());
					args.put("keyId", featureKeyObject.getKeyId().toString());
					url = "http://localhost:" + oltp.getPort() + "/getAttributeCobConfig";
					AttributeData attributeData = restTemplate.postForObject(url, args, AttributeData.class);
					updateFeatureKey.setTableName("ATTRIBUTE_COB_CONFIG");
					updateFeatureKey.setValue(attributeData.getConfigs());
					updateFeatureKey.setKeyId(attributeData.getAttributeCobConfigId());
					args.put("keyValue", featureKeyObject.getKeyValue());
					args.put("keyId", attributeData.getAttributeCobConfigId().toString());
					attributeData = restTemplate.postForObject(
							"http://localhost:" + oltp.getPort() + "/updateAttributeCobConfig", args,
							AttributeData.class);
					break;

				default:
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please provide the Correct Table Name");
					return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
			}
		} catch (HttpClientErrorException e) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "No Values present for the given parameters");
			updateFeatureKey = null;
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}

		updateFeatureKey.setInstanceRef(oltp.getOltpref());
		updateFeatureKey.setUpdateTime(System.currentTimeMillis());
		updateFeatureKey.setRevertTime(0L);

		serviceInterface.saveUpdateFeatureKey(updateFeatureKey);

		return new ResponseEntity<>("done", HttpStatus.OK);
	}

	@GetMapping("listStaticTables")
	public List<String> listOfStaticTables() {

		List<Sqlverdict> listSqlverdict = serviceInterface.getAlltables();

		List<String> tableNames = new ArrayList<String>();

		for (Sqlverdict sqlverdict : listSqlverdict) {
			tableNames.add(sqlverdict.getTablename());
		}
		return tableNames;
	}

	@GetMapping("checkTableName/{tableName}")
	public Sqlverdict checkTableName(@PathVariable String tableName) {

		return serviceInterface.getSqlverdictByTName(tableName.toUpperCase());
	}

	@GetMapping("addTableName/{tableName}")
	public Sqlverdict addTableName(@PathVariable String tableName) {

		Sqlverdict test = serviceInterface.getSqlverdictByTName(tableName.toUpperCase());
		if (test == null) {
			Sqlverdict sqlverdict = new Sqlverdict();
			sqlverdict.setTablename(tableName.toUpperCase());
			return serviceInterface.addSqlverdict(sqlverdict);
		} else
			return test;
	}

	@GetMapping("deleteTableName/{tableName}")
	public Sqlverdict deleteTableName(@PathVariable String tableName) {

		Sqlverdict sqlverdict = serviceInterface.getSqlverdictByTName(tableName.toUpperCase());
		if (sqlverdict == null)
			return sqlverdict;
		else
			return serviceInterface.deleteSqlverdictByTName(sqlverdict);
	}

}
