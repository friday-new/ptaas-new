/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */

package com.yodlee.perf.fridaynew.controller;

import java.util.HashMap;
import java.util.Map;

import com.yodlee.perf.fridaynew.model.Components;
import com.yodlee.perf.fridaynew.model.Configfeaturekey;
import com.yodlee.perf.fridaynew.model.Oltp;
import com.yodlee.perf.fridaynew.model.References;
import com.yodlee.perf.fridaynew.model.request.UpdateConfigValueReq;
import com.yodlee.perf.fridaynew.service.ServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

//@Api(value = "Ptaas Config DB Related API Info", description = "Config DB related API for the Team Reference", produces = "application/json")
@RestController
public class ConfigDbRelatedController {

	@Autowired
	private ServiceInterface serviceInterface;

	@SuppressWarnings("unchecked")
	/*
	 * @ApiOperation(value =
	 * "Get Config Group ID api to get the config group id for the instance in the config db"
	 * , notes = "Verify the object in the database, " +
	 * "Get the Config group id based on config module id, instance id in the config db"
	 * )
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, message =
	 * "Config group id returned", response = Map.class),
	 * 
	 * @ApiResponse(code = 400, message = "Given Object is not proper", response =
	 * Map.class),
	 * 
	 * @ApiResponse(code = 403, message = "Given Object is already in use", response
	 * = Map.class),
	 * 
	 * @ApiResponse(code = 404, message = "Given Object is not present", response =
	 * Map.class)
	 * 
	 * })
	 */
	@GetMapping("getConfigGroupId/{instanceref}/{oltpref}")
	public ResponseEntity<Object> getConfigGroupId(@PathVariable String instanceref, @PathVariable String oltpref) {

		Oltp oltp = serviceInterface.getOltpObjRef(oltpref);
		Map<String, String> map = new HashMap<String, String>();
		if (oltp == null) {
			map.put("message", "Please provide the correct input");
			return new ResponseEntity<Object>(map, HttpStatus.NOT_FOUND);
		}
		if ("N".equalsIgnoreCase(oltp.getIsconfig()) || oltp.getIsconfig() == null) {
			map.put("message", "Please provide the Config DB refrence only");
			return new ResponseEntity<Object>(map, HttpStatus.FORBIDDEN);
		}

		String str[] = instanceref.split("_");

		String myComponent = ControllerClass.allObjects.get(str[0]);
		if (myComponent == null) {
			map.put("message", "Please provide the correct input");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}

		Components components = serviceInterface.getComponentsObjRef(instanceref);
		if (components == null) {
			map.put("message", "No " + str[0] + " Object is present with given Reference");
			return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
		}

		String machineId = components.getServiceip() != null ? components.getServiceip() : null;
		String instanceId = components.getJbossinstance() != null ? components.getJbossinstance().toString() : null;
		References references = serviceInterface.getReferencesByInstance(str[0]);
		String moduleId = references.getConfigmoduleid() != null ? references.getConfigmoduleid().toString() : null;
		Map<String, String> args = new HashMap<String, String>();
		args.put("machineId", machineId);
		args.put("moduleInstanceId", instanceId);
		args.put("configModuleId", moduleId);

		RestTemplate restTemplate = new RestTemplate();
		Map<String, String> result = null;
		if (machineId != null && instanceId != null && moduleId != null) {
			try {
				result = restTemplate.postForObject("http://localhost:" + oltp.getPort() + "/findConfigGroupId", args,
						Map.class);
			} catch (HttpClientErrorException e) {
				map.put("message", "No Config Group Id is present");
				return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@GetMapping("getConfigGroupDetails/{oltpref}/{configGroupId}")
	public ResponseEntity<Object> getConfigGroupDetails(@PathVariable String oltpref,
			@PathVariable Long configGroupId) {

		Oltp oltp = serviceInterface.getOltpObjRef(oltpref);
		Map<String, String> map = new HashMap<String, String>();
		if (oltp == null) {
			map.put("message", "The oltp Object doesn't exists with the given refrence");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}
		if ("N".equalsIgnoreCase(oltp.getIsconfig()) || oltp.getIsconfig() == null) {
			map.put("message", "Please provide the OLTP refrence");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}

		RestTemplate restTemplate = new RestTemplate();
		String result = null;

		try {
			result = restTemplate.getForObject(
					"http://localhost:" + oltp.getPort() + "/getConfigGroupIdDetails/" + configGroupId, String.class);
		} catch (HttpClientErrorException e) {
			map.put("message", "No Config Group Id is present");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@PostMapping("updateConfigValue")
	public ResponseEntity<Object> updateConfigValue(@RequestBody UpdateConfigValueReq updateConfigValueReq) {

		Oltp oltp = serviceInterface.getOltpObjRef(updateConfigValueReq.getOltpRef());
		if (oltp == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "The oltp Object doesn't exists with the given refrence");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}
		if ("N".equalsIgnoreCase(oltp.getIsconfig()) || oltp.getIsconfig() == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the OLTP refrence");
			return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		}

		Map<String, String> args = new HashMap<String, String>();
		args.put("configGroupId", updateConfigValueReq.getConfigGroupId().toString());
		args.put("configKey", updateConfigValueReq.getConfigKey());
		args.put("configValue", updateConfigValueReq.getConfigValue());

		RestTemplate restTemplate = new RestTemplate();
		String result = null;
		Map<String, String> map = new HashMap<String, String>();

		try {
			result = restTemplate.postForObject("http://localhost:" + oltp.getPort() + "/updateConfigValue", args,
					String.class);

			Configfeaturekey configfeaturekey = new Configfeaturekey();
			configfeaturekey.setInstanceref(oltp.getOltpref());
			configfeaturekey.setConfigvalue(result);
			configfeaturekey.setConfigkey(updateConfigValueReq.getConfigKey());
			configfeaturekey.setConfiggroupid(updateConfigValueReq.getConfigGroupId().toString());
			configfeaturekey.setUpdatetime(System.currentTimeMillis());
			configfeaturekey.setReverttime(0L);

			serviceInterface.saveConfigfeaturekey(configfeaturekey);

		} catch (HttpClientErrorException e) {
			map.put("message", "No Entry is present for the given Config Group Id and config key");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		} catch (HttpServerErrorException e) {
			map.put("message", "There are more than One entry is present for the given Config Group Id and config key");
			return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		map.put("message", result);
		return new ResponseEntity<>(map, HttpStatus.OK);

	}

}
