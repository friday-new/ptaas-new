/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */

package com.yodlee.perf.fridaynew.controller;

import java.util.HashMap;
import java.util.Map;

import com.yodlee.perf.fridaynew.model.Components;
import com.yodlee.perf.fridaynew.model.Gatherer;
import com.yodlee.perf.fridaynew.model.Simulator;
import com.yodlee.perf.fridaynew.model.request.CopyLogsRequest;
import com.yodlee.perf.fridaynew.service.ServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//@Api(value = "Ptaas Instance Related API Info", description = "Instance related API for the Team Reference", produces = "application/json")
@RestController
public class InstanceRelatedController {

	@Autowired
	private ServiceInterface serviceInterface;

	/*
	 * @ApiOperation(value = "Log Purge API to Nullify the previous Execution Logs",
	 * notes = "Verify the object in the database, " +
	 * "and nullify the previous Execution Logs")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, message =
	 * "Logs purged successfully", response = Map.class),
	 * 
	 * @ApiResponse(code = 400, message = "Given Object is not proper", response =
	 * Map.class),
	 * 
	 * @ApiResponse(code = 403, message = "Couldn't able to purge the logs",
	 * response = Map.class),
	 * 
	 * @ApiResponse(code = 404, message = "Given Object is not present", response =
	 * Map.class) })
	 */
	@GetMapping("logPurge/{instanceRef}")
	public ResponseEntity<Object> logPurge(@PathVariable String instanceRef) {
		String str[] = instanceRef.split("_");

		String myComponent = ControllerClass.allObjects.get(str[0]);
		if (myComponent == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the correct input");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}
		Components components = serviceInterface.getComponentsObjRef(instanceRef);
		if (components == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "No " + str[0] + " Object is present with given Reference");
			return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
		}
		return components.logPurge();
	}

	/*
	 * @ApiOperation(value =
	 * "Stop Component API is used to stop the given Instance based on instance reference"
	 * , notes = "Verify the object in the database, " + "and stop the Instance")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, message =
	 * "Stopped Instance Successfully", response = Map.class),
	 * 
	 * @ApiResponse(code = 400, message = "Given Object is not proper", response =
	 * Map.class),
	 * 
	 * @ApiResponse(code = 403, message = "Couldn't able to Stop the Instance",
	 * response = Map.class),
	 * 
	 * @ApiResponse(code = 404, message = "Given Object is not present", response =
	 * Map.class) })
	 */
	@GetMapping("stopComponent/{instanceRef}")
	public ResponseEntity<Object> stopComponent(@PathVariable String instanceRef) {
		String str[] = instanceRef.split("_");

		String myComponent = ControllerClass.allObjects.get(str[0]);
		if (myComponent == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the correct input");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}
		if (myComponent.equals("Y")) {
			Components components = serviceInterface.getComponentsObjRef(instanceRef);
			if (components == null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "No " + str[0] + " Object is present with given Reference");
				return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}
			return components.stopComponent();
		} else {
			switch (str[0]) {
				case "Gatherer":
					Gatherer gatherer = serviceInterface.getGathererObjRef(instanceRef);
					if (gatherer == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					return gatherer.stopComponent();
				case "Simulator":
					Simulator simulator = serviceInterface.getSimulatorObjRef(instanceRef);
					if (simulator == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					return simulator.stopComponent();
				default:
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please Provide the correct input");
					return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}
		}
	}

	/*
	 * @ApiOperation(value =
	 * "Start Component API is used to start the given Instance based on instance reference"
	 * , notes = "Verify the object in the database, " +
	 * "and start the Instance with the given number of containers(default is 1).")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, message =
	 * "Started Instance Successfully", response = Map.class),
	 * 
	 * @ApiResponse(code = 400, message = "Given Object is not proper", response =
	 * Map.class),
	 * 
	 * @ApiResponse(code = 403, message = "Couldn't able to Start the Instance",
	 * response = Map.class),
	 * 
	 * @ApiResponse(code = 404, message = "Given Object is not present", response =
	 * Map.class) })
	 */
	@GetMapping("startComponent/{instanceRef}/{numOfContainers}")
	public ResponseEntity<Object> startComponent(@PathVariable String instanceRef,
			@PathVariable Integer numOfContainers) {
		String str[] = instanceRef.split("_");

		String myComponent = ControllerClass.allObjects.get(str[0]);
		if (myComponent == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the correct input");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}
		if (myComponent.equals("Y")) {
			Components components = serviceInterface.getComponentsObjRef(instanceRef);
			if (components == null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "No " + str[0] + " Object is present with given Reference");
				return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}
			if (numOfContainers == null || numOfContainers == 0)
				numOfContainers = 1;

			return components.startComponent(numOfContainers);
		} else {
			switch (str[0]) {
				case "Gatherer":
					Gatherer gatherer = serviceInterface.getGathererObjRef(instanceRef);
					if (gatherer == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					return gatherer.startComponent(1);
				case "Simulator":
					Simulator simulator = serviceInterface.getSimulatorObjRef(instanceRef);
					if (simulator == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					return simulator.startComponent(1);
				default:
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please Provide the correct input");
					return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}
		}
	}

	@GetMapping("reStartComponent/{instanceRef}")
	public ResponseEntity<Object> reStartComponent(@PathVariable String instanceRef) {
		String str[] = instanceRef.split("_");

		String myComponent = ControllerClass.allObjects.get(str[0]);
		if (myComponent == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the correct input");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}
		if (myComponent.equals("Y")) {
			Components components = serviceInterface.getComponentsObjRef(instanceRef);
			if (components == null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "No " + str[0] + " Object is present with given Reference");
				return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}
			return components.reStartComponent();
		}
		return null;

	}

	/*
	 * @ApiOperation(value =
	 * "Start Component API is used to start the given Instance based on instance reference"
	 * , notes = "Verify the object in the database, " + "and start the Instance.")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, message =
	 * "Started Instance Successfully", response = Map.class),
	 * 
	 * @ApiResponse(code = 400, message = "Given Object is not proper", response =
	 * Map.class),
	 * 
	 * @ApiResponse(code = 403, message = "Couldn't able to Start the Instance",
	 * response = Map.class),
	 * 
	 * @ApiResponse(code = 404, message = "Given Object is not present", response =
	 * Map.class) })
	 */
	@GetMapping("startComponent/{instanceRef}")
	public ResponseEntity<Object> startComponentwithoutParam(@PathVariable String instanceRef) {

		return startComponent(instanceRef, null);
	}

	/*
	 * @ApiOperation(value =
	 * "Copy Logs API is used to copy the instance logs to the provided path", notes
	 * = "Verify the object in the database, " +
	 * "and copy the instance logs to the path")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, message =
	 * "Copied Logs Successfully", response = Map.class),
	 * 
	 * @ApiResponse(code = 400, message = "Given Object is not proper", response =
	 * Map.class),
	 * 
	 * @ApiResponse(code = 403, message = "Couldn't able to zip the Logs", response
	 * = Map.class),
	 * 
	 * @ApiResponse(code = 404, message = "Given Object is not present", response =
	 * Map.class) })
	 */
	@PostMapping("copyLogs")
	public ResponseEntity<Object> copyLogs(@RequestBody CopyLogsRequest copyLogsRequest) {
		String str[] = copyLogsRequest.getInstanceRef().split("_");

		String myComponent = ControllerClass.allObjects.get(str[0]);
		if (myComponent == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the correct input");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}
		Components components = serviceInterface.getComponentsObjRef(copyLogsRequest.getInstanceRef());
		if (components == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "No " + str[0] + " Object is present with given Reference");
			return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
		}
		return components.copyLogs(copyLogsRequest.getLogLocation() + "/Logs");
	}

	/*
	 * @ApiOperation(value =
	 * "Check Component API is used to check the status of the component", notes =
	 * "Verify the object in the database, " +
	 * "and check the component based on the pre-defined logs and declare the status."
	 * )
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, message = "Returns the status of the Component",
	 * response = ComponentResponse.class),
	 * 
	 * @ApiResponse(code = 400, message = "Given Object is not proper", response =
	 * Map.class),
	 * 
	 * @ApiResponse(code = 404, message = "Given Object is not present", response =
	 * Map.class) })
	 */
	@GetMapping("checkComponent/{instanceRef}")
	public ResponseEntity<Object> checkComponent(@PathVariable String instanceRef) {

		String str[] = instanceRef.split("_");

		String myComponent = ControllerClass.allObjects.get(str[0]);
		if (myComponent == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("message", "Please provide the correct input");
			return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
		}

		if (myComponent.equals("Y")) {
			Components components = serviceInterface.getComponentsObjRef(instanceRef);
			if (components == null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("message", "No " + str[0] + " Object is present with given Reference");
				return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}
			return components.checkComponent(ControllerClass.logRelated.get(str[0]));

		} else {
			switch (str[0]) {
				case "Simulator":
					Simulator simulator = serviceInterface.getSimulatorObjRef(instanceRef);
					if (simulator == null) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("message", "No " + str[0] + " Object is present with given Reference");
						return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
					}
					return simulator.checkComponent(null);
				default:
					Map<String, String> map = new HashMap<String, String>();
					map.put("message", "Please Provide the correct input");
					return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
			}
		}
	}
}
