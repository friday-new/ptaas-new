/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.service;

import java.util.List;

import com.yodlee.perf.fridaynew.model.Components;
import com.yodlee.perf.fridaynew.model.Configfeaturekey;
import com.yodlee.perf.fridaynew.model.Dbupgrade;
import com.yodlee.perf.fridaynew.model.Execmetricsummary;
import com.yodlee.perf.fridaynew.model.Exectracking;
import com.yodlee.perf.fridaynew.model.Gatherer;
import com.yodlee.perf.fridaynew.model.Jmeter;
import com.yodlee.perf.fridaynew.model.Mq;
import com.yodlee.perf.fridaynew.model.Oltp;
import com.yodlee.perf.fridaynew.model.References;
import com.yodlee.perf.fridaynew.model.Simulator;
import com.yodlee.perf.fridaynew.model.Sqlverdict;
import com.yodlee.perf.fridaynew.model.Taasexectracking;
import com.yodlee.perf.fridaynew.model.Testcaseverdict;
import com.yodlee.perf.fridaynew.model.UpdateFeatureKey;

public interface ServiceInterface {

	List<References> getAllComponents();

	References getReferencesByInstance(String name);

	Components getComponents(Integer componentsId);

	Components addComponents(Components components);

	Components getComponentsObjName(String componentsObjname);

	Components getComponentsObjRef(String componentsRef);

	List<Components> updateUserName(String username);

	Jmeter addJmeter(Jmeter jmeter);

	Jmeter getJmeterByname(String instancename);

	Exectracking addExectracking(Exectracking exectracking);

	Exectracking getExectracking(String exectrackingid);

	Exectracking getExectrackingbyfoldername(String execfolder);

	Oltp addOltp(Oltp oltp);

	Oltp getOltpObjName(String oltpname);

	Oltp getOltpObjRef(String oltpref);

	List<Oltp> getOltpbySchema(String oltpschema);

	Execmetricsummary getExecmetricsummary(Long execmetricsummaryId);

	List<Execmetricsummary> getExecmetricsummarybytestcaseexectrackingobjectname(String testcaseid, String exectracking,
			String objectname);

	List<Execmetricsummary> getExecmetricsummarybyexectracking(String exectracking);

	Execmetricsummary addExecmetricsummary(Execmetricsummary execmetricsummary);

	Execmetricsummary getExecmetricsummarybytestcaseexectrackingobjectnameinterval(String testcaseid,
			String exectracking, String objectname, Long interval);

	UpdateFeatureKey saveUpdateFeatureKey(UpdateFeatureKey updateFeatureKey);

	List<UpdateFeatureKey> getAllUpdateFeatureKey(String instanceRef);

	Configfeaturekey saveConfigfeaturekey(Configfeaturekey configfeaturekey);

	List<Configfeaturekey> getAllConfigfeaturekey(String instanceref);

	Gatherer getGatherer(Integer gid);

	Gatherer addGatherer(Gatherer gatherer);

	Gatherer getGathererObjName(String gobjname);

	Gatherer getGathererObjRef(String gref);

	Mq getMq(Integer mqid);

	Mq addMq(Mq mq);

	Mq getMqObjName(String mqobjname);

	Mq getMqObjRef(String mqref);

	List<Sqlverdict> getAlltables();

	Sqlverdict addSqlverdict(Sqlverdict sqlverdict);

	Sqlverdict getSqlverdictByTName(String tablename);

	Sqlverdict deleteSqlverdictByTName(Sqlverdict sqlverdict);

	List<Oltp> getOltpforTaas(String oltpipaddr, Integer oltpport, String projectname, String oltpusername);

	Taasexectracking saveTaasexecTracking(Taasexectracking taasexectracking);

	Taasexectracking getTaasexectracking(String oltpname);

	Dbupgrade getByDbname(String dbname);

	Testcaseverdict getTestcaseverdict(String testcaseid, String instancename, Long interval, Long active,
			String branch);

	Testcaseverdict getTestcaseVerdictById(Long id);

	List<Testcaseverdict> getTestcaseVerdictByTestcaseId(String testcaseid);

	List<Testcaseverdict> getTestcaseVerdictByTestcaseIdAndInstanceType(String testcaseid, String instanceType);

	Testcaseverdict addTestcaseverdict(Testcaseverdict testcaseverdict);

	Simulator getSimulatorObjName(String simulatorName);

	Simulator getSimulatorObjRef(String simulatorInstanceRef);

	Simulator getSimulator(Long simulatorId);

	Simulator addSimulator(Simulator simulator);

}
