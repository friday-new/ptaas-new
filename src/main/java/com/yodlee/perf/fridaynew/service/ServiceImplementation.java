/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.service;

import java.util.List;

import com.yodlee.perf.fridaynew.dao.ComponentsRepository;
import com.yodlee.perf.fridaynew.dao.ConfigfeaturekeyRepository;
import com.yodlee.perf.fridaynew.dao.DbupgradeRepository;
import com.yodlee.perf.fridaynew.dao.ExecmetricsummaryRepository;
import com.yodlee.perf.fridaynew.dao.ExectrackingRepository;
import com.yodlee.perf.fridaynew.dao.GathererRepository;
import com.yodlee.perf.fridaynew.dao.JmeterRepository;
import com.yodlee.perf.fridaynew.dao.MqRepository;
import com.yodlee.perf.fridaynew.dao.OltpRepository;
import com.yodlee.perf.fridaynew.dao.ReferencesRepository;
import com.yodlee.perf.fridaynew.dao.SimulatorRepository;
import com.yodlee.perf.fridaynew.dao.SqlverdictRepository;
import com.yodlee.perf.fridaynew.dao.TaasexectrackingRepository;
import com.yodlee.perf.fridaynew.dao.TestcaseverdictRepository;
import com.yodlee.perf.fridaynew.dao.UpdateFeatureKeyRepository;
import com.yodlee.perf.fridaynew.model.Components;
import com.yodlee.perf.fridaynew.model.Configfeaturekey;
import com.yodlee.perf.fridaynew.model.Dbupgrade;
import com.yodlee.perf.fridaynew.model.Execmetricsummary;
import com.yodlee.perf.fridaynew.model.Exectracking;
import com.yodlee.perf.fridaynew.model.Gatherer;
import com.yodlee.perf.fridaynew.model.Jmeter;
import com.yodlee.perf.fridaynew.model.Mq;
import com.yodlee.perf.fridaynew.model.Oltp;
import com.yodlee.perf.fridaynew.model.References;
import com.yodlee.perf.fridaynew.model.Simulator;
import com.yodlee.perf.fridaynew.model.Sqlverdict;
import com.yodlee.perf.fridaynew.model.Taasexectracking;
import com.yodlee.perf.fridaynew.model.Testcaseverdict;
import com.yodlee.perf.fridaynew.model.UpdateFeatureKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceImplementation implements ServiceInterface {

	@Autowired
	private ReferencesRepository referencesRepository;
	@Autowired
	private ComponentsRepository componentsRepository;
	@Autowired
	private JmeterRepository jmeterRepository;
	@Autowired
	private ExectrackingRepository exectrackingRepository;
	@Autowired
	private OltpRepository oltpRepository;
	@Autowired
	private ExecmetricsummaryRepository execmetricsummaryRepository;
	@Autowired
	private UpdateFeatureKeyRepository updateFeatureKeyRepository;
	@Autowired
	private ConfigfeaturekeyRepository configfeaturekeyRepository;
	@Autowired
	private TestcaseverdictRepository testcaseverdictRepository;
	@Autowired
	private GathererRepository gathererRepository;
	@Autowired
	private MqRepository mqRepository;
	@Autowired
	private SqlverdictRepository sqlverdictRepository;
	@Autowired
	private TaasexectrackingRepository taasexectrackingRepository;
	@Autowired
	private DbupgradeRepository dbupgradeRepository;
	@Autowired
	private SimulatorRepository simulatorRepository;

	@Override
	public List<References> getAllComponents() {
		return referencesRepository.findAll();
	}

	@Override
	public Components getComponents(Integer componentsId) {
		return componentsRepository.findById(componentsId).get();
	}

	@Override
	public Components addComponents(Components components) {
		return componentsRepository.save(components);
	}

	@Override
	public Components getComponentsObjName(String componentsObjname) {
		return componentsRepository.findByinstancename(componentsObjname);
	}

	@Override
	public Components getComponentsObjRef(String componentsRef) {
		return componentsRepository.findByinstanceref(componentsRef);
	}

	@Override
	public Jmeter addJmeter(Jmeter jmeter) {
		return jmeterRepository.save(jmeter);
	}

	@Override
	public Jmeter getJmeterByname(String instancename) {
		return jmeterRepository.findByinstancename(instancename);
	}

	@Override
	public Exectracking addExectracking(Exectracking exectracking) {
		return exectrackingRepository.save(exectracking);
	}

	@Override
	public Exectracking getExectracking(String exectrackingid) {
		return exectrackingRepository.findById(exectrackingid).get();
	}

	@Override
	public Oltp addOltp(Oltp oltp) {
		return oltpRepository.save(oltp);
	}

	@Override
	public Oltp getOltpObjName(String oltpname) {
		return oltpRepository.findByoltpname(oltpname);
	}

	@Override
	public Oltp getOltpObjRef(String oltpref) {
		return oltpRepository.findByoltpref(oltpref);
	}

	@Override
	public Execmetricsummary getExecmetricsummary(Long execmetricsummaryId) {
		return execmetricsummaryRepository.findById(execmetricsummaryId).get();
	}

	@Override
	public Exectracking getExectrackingbyfoldername(String execfolder) {
		return exectrackingRepository.findByExecfolderContainingIgnoreCase(execfolder);
	}

	@Override
	public List<Execmetricsummary> getExecmetricsummarybytestcaseexectrackingobjectname(String testcaseid,
			String exectracking, String objectname) {
		return execmetricsummaryRepository.findByTestcaseidIgnoreCaseAndExectrackingIgnoreCaseAndObjectnameIgnoreCase(
				testcaseid, exectracking, objectname);
	}

	@Override
	public Execmetricsummary addExecmetricsummary(Execmetricsummary execmetricsummary) {
		return execmetricsummaryRepository.save(execmetricsummary);
	}

	@Override
	public Execmetricsummary getExecmetricsummarybytestcaseexectrackingobjectnameinterval(String testcaseid,
			String exectracking, String objectname, Long interval) {
		return execmetricsummaryRepository
				.findByTestcaseidIgnoreCaseAndExectrackingIgnoreCaseAndObjectnameIgnoreCaseAndInterval(testcaseid,
						exectracking, objectname, interval);
	}

	@Override
	public UpdateFeatureKey saveUpdateFeatureKey(UpdateFeatureKey updateFeatureKey) {
		return updateFeatureKeyRepository.save(updateFeatureKey);
	}

	@Override
	public List<UpdateFeatureKey> getAllUpdateFeatureKey(String instanceRef) {
		return updateFeatureKeyRepository.findByInstanceRef(instanceRef);
	}

	@Override
	public Configfeaturekey saveConfigfeaturekey(Configfeaturekey configfeaturekey) {
		return configfeaturekeyRepository.save(configfeaturekey);
	}

	@Override
	public List<Configfeaturekey> getAllConfigfeaturekey(String instanceref) {
		return configfeaturekeyRepository.findByInstanceref(instanceref);
	}

	@Override
	public References getReferencesByInstance(String name) {
		return referencesRepository.findByName(name);
	}

	@Override
	public Testcaseverdict getTestcaseverdict(String testcaseid, String instancename, Long interval, Long active,
			String branch) {
		return testcaseverdictRepository
				.findByTestcaseidIgnoreCaseAndInstancenameAndIntervalAndActiveAndBranchIgnoreCase(testcaseid,
						instancename, interval, active, branch);
	}

	@Override
	public List<Execmetricsummary> getExecmetricsummarybyexectracking(String exectracking) {
		return execmetricsummaryRepository.findByExectrackingIgnoreCase(exectracking);
	}

	@Override
	public Gatherer getGatherer(Integer gid) {
		return gathererRepository.findById(gid).get();
	}

	@Override
	public Gatherer addGatherer(Gatherer gatherer) {
		return gathererRepository.save(gatherer);
	}

	@Override
	public Gatherer getGathererObjName(String gobjname) {
		return gathererRepository.findBygobjname(gobjname);
	}

	@Override
	public Gatherer getGathererObjRef(String gref) {
		return gathererRepository.findBygref(gref);
	}

	@Override
	public Mq getMq(Integer mqid) {
		return mqRepository.findById(mqid).get();
	}

	@Override
	public Mq addMq(Mq mq) {
		return mqRepository.save(mq);
	}

	@Override
	public Mq getMqObjName(String mqobjname) {
		return mqRepository.findBymqobjname(mqobjname);
	}

	@Override
	public Mq getMqObjRef(String mqref) {
		return mqRepository.findBymqref(mqref);
	}

	@Override
	public List<Sqlverdict> getAlltables() {
		return sqlverdictRepository.findAll();
	}

	@Override
	public Sqlverdict addSqlverdict(Sqlverdict sqlverdict) {
		return sqlverdictRepository.save(sqlverdict);
	}

	@Override
	public Sqlverdict getSqlverdictByTName(String tablename) {
		return sqlverdictRepository.findByTablename(tablename);
	}

	@Override
	public Sqlverdict deleteSqlverdictByTName(Sqlverdict sqlverdict) {
		sqlverdictRepository.delete(sqlverdict);
		return sqlverdict;
	}

	@Override
	public List<Oltp> getOltpforTaas(String oltpipaddr, Integer oltpport, String projectname, String oltpusername) {

		return oltpRepository.findByOltpipaddrAndOltpportAndProjectnameAndOltpusernameAndIsassignedAndOltpinuse(
				oltpipaddr, oltpport, projectname, oltpusername, "N", "N");
	}

	@Override
	public Taasexectracking saveTaasexecTracking(Taasexectracking taasexectracking) {
		return taasexectrackingRepository.save(taasexectracking);
	}

	@Override
	public Taasexectracking getTaasexectracking(String oltpname) {
		return taasexectrackingRepository.findByInstancenameAndIsdeleted(oltpname, "N");
	}

	@Override
	public List<Components> updateUserName(String username) {
		return componentsRepository.findByUsername(username);
	}

	@Override
	public List<Oltp> getOltpbySchema(String oltpschema) {
		return oltpRepository.findByoltpschema(oltpschema);
	}

	@Override
	public Dbupgrade getByDbname(String dbname) {
		return dbupgradeRepository.findByDbname(dbname);
	}

	@Override
	public Testcaseverdict getTestcaseVerdictById(Long id) {
		return testcaseverdictRepository.findById(id).get();
	}

	@Override
	public List<Testcaseverdict> getTestcaseVerdictByTestcaseId(String testcaseid) {
		return testcaseverdictRepository.findByTestcaseidIgnoreCaseAndActive(testcaseid, 1L);
	}

	@Override
	public List<Testcaseverdict> getTestcaseVerdictByTestcaseIdAndInstanceType(String testcaseid, String instanceType) {
		return testcaseverdictRepository.findByTestcaseidIgnoreCaseAndInstancenameAndActive(testcaseid, instanceType,
				1L);
	}

	@Override
	public Testcaseverdict addTestcaseverdict(Testcaseverdict testcaseverdict) {
		return testcaseverdictRepository.save(testcaseverdict);
	}

	@Override
	public Simulator getSimulatorObjName(String simulatorName) {
		return simulatorRepository.findBysimulatorName(simulatorName);
	}

	@Override
	public Simulator getSimulatorObjRef(String simulatorInstanceRef) {
		return simulatorRepository.findBysimulatorInstanceRef(simulatorInstanceRef);
	}

	@Override
	public Simulator getSimulator(Long simulatorId) {
		return simulatorRepository.findById(simulatorId).get();
	}

	@Override
	public Simulator addSimulator(Simulator simulator) {
		return simulatorRepository.save(simulator);
	}

}
