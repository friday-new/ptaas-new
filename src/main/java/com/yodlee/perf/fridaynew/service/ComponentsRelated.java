/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.service;

import java.io.IOException;
import java.io.InputStream;

public class ComponentsRelated {

	public String purgeLogs(String user, String hostname, String password, String logpath, String logpattern,
			String isDocker) {

		Process process = null;
		String finalString = null;
		try {
			if (isDocker.equalsIgnoreCase("N")) {
				process = Runtime.getRuntime().exec("/backup/Users/caradhya/RootUserFiles/Logpurge.sh " + user + " "
						+ hostname + " " + password + " " + logpath + " " + logpattern);
			} else {
				process = Runtime.getRuntime().exec("/backup/Users/caradhya/RootUserFiles/DockerLogpurge.sh " + user
						+ " " + hostname + " " + password + " " + logpath + " " + logpattern);
			}

			InputStream inputStream = process.getInputStream();

			int i = 0;
			StringBuffer sb = new StringBuffer();
			String temp = null;
			while ((i = inputStream.read()) != -1)
				sb.append((char) i);
			temp = sb.toString();
			String[] temp1 = temp.split("\n");
			finalString = temp1[temp1.length - 2].replace("\n", "").replace("\r", "");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return finalString;
	}

	public String startOrStopComponent(String user, String hostname, String password, String filepath, String filename,
			String isDocker, String projectName, String clusterName, String nameSpaceId, String activePool,
			String dockerId, String value, Integer numOfContainers, String service) {

		Process process = null;
		String finalString = null;
		String temp = null;
		try {
			if (isDocker.equalsIgnoreCase("N")) {
				process = Runtime.getRuntime().exec("/backup/Users/caradhya/RootUserFiles/StartorStopComponent.sh "
						+ user + " " + hostname + " " + password + " " + filepath + " " + filename);
			} else {
				process = Runtime.getRuntime()
						.exec("sh /backup/Users/caradhya/RootUserFiles/DockerStartorStopComponent.sh " + projectName
								+ " " + clusterName + " " + nameSpaceId + " " + activePool + " " + dockerId + " "
								+ value + " " + numOfContainers + " " + service);
			}

			InputStream inputStream = process.getInputStream();

			int i = 0;
			StringBuffer sb = new StringBuffer();

			while ((i = inputStream.read()) != -1)
				sb.append((char) i);
			temp = sb.toString();
			String[] temp1 = temp.split("\n");
			for (String dummy : temp1) {
				if (dummy.regionMatches(0, "Component", 0, 8))
					finalString = dummy.split(",")[1];
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		finalString = finalString.replaceAll("\\r\\n|\\r|\\n", "");
		return finalString;
	}

	public String zipLogs(String user, String hostname, String password, String logpath, String logpattern,
			String logtype, String instancename, String isDocker) {
		Process process = null;
		String finalString = null;
		try {

			if (isDocker.equalsIgnoreCase("N")) {
				process = Runtime.getRuntime()
						.exec("/backup/Users/caradhya/RootUserFiles/Loggzip.sh " + user + " " + hostname + " "
								+ password + " " + logpath + " " + logpattern + " " + logtype + " " + instancename);
			} else {
				process = Runtime.getRuntime()
						.exec("/backup/Users/caradhya/RootUserFiles/DockerLoggzip.sh " + user + " " + hostname + " "
								+ password + " " + logpath + " " + logpattern + " " + logtype + " " + instancename);
			}

			InputStream inputStream = process.getInputStream();

			int i = 0;
			StringBuffer sb = new StringBuffer();
			String temp = null;
			while ((i = inputStream.read()) != -1)
				sb.append((char) i);
			temp = sb.toString();
			String[] temp1 = temp.split("\n");
			finalString = temp1[temp1.length - 2].replace("\n", "").replace("\r", "");
		} catch (IOException e) {
			e.printStackTrace();
		}

		return finalString;
	}

	public void copyFile(String user, String hostname, String password, String sourceLocation,
			String destinationLocation) {

		try {
			Runtime.getRuntime().exec("/backup/Users/caradhya/RootUserFiles/CopyFile.sh " + user + " " + hostname + " "
					+ password + " " + sourceLocation + " " + destinationLocation);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
