/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.yodlee.perf.fridaynew.configuration.ConfigurationClass;
import com.yodlee.perf.fridaynew.model.Data;
import com.yodlee.perf.fridaynew.model.Execmetricsummary;
import com.yodlee.perf.fridaynew.model.Testcaseverdict;
import com.yodlee.perf.fridaynew.model.request.HeapUsedVerdict;
import com.yodlee.perf.fridaynew.model.request.MemoryVerdict;
import com.yodlee.perf.fridaynew.model.request.ProcessCpuVerdict;
import com.yodlee.perf.fridaynew.model.request.TestcaseverdictRequest;
import com.yodlee.perf.fridaynew.model.request.TpsVerdict;
import com.yodlee.perf.fridaynew.model.request.VmCpuVerdict;

import org.springframework.beans.factory.annotation.Autowired;

public class ServiceClass {

	@Autowired
	private ConfigurationClass configurationClass;

	private static final String newLine = System.getProperty("line.separator");

	public String zabbixReportGenerate(int elementId, String startTime, String endTime, String instanceName,
			String destination) {

		try {
			Runtime.getRuntime().exec("sh /backup/Users/caradhya/RootUserFiles/GenerateZabbixReport.sh " + elementId
					+ " " + startTime + " " + endTime + " " + instanceName + " " + destination + "/Reports");
			return "Completed";
		} catch (IOException e) {
			e.printStackTrace();
			return "Some issue";
		}
	}

	public void reportUpdate(String str, String filename) {

		PrintWriter printWriter = null;
		File file = new File(filename);
		try {
			if (!file.exists())
				file.createNewFile();
			printWriter = new PrintWriter(new FileOutputStream(filename, true));
			printWriter.write(newLine + str);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			if (printWriter != null) {
				printWriter.flush();
				printWriter.close();
			}

		}
	}

	public Map<String, String> specificationUpdate(String userName, String serviceIp, String password,
			String destination, String instanceName) {

		String mainString = executeShellCommand("/backup/Users/caradhya/RootUserFiles/UpdateSystemSpecification.sh "
				+ userName + " " + serviceIp + " " + password);
		String CPUCOUNT = null, MEMORY = null;

		reportUpdate(mainString, destination + "/Reports/" + instanceName + "_Specification.txt");

		String[] temp = mainString.split("\n");

		for (String string : temp) {
			if (string.regionMatches(0, "CPUCOUNT", 0, 8))
				CPUCOUNT = string.split(",")[1];
			if (string.regionMatches(0, "MEMORY", 0, 6))
				MEMORY = string.split(",")[1];
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("cpu", CPUCOUNT);
		map.put("memory", MEMORY);
		map.put("status", "Completed");
		return map;
	}

	public int checkComponent(String user, String hostname, String password, String filepath, String filename,
			String stringTobeTest, long number) {

		Process process = null;
		String count = null;
		String testString = "'" + stringTobeTest + "'";

		try {
			process = Runtime.getRuntime().exec(new String[] { "/backup/Users/caradhya/RootUserFiles/CheckComponent.sh",
					user, hostname, password, filepath, filename, testString });

			InputStream inputStream = process.getInputStream();
			int i = 0;
			StringBuffer sb = new StringBuffer();
			String temp = null;
			while ((i = inputStream.read()) != -1)
				sb.append((char) i);

			temp = sb.toString();

			if (configurationClass.isPrintLog())
				System.out.println(temp);

			String[] temp1 = temp.split("\n");

			for (int j = 0; j < temp1.length; j++) {
				if (temp1[j].contains("grep")) {
					count = temp1[j + 1].replace("\n", "").replace("\r", "");
					break;
				} else
					count = "0";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		int num = 0;
		try {
			num = Integer.parseInt(count);
		} catch (Exception e) {
			if (number > (System.currentTimeMillis() + 1800000))
				num = 1;
		}
		return num;
	}

	public Long tsToSec8601(String timestamp) {
		if (timestamp == null)
			return null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date dt = sdf.parse(timestamp);
			long epoch = dt.getTime();
			return (long) (epoch / 1000);
		} catch (ParseException e) {
			return null;
		}
	}

	public String collectFTSQueries(String userName, String SchemaName, String password, Integer startId, Integer endId,
			String folder) {

		String mainString = executeShellCommand("sh /backup/Users/caradhya/RootUserFiles/CollectFTSQueries.sh "
				+ userName + " " + SchemaName + " " + password + " " + startId + " " + endId);

		reportUpdate(mainString, folder + "/collectFTSQueries.txt");
		return "Completed";
	}

	public String compareNumbers(String num, String mainNum) {

		if (mainNum.equalsIgnoreCase("null")) {
			return "Ambiguous";
		}

		Double Num = Double.valueOf(num);
		Double MainNum = Double.valueOf(mainNum);

		Double MainNumPlus = MainNum + MainNum * 0.05;
		Double MainNumMinus = MainNum - MainNum * 0.05;

		if (Num >= MainNumMinus && Num <= MainNumPlus)
			return "Pass";
		else
			return "Fail";
	}

	public String compareArrayNumbers(String num, List<String> list) {

		if (list.size() == 1)
			return compareNumbers(num, list.get(0));

		else {
			Double minNumber = null, maxNumber = null;
			Double Num = Double.valueOf(num);
			Double Num1 = Double.valueOf(list.get(0));
			Double Num2 = Double.valueOf(list.get(1));

			if (Num1 > Num2) {
				minNumber = Num2;
				maxNumber = Num1;
			} else {
				minNumber = Num1;
				maxNumber = Num2;
			}

			if (Num >= minNumber && Num <= maxNumber)
				return "Pass";
			else
				return "Fail";
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Map<String, Data>> compareWithTestData(Execmetricsummary execmetricsummary,
			Testcaseverdict testcaseverdict) {

		Gson gson = new Gson();

		DecimalFormat decimalFormat = new DecimalFormat("#0.0");
		Map<String, Data> finalCpuData = new HashMap<String, Data>();
		Map<String, Data> finalMemoryData = new HashMap<String, Data>();
		Map<String, Data> finalProcessCpuData = new HashMap<String, Data>();
		Map<String, Data> finalHeapUsedData = new HashMap<String, Data>();
		Map<String, String> vmcpu = gson.fromJson(execmetricsummary.getVmcpu(), Map.class);
		Map<String, String> memory = gson.fromJson(execmetricsummary.getMemory(), Map.class);
		Map<String, String> processCpu = gson.fromJson(execmetricsummary.getProcesscpu(), Map.class);
		Map<String, String> heapUsed = gson.fromJson(execmetricsummary.getHeapused(), Map.class);

		if (testcaseverdict == null) {

			try {
				finalCpuData.put("Average",
						new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("avg"))) + " %", "Fail"));
				finalCpuData.put("Min",
						new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("min"))) + " %", "Fail"));
				finalCpuData.put("Max",
						new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("max"))) + " %", "Fail"));
				finalCpuData.put("Nintyfivepercentile", new Data(
						decimalFormat.format(Double.parseDouble(vmcpu.get("nintyfivepercentile"))) + " %", "Fail"));
				finalCpuData.put("Nintypercentile", new Data(
						decimalFormat.format(Double.parseDouble(vmcpu.get("nintypercentile"))) + " %", "Fail"));
				finalCpuData.put("verdict", new Data("Fail", "Fail"));
			} catch (Exception e) {
				finalCpuData.put("Average", new Data("Null", "Ambiguous"));
				finalCpuData.put("Min", new Data("Null", "Ambiguous"));
				finalCpuData.put("Max", new Data("Null", "Ambiguous"));
				finalCpuData.put("Nintyfivepercentile", new Data("Null", "Ambiguous"));
				finalCpuData.put("Nintypercentile", new Data("Null", "Ambiguous"));
				finalCpuData.put("verdict", new Data("Ambiguous", "Ambiguous"));
			}

			try {
				finalMemoryData.put("Average", new Data(
						decimalFormat.format((Double.parseDouble(memory.get("avg"))) / 1048576) + " MB", "Fail"));
				finalMemoryData.put("Min", new Data(
						decimalFormat.format((Double.parseDouble(memory.get("min"))) / 1048576) + " MB", "Fail"));
				finalMemoryData.put("Max", new Data(
						decimalFormat.format((Double.parseDouble(memory.get("max"))) / 1048576) + " MB", "Fail"));
				finalMemoryData.put("Nintyfivepercentile", new Data(
						decimalFormat.format((Double.parseDouble(memory.get("nintyfivepercentile"))) / 1048576) + " MB",
						"Fail"));
				finalMemoryData.put("Nintypercentile", new Data(
						decimalFormat.format((Double.parseDouble(memory.get("nintypercentile"))) / 1048576) + " MB",
						"Fail"));
				finalMemoryData.put("verdict", new Data("Fail", "Fail"));
			} catch (Exception e) {
				finalMemoryData.put("Average", new Data("Null", "Ambiguous"));
				finalMemoryData.put("Min", new Data("Null", "Ambiguous"));
				finalMemoryData.put("Max", new Data("Null", "Ambiguous"));
				finalMemoryData.put("Nintyfivepercentile", new Data("Null", "Ambiguous"));
				finalMemoryData.put("Nintypercentile", new Data("Null", "Ambiguous"));
				finalMemoryData.put("verdict", new Data("Ambiguous", "Ambiguous"));
			}

			try {
				finalProcessCpuData.put("Average",
						new Data(decimalFormat.format(Double.parseDouble(processCpu.get("avg"))) + " %", "Fail"));
				finalProcessCpuData.put("Min",
						new Data(decimalFormat.format(Double.parseDouble(processCpu.get("min"))) + " %", "Fail"));
				finalProcessCpuData.put("Max",
						new Data(decimalFormat.format(Double.parseDouble(processCpu.get("max"))) + " %", "Fail"));
				finalProcessCpuData.put("Nintyfivepercentile",
						new Data(decimalFormat.format(Double.parseDouble(processCpu.get("nintyfivepercentile"))) + " %",
								"Fail"));
				finalProcessCpuData.put("Nintypercentile", new Data(
						decimalFormat.format(Double.parseDouble(processCpu.get("nintypercentile"))) + " %", "Fail"));
				finalProcessCpuData.put("verdict", new Data("Fail", "Fail"));
			} catch (Exception e) {
				finalProcessCpuData.put("Average", new Data("Null", "Ambiguous"));
				finalProcessCpuData.put("Min", new Data("Null", "Ambiguous"));
				finalProcessCpuData.put("Max", new Data("Null", "Ambiguous"));
				finalProcessCpuData.put("Nintyfivepercentile", new Data("Null", "Ambiguous"));
				finalProcessCpuData.put("Nintypercentile", new Data("Null", "Ambiguous"));
				finalProcessCpuData.put("verdict", new Data("Ambiguous", "Ambiguous"));
			}

			try {
				finalHeapUsedData.put("Average", new Data(
						decimalFormat.format((Double.parseDouble(heapUsed.get("avg"))) / 1048576) + " MB", "Fail"));
				finalHeapUsedData.put("Min", new Data(
						decimalFormat.format((Double.parseDouble(heapUsed.get("min"))) / 1048576) + " MB", "Fail"));
				finalHeapUsedData.put("Max", new Data(
						decimalFormat.format((Double.parseDouble(heapUsed.get("max"))) / 1048576) + " MB", "Fail"));
				finalHeapUsedData.put("Nintyfivepercentile",
						new Data(
								decimalFormat.format(
										(Double.parseDouble(heapUsed.get("nintyfivepercentile"))) / 1048576) + " MB",
								"Fail"));
				finalHeapUsedData.put("Nintypercentile", new Data(
						decimalFormat.format((Double.parseDouble(heapUsed.get("nintypercentile"))) / 1048576) + " MB",
						"Fail"));
				finalHeapUsedData.put("verdict", new Data("Fail", "Fail"));
			} catch (Exception e) {
				finalHeapUsedData.put("Average", new Data("Null", "Ambiguous"));
				finalHeapUsedData.put("Min", new Data("Null", "Ambiguous"));
				finalHeapUsedData.put("Max", new Data("Null", "Ambiguous"));
				finalHeapUsedData.put("Nintyfivepercentile", new Data("Null", "Ambiguous"));
				finalHeapUsedData.put("Nintypercentile", new Data("Null", "Ambiguous"));
				finalHeapUsedData.put("verdict", new Data("Ambiguous", "Ambiguous"));
			}

		} else {

			Map<String, List<String>> mainVmcpu = gson.fromJson(testcaseverdict.getVmcpuverdict(), Map.class);
			Map<String, List<String>> mainMemory = gson.fromJson(testcaseverdict.getMemoryverdict(), Map.class);
			Map<String, List<String>> mainProcessCpu = gson.fromJson(testcaseverdict.getProcesscpuverdict(), Map.class);
			Map<String, List<String>> mainheapUsed = gson.fromJson(testcaseverdict.getHeapusedverdict(), Map.class);

			try {
				finalCpuData.put("Average", new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("avg"))) + " %",
						compareArrayNumbers(vmcpu.get("avg"), mainVmcpu.get("avg"))));
				finalCpuData.put("Min", new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("min"))) + " %",
						compareArrayNumbers(vmcpu.get("min"), mainVmcpu.get("min"))));
				finalCpuData.put("Max", new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("max"))) + " %",
						compareArrayNumbers(vmcpu.get("max"), mainVmcpu.get("max"))));
				finalCpuData.put("Nintyfivepercentile", new Data(
						decimalFormat.format(Double.parseDouble(vmcpu.get("nintyfivepercentile"))) + " %",
						compareArrayNumbers(vmcpu.get("nintyfivepercentile"), mainVmcpu.get("nintyfivepercentile"))));
				finalCpuData.put("Nintypercentile",
						new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("nintypercentile"))) + " %",
								compareArrayNumbers(vmcpu.get("nintypercentile"), mainVmcpu.get("nintypercentile"))));
				String vmcpuVerdict = "Pass";
				if (compareArrayNumbers(vmcpu.get("avg"), mainVmcpu.get("avg")).equals("Fail")
						|| compareArrayNumbers(vmcpu.get("min"), mainVmcpu.get("min")).equals("Fail")
						|| compareArrayNumbers(vmcpu.get("max"), mainVmcpu.get("max")).equals("Fail")
						|| compareArrayNumbers(vmcpu.get("nintyfivepercentile"), mainVmcpu.get("nintyfivepercentile"))
								.equals("Fail")
						|| compareArrayNumbers(vmcpu.get("nintypercentile"), mainVmcpu.get("nintypercentile"))
								.equals("Fail"))
					vmcpuVerdict = "Fail";
				finalCpuData.put("verdict", new Data(vmcpuVerdict, vmcpuVerdict));
			} catch (Exception e) {
				finalCpuData.put("Average", new Data("Null", "Ambiguous"));
				finalCpuData.put("Min", new Data("Null", "Ambiguous"));
				finalCpuData.put("Max", new Data("Null", "Ambiguous"));
				finalCpuData.put("Nintyfivepercentile", new Data("Null", "Ambiguous"));
				finalCpuData.put("Nintypercentile", new Data("Null", "Ambiguous"));
				finalCpuData.put("verdict", new Data("Ambiguous", "Ambiguous"));
			}

			try {
				finalMemoryData.put("Average",
						new Data(decimalFormat.format((Double.parseDouble(memory.get("avg"))) / 1048576) + " MB",
								compareArrayNumbers(memory.get("avg"), mainMemory.get("avg"))));
				finalMemoryData.put("Min",
						new Data(decimalFormat.format((Double.parseDouble(memory.get("min"))) / 1048576) + " MB",
								compareArrayNumbers(memory.get("min"), mainMemory.get("min"))));
				finalMemoryData.put("Max",
						new Data(decimalFormat.format((Double.parseDouble(memory.get("max"))) / 1048576) + " MB",
								compareArrayNumbers(memory.get("max"), mainMemory.get("max"))));
				finalMemoryData.put("Nintyfivepercentile", new Data(
						decimalFormat.format((Double.parseDouble(memory.get("nintyfivepercentile"))) / 1048576) + " MB",
						compareArrayNumbers(memory.get("nintyfivepercentile"), mainMemory.get("nintyfivepercentile"))));
				finalMemoryData.put("Nintypercentile",
						new Data(
								decimalFormat.format((Double.parseDouble(memory.get("nintypercentile"))) / 1048576)
										+ " MB",
								compareArrayNumbers(memory.get("nintypercentile"), mainMemory.get("nintypercentile"))));
				String memoryVerdict = "Pass";
				if (compareArrayNumbers(memory.get("avg"), mainMemory.get("avg")).equals("Fail")
						|| compareArrayNumbers(memory.get("min"), mainMemory.get("min")).equals("Fail")
						|| compareArrayNumbers(memory.get("max"), mainMemory.get("max")).equals("Fail")
						|| compareArrayNumbers(memory.get("nintyfivepercentile"), mainMemory.get("nintyfivepercentile"))
								.equals("Fail")
						|| compareArrayNumbers(memory.get("nintypercentile"), mainMemory.get("nintypercentile"))
								.equals("Fail"))
					memoryVerdict = "Fail";
				finalMemoryData.put("verdict", new Data(memoryVerdict, memoryVerdict));
			} catch (Exception e) {
				finalMemoryData.put("Average", new Data("Null", "Ambiguous"));
				finalMemoryData.put("Min", new Data("Null", "Ambiguous"));
				finalMemoryData.put("Max", new Data("Null", "Ambiguous"));
				finalMemoryData.put("Nintyfivepercentile", new Data("Null", "Ambiguous"));
				finalMemoryData.put("Nintypercentile", new Data("Null", "Ambiguous"));
				finalMemoryData.put("verdict", new Data("Ambiguous", "Ambiguous"));
			}

			try {
				finalProcessCpuData.put("Average",
						new Data(decimalFormat.format(Double.parseDouble(processCpu.get("avg"))) + " %",
								compareArrayNumbers(processCpu.get("avg"), mainProcessCpu.get("avg"))));
				finalProcessCpuData.put("Min",
						new Data(decimalFormat.format(Double.parseDouble(processCpu.get("min"))) + " %",
								compareArrayNumbers(processCpu.get("min"), mainProcessCpu.get("min"))));
				finalProcessCpuData.put("Max",
						new Data(decimalFormat.format(Double.parseDouble(processCpu.get("max"))) + " %",
								compareArrayNumbers(processCpu.get("max"), mainProcessCpu.get("max"))));
				finalProcessCpuData.put("Nintyfivepercentile",
						new Data(decimalFormat.format(Double.parseDouble(processCpu.get("nintyfivepercentile"))) + " %",
								compareArrayNumbers(processCpu.get("nintyfivepercentile"),
										mainProcessCpu.get("nintyfivepercentile"))));
				finalProcessCpuData.put("Nintypercentile", new Data(
						decimalFormat.format(Double.parseDouble(processCpu.get("nintypercentile"))) + " %",
						compareArrayNumbers(processCpu.get("nintypercentile"), mainProcessCpu.get("nintypercentile"))));
				String processCpuVerdict = "Pass";
				if (compareArrayNumbers(processCpu.get("avg"), mainProcessCpu.get("avg")).equals("Fail")
						|| compareArrayNumbers(processCpu.get("min"), mainProcessCpu.get("min")).equals("Fail")
						|| compareArrayNumbers(processCpu.get("max"), mainProcessCpu.get("max")).equals("Fail")
						|| compareArrayNumbers(processCpu.get("nintyfivepercentile"),
								mainProcessCpu.get("nintyfivepercentile")).equals("Fail")
						|| compareArrayNumbers(processCpu.get("nintypercentile"), mainProcessCpu.get("nintypercentile"))
								.equals("Fail"))
					processCpuVerdict = "Fail";
				finalProcessCpuData.put("verdict", new Data(processCpuVerdict, processCpuVerdict));
			} catch (Exception e) {
				finalProcessCpuData.put("Average", new Data("Null", "Ambiguous"));
				finalProcessCpuData.put("Min", new Data("Null", "Ambiguous"));
				finalProcessCpuData.put("Max", new Data("Null", "Ambiguous"));
				finalProcessCpuData.put("Nintyfivepercentile", new Data("Null", "Ambiguous"));
				finalProcessCpuData.put("Nintypercentile", new Data("Null", "Ambiguous"));
				finalProcessCpuData.put("verdict", new Data("Ambiguous", "Ambiguous"));
			}

			try {
				finalHeapUsedData.put("Average",
						new Data(decimalFormat.format((Double.parseDouble(heapUsed.get("avg"))) / 1048576) + " MB",
								compareArrayNumbers(heapUsed.get("avg"), mainheapUsed.get("avg"))));
				finalHeapUsedData.put("Min",
						new Data(decimalFormat.format((Double.parseDouble(heapUsed.get("min"))) / 1048576) + " MB",
								compareArrayNumbers(heapUsed.get("min"), mainheapUsed.get("min"))));
				finalHeapUsedData.put("Max",
						new Data(decimalFormat.format((Double.parseDouble(heapUsed.get("max"))) / 1048576) + " MB",
								compareArrayNumbers(heapUsed.get("max"), mainheapUsed.get("max"))));
				finalHeapUsedData.put("Nintyfivepercentile", new Data(
						decimalFormat.format((Double.parseDouble(heapUsed.get("nintyfivepercentile"))) / 1048576)
								+ " MB",
						compareArrayNumbers(heapUsed.get("nintyfivepercentile"),
								mainheapUsed.get("nintyfivepercentile"))));
				finalHeapUsedData.put("Nintypercentile", new Data(
						decimalFormat.format((Double.parseDouble(heapUsed.get("nintypercentile"))) / 1048576) + " MB",
						compareArrayNumbers(heapUsed.get("nintypercentile"), mainheapUsed.get("nintypercentile"))));
				String heapUsedVerdict = "Pass";
				if (compareArrayNumbers(heapUsed.get("avg"), mainheapUsed.get("avg")).equals("Fail")
						|| compareArrayNumbers(heapUsed.get("min"), mainheapUsed.get("min")).equals("Fail")
						|| compareArrayNumbers(heapUsed.get("max"), mainheapUsed.get("max")).equals("Fail")
						|| compareArrayNumbers(heapUsed.get("nintyfivepercentile"),
								mainheapUsed.get("nintyfivepercentile")).equals("Fail")
						|| compareArrayNumbers(heapUsed.get("nintypercentile"), mainheapUsed.get("nintypercentile"))
								.equals("Fail"))
					heapUsedVerdict = "Fail";
				finalHeapUsedData.put("verdict", new Data(heapUsedVerdict, heapUsedVerdict));
			} catch (Exception e) {
				finalHeapUsedData.put("Average", new Data("Null", "Ambiguous"));
				finalHeapUsedData.put("Min", new Data("Null", "Ambiguous"));
				finalHeapUsedData.put("Max", new Data("Null", "Ambiguous"));
				finalHeapUsedData.put("Nintyfivepercentile", new Data("Null", "Ambiguous"));
				finalHeapUsedData.put("Nintypercentile", new Data("Null", "Ambiguous"));
				finalHeapUsedData.put("verdict", new Data("Ambiguous", "Ambiguous"));
			}
		}

		Map<String, Map<String, Data>> map = new HashMap<String, Map<String, Data>>();
		map.put("vmcpu", finalCpuData);
		map.put("memory", finalMemoryData);
		map.put("processCpu", finalProcessCpuData);
		map.put("heapUsed", finalHeapUsedData);
		return map;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Map<String, Data>> compareWithExecData(Execmetricsummary execmetricsummary,
			Execmetricsummary mainExecmetricsummary) {

		Gson gson = new Gson();

		DecimalFormat decimalFormat = new DecimalFormat("#0.0");
		Map<String, Data> finalCpuData = new HashMap<String, Data>();
		Map<String, Data> finalMemoryData = new HashMap<String, Data>();
		Map<String, Data> finalProcessCpuData = new HashMap<String, Data>();
		Map<String, Data> finalHeapUsedData = new HashMap<String, Data>();
		Map<String, String> vmcpu = gson.fromJson(execmetricsummary.getVmcpu(), Map.class);
		Map<String, String> memory = gson.fromJson(execmetricsummary.getMemory(), Map.class);
		Map<String, String> processCpu = gson.fromJson(execmetricsummary.getProcesscpu(), Map.class);
		Map<String, String> heapUsed = gson.fromJson(execmetricsummary.getHeapused(), Map.class);

		Map<String, String> mainVmcpu = gson.fromJson(mainExecmetricsummary.getVmcpu(), Map.class);
		Map<String, String> mainMemory = gson.fromJson(mainExecmetricsummary.getMemory(), Map.class);
		Map<String, String> mainProcessCpu = gson.fromJson(mainExecmetricsummary.getProcesscpu(), Map.class);
		Map<String, String> mainheapUsed = gson.fromJson(mainExecmetricsummary.getHeapused(), Map.class);

		try {
			finalCpuData.put("Average", new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("avg"))) + " %",
					compareNumbers(vmcpu.get("avg"), mainVmcpu.get("avg"))));
			finalCpuData.put("Min", new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("min"))) + " %",
					compareNumbers(vmcpu.get("min"), mainVmcpu.get("min"))));
			finalCpuData.put("Max", new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("max"))) + " %",
					compareNumbers(vmcpu.get("max"), mainVmcpu.get("max"))));
			finalCpuData.put("Nintyfivepercentile",
					new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("nintyfivepercentile"))) + " %",
							compareNumbers(vmcpu.get("nintyfivepercentile"), mainVmcpu.get("nintyfivepercentile"))));
			finalCpuData.put("Nintypercentile",
					new Data(decimalFormat.format(Double.parseDouble(vmcpu.get("nintypercentile"))) + " %",
							compareNumbers(vmcpu.get("nintypercentile"), mainVmcpu.get("nintypercentile"))));
			String vmcpuVerdict = "Pass";
			if (compareNumbers(vmcpu.get("avg"), mainVmcpu.get("avg")).equals("Fail")
					|| compareNumbers(vmcpu.get("min"), mainVmcpu.get("min")).equals("Fail")
					|| compareNumbers(vmcpu.get("max"), mainVmcpu.get("max")).equals("Fail")
					|| compareNumbers(vmcpu.get("nintyfivepercentile"), mainVmcpu.get("nintyfivepercentile"))
							.equals("Fail")
					|| compareNumbers(vmcpu.get("nintypercentile"), mainVmcpu.get("nintypercentile")).equals("Fail"))
				vmcpuVerdict = "Fail";
			finalCpuData.put("verdict", new Data(vmcpuVerdict, vmcpuVerdict));
		} catch (Exception e) {
			finalCpuData.put("Average", new Data("Null", "Fail"));
			finalCpuData.put("Min", new Data("Null", "Fail"));
			finalCpuData.put("Max", new Data("Null", "Fail"));
			finalCpuData.put("Nintyfivepercentile", new Data("Null", "Fail"));
			finalCpuData.put("Nintypercentile", new Data("Null", "Fail"));
			finalCpuData.put("verdict", new Data("Fail", "Fail"));
		}

		try {
			finalMemoryData.put("Average",
					new Data(decimalFormat.format((Double.parseDouble(memory.get("avg"))) / 1048576) + " MB",
							compareNumbers(memory.get("avg"), mainMemory.get("avg"))));
			finalMemoryData.put("Min",
					new Data(decimalFormat.format((Double.parseDouble(memory.get("min"))) / 1048576) + " MB",
							compareNumbers(memory.get("min"), mainMemory.get("min"))));
			finalMemoryData.put("Max",
					new Data(decimalFormat.format((Double.parseDouble(memory.get("max"))) / 1048576) + " MB",
							compareNumbers(memory.get("max"), mainMemory.get("max"))));
			finalMemoryData.put("Nintyfivepercentile",
					new Data(
							decimalFormat.format((Double.parseDouble(memory.get("nintyfivepercentile"))) / 1048576)
									+ " MB",
							compareNumbers(memory.get("nintyfivepercentile"), mainMemory.get("nintyfivepercentile"))));
			finalMemoryData.put("Nintypercentile",
					new Data(
							decimalFormat.format((Double.parseDouble(memory.get("nintypercentile"))) / 1048576) + " MB",
							compareNumbers(memory.get("nintypercentile"), mainMemory.get("nintypercentile"))));
			String memoryVerdict = "Pass";
			if (compareNumbers(memory.get("avg"), mainMemory.get("avg")).equals("Fail")
					|| compareNumbers(memory.get("min"), mainMemory.get("min")).equals("Fail")
					|| compareNumbers(memory.get("max"), mainMemory.get("max")).equals("Fail")
					|| compareNumbers(memory.get("nintyfivepercentile"), mainMemory.get("nintyfivepercentile"))
							.equals("Fail")
					|| compareNumbers(memory.get("nintypercentile"), mainMemory.get("nintypercentile")).equals("Fail"))
				memoryVerdict = "Fail";
			finalMemoryData.put("verdict", new Data(memoryVerdict, memoryVerdict));
		} catch (Exception e) {
			finalMemoryData.put("Average", new Data("Null", "Fail"));
			finalMemoryData.put("Min", new Data("Null", "Fail"));
			finalMemoryData.put("Max", new Data("Null", "Fail"));
			finalMemoryData.put("Nintyfivepercentile", new Data("Null", "Fail"));
			finalMemoryData.put("Nintypercentile", new Data("Null", "Fail"));
			finalMemoryData.put("verdict", new Data("Fail", "Fail"));
		}

		try {
			finalProcessCpuData.put("Average",
					new Data(decimalFormat.format(Double.parseDouble(processCpu.get("avg"))) + " %",
							compareNumbers(processCpu.get("avg"), mainProcessCpu.get("avg"))));
			finalProcessCpuData.put("Min",
					new Data(decimalFormat.format(Double.parseDouble(processCpu.get("min"))) + " %",
							compareNumbers(processCpu.get("min"), mainProcessCpu.get("min"))));
			finalProcessCpuData.put("Max",
					new Data(decimalFormat.format(Double.parseDouble(processCpu.get("max"))) + " %",
							compareNumbers(processCpu.get("max"), mainProcessCpu.get("max"))));
			finalProcessCpuData.put("Nintyfivepercentile", new Data(
					decimalFormat.format(Double.parseDouble(processCpu.get("nintyfivepercentile"))) + " %",
					compareNumbers(processCpu.get("nintyfivepercentile"), mainProcessCpu.get("nintyfivepercentile"))));
			finalProcessCpuData.put("Nintypercentile",
					new Data(decimalFormat.format(Double.parseDouble(processCpu.get("nintypercentile"))) + " %",
							compareNumbers(processCpu.get("nintypercentile"), mainProcessCpu.get("nintypercentile"))));
			String processCpuVerdict = "Pass";
			if (compareNumbers(processCpu.get("avg"), mainProcessCpu.get("avg")).equals("Fail")
					|| compareNumbers(processCpu.get("min"), mainProcessCpu.get("min")).equals("Fail")
					|| compareNumbers(processCpu.get("max"), mainProcessCpu.get("max")).equals("Fail")
					|| compareNumbers(processCpu.get("nintyfivepercentile"), mainProcessCpu.get("nintyfivepercentile"))
							.equals("Fail")
					|| compareNumbers(processCpu.get("nintypercentile"), mainProcessCpu.get("nintypercentile"))
							.equals("Fail"))
				processCpuVerdict = "Fail";
			finalProcessCpuData.put("verdict", new Data(processCpuVerdict, processCpuVerdict));
		} catch (Exception e) {
			finalProcessCpuData.put("Average", new Data("Null", "Fail"));
			finalProcessCpuData.put("Min", new Data("Null", "Fail"));
			finalProcessCpuData.put("Max", new Data("Null", "Fail"));
			finalProcessCpuData.put("Nintyfivepercentile", new Data("Null", "Fail"));
			finalProcessCpuData.put("Nintypercentile", new Data("Null", "Fail"));
			finalProcessCpuData.put("verdict", new Data("Fail", "Fail"));
		}
		try {
			finalHeapUsedData.put("Average",
					new Data(decimalFormat.format((Double.parseDouble(heapUsed.get("avg"))) / 1048576) + " MB",
							compareNumbers(heapUsed.get("avg"), mainheapUsed.get("avg"))));
			finalHeapUsedData.put("Min",
					new Data(decimalFormat.format((Double.parseDouble(heapUsed.get("min"))) / 1048576) + " MB",
							compareNumbers(heapUsed.get("min"), mainheapUsed.get("min"))));
			finalHeapUsedData.put("Max",
					new Data(decimalFormat.format((Double.parseDouble(heapUsed.get("max"))) / 1048576) + " MB",
							compareNumbers(heapUsed.get("max"), mainheapUsed.get("max"))));
			finalHeapUsedData.put("Nintyfivepercentile", new Data(
					decimalFormat.format((Double.parseDouble(heapUsed.get("nintyfivepercentile"))) / 1048576) + " MB",
					compareNumbers(heapUsed.get("nintyfivepercentile"), mainheapUsed.get("nintyfivepercentile"))));
			finalHeapUsedData.put("Nintypercentile",
					new Data(
							decimalFormat.format((Double.parseDouble(heapUsed.get("nintypercentile"))) / 1048576)
									+ " MB",
							compareNumbers(heapUsed.get("nintypercentile"), mainheapUsed.get("nintypercentile"))));
			String heapUsedVerdict = "Pass";
			if (compareNumbers(heapUsed.get("avg"), mainheapUsed.get("avg")).equals("Fail")
					|| compareNumbers(heapUsed.get("min"), mainheapUsed.get("min")).equals("Fail")
					|| compareNumbers(heapUsed.get("max"), mainheapUsed.get("max")).equals("Fail")
					|| compareNumbers(heapUsed.get("nintyfivepercentile"), mainheapUsed.get("nintyfivepercentile"))
							.equals("Fail")
					|| compareNumbers(heapUsed.get("nintypercentile"), mainheapUsed.get("nintypercentile"))
							.equals("Fail"))
				heapUsedVerdict = "Fail";
			finalHeapUsedData.put("verdict", new Data(heapUsedVerdict, heapUsedVerdict));
		} catch (Exception e) {
			finalHeapUsedData.put("Average", new Data("Null", "Fail"));
			finalHeapUsedData.put("Min", new Data("Null", "Fail"));
			finalHeapUsedData.put("Max", new Data("Null", "Fail"));
			finalHeapUsedData.put("Nintyfivepercentile", new Data("Null", "Fail"));
			finalHeapUsedData.put("Nintypercentile", new Data("Null", "Fail"));
			finalHeapUsedData.put("verdict", new Data("Fail", "Fail"));
		}
		Map<String, Map<String, Data>> map = new HashMap<String, Map<String, Data>>();
		map.put("vmcpu", finalCpuData);
		map.put("memory", finalMemoryData);
		map.put("processCpu", finalProcessCpuData);
		map.put("heapUsed", finalHeapUsedData);
		return map;
	}

	public String collectTelnetStats(String user, String ip, String password, Integer controlPort, String instanceName,
			String destination) {

		String mainString = executeShellCommand(
				"/backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/collectTelnetStats.sh " + user + " " + ip + " "
						+ password + " " + controlPort);

		reportUpdate(mainString, destination + "/Reports/" + instanceName + "_TelnetStats.txt");
		return "Completed";
	}

	public String updateBuildInfoForNonDocker(String username, String serviceip, String password, String sutversion) {

		if (sutversion.equals("tde")) {
			return updateBuildInfoForDocker(username, serviceip, password, sutversion);
		}

		String mainString = executeShellCommand("/backup/Users/caradhya/RootUserFiles/updateBuildInfo.sh " + username
				+ " " + serviceip + " " + password + " " + sutversion);

		String[] temp = mainString.split("\n");
		List<String> myString = new ArrayList<String>();
		boolean readData = false;
		for (String string : temp) {
			if (string.regionMatches(0, "START", 0, 5))
				readData = true;
			if (string.regionMatches(0, "END", 0, 3))
				readData = false;
			if (readData)
				myString.add(string.replaceAll("\\r\\n|\\r|\\n", ""));
		}
		StringBuffer sb = new StringBuffer();
		for (int i = 2; i < myString.size() - 1; i++) {
			String[] arg = myString.get(i).split("=");
			if (i != myString.size() - 2)
				sb.append(arg[1] + ".");
			else
				sb.append(arg[1]);
		}
		return sb.toString();
	}

	public String updateBuildInfoForDocker(String username, String serviceip, String password, String instanceType) {

		String mainString = executeShellCommand("/backup/Users/caradhya/RootUserFiles/updateBuildInfoDocker.sh "
				+ username + " " + serviceip + " " + password + " " + instanceType);
		String[] temp = mainString.split("\n");
		String originalString = null;

		for (int i = 0; i < temp.length; i++) {
			if (temp[i].regionMatches(0, "START", 0, 5))
				originalString = temp[i + 2].replaceAll("\\r\\n|\\r|\\n", "");
		}
		return originalString;
	}

	public Testcaseverdict convertTestcaseverdictRequestToTestcaseverdict(TestcaseverdictRequest testcaseverdictRequest,
			Testcaseverdict testcaseverdict) {
		Gson gson = new Gson();

		if (testcaseverdict == null) {
			testcaseverdict = new Testcaseverdict();
			testcaseverdict.setActive(1L);
			testcaseverdict.setCreated(System.currentTimeMillis());
			testcaseverdict.setGccountverdict(null);
			testcaseverdict.setGcthroughputverdict(null);
			testcaseverdict.setPausetimeverdict(null);
		}

		testcaseverdict.setBranch(testcaseverdictRequest.getBranch());
		testcaseverdict.setBuild(testcaseverdictRequest.getBuild());
		testcaseverdict.setDescription(testcaseverdictRequest.getDescription());
		testcaseverdict.setHeapusedverdict(gson.toJson(testcaseverdictRequest.getHeapUsedVerdict()));
		testcaseverdict.setInstancename(testcaseverdictRequest.getInstanceType());
		testcaseverdict.setInterval(testcaseverdictRequest.getInterval());
		testcaseverdict.setLastupdated(System.currentTimeMillis());
		testcaseverdict.setMemoryverdict(gson.toJson(testcaseverdictRequest.getMemoryVerdict()));
		testcaseverdict.setProcesscpuverdict(gson.toJson(testcaseverdictRequest.getProcessCpuVerdict()));
		testcaseverdict.setTestcaseid(testcaseverdictRequest.getTestCaseId());
		testcaseverdict.setTpsverdict(gson.toJson(testcaseverdictRequest.getTpsVerdict()));
		testcaseverdict.setVmcpuverdict(gson.toJson(testcaseverdictRequest.getVmCpuVerdict()));

		return testcaseverdict;

	}

	public TestcaseverdictRequest convertTestcaseverdictToTestcaseverdictRequest(Testcaseverdict testcaseverdict,
			TestcaseverdictRequest testcaseverdictRequest) {
		Gson gson = new Gson();
		if (testcaseverdictRequest == null)
			testcaseverdictRequest = new TestcaseverdictRequest();

		testcaseverdictRequest.setBranch(testcaseverdict.getBranch());
		testcaseverdictRequest.setBuild(testcaseverdict.getBuild());
		testcaseverdictRequest.setDescription(testcaseverdict.getDescription());
		testcaseverdictRequest
				.setHeapUsedVerdict(gson.fromJson(testcaseverdict.getHeapusedverdict(), HeapUsedVerdict.class));
		testcaseverdictRequest.setInstanceType(testcaseverdict.getInstancename());
		testcaseverdictRequest.setInterval(testcaseverdict.getInterval());
		testcaseverdictRequest.setMemoryVerdict(gson.fromJson(testcaseverdict.getMemoryverdict(), MemoryVerdict.class));
		testcaseverdictRequest
				.setProcessCpuVerdict(gson.fromJson(testcaseverdict.getProcesscpuverdict(), ProcessCpuVerdict.class));
		testcaseverdictRequest.setTestCaseId(testcaseverdict.getTestcaseid());
		testcaseverdictRequest.setTestCaseVerdictId(testcaseverdict.getTestcaseverdictid());
		testcaseverdictRequest.setTpsVerdict(gson.fromJson(testcaseverdict.getTpsverdict(), TpsVerdict.class));
		testcaseverdictRequest.setVmCpuVerdict(gson.fromJson(testcaseverdict.getVmcpuverdict(), VmCpuVerdict.class));

		return testcaseverdictRequest;

	}

	public String executeShellCommand(String command) {

		Process p = null;
		String temp = null;
		try {
			p = Runtime.getRuntime().exec(command);
			InputStream is = p.getInputStream();
			int i = 0;
			StringBuffer sb = new StringBuffer();
			while ((i = is.read()) != -1)
				sb.append((char) i);
			temp = sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return temp;
	}
}
