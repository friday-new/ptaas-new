/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.request;

import java.util.List;

public class ProcessCpuVerdict {

	private List<String> avg;
	private List<String> min;
	private List<String> nintypercentile;
	private List<String> max;
	private List<String> nintyfivepercentile;

	public List<String> getAvg() {
		return avg;
	}

	public void setAvg(List<String> avg) {
		this.avg = avg;
	}

	public List<String> getMin() {
		return min;
	}

	public void setMin(List<String> min) {
		this.min = min;
	}

	public List<String> getNintypercentile() {
		return nintypercentile;
	}

	public void setNintypercentile(List<String> nintypercentile) {
		this.nintypercentile = nintypercentile;
	}

	public List<String> getMax() {
		return max;
	}

	public void setMax(List<String> max) {
		this.max = max;
	}

	public List<String> getNintyfivepercentile() {
		return nintyfivepercentile;
	}

	public void setNintyfivepercentile(List<String> nintyfivepercentile) {
		this.nintyfivepercentile = nintyfivepercentile;
	}

}
