/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.lang.Nullable;

@Entity
public class Oltp {

	@Id
	private Integer oltpid;
	private String oltpname;
	private String oltpipaddr;
	private Long oltpport;
	private String oltpusername;
	private String oltppasswd;
	private String oltpschema;
	@Nullable
	private String oltpref;
	private String oltpinuse;
	private String isconfig;
	private Integer port;
	private String projectname;
	private String isassigned;

	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}

	public String getIsassigned() {
		return isassigned;
	}

	public void setIsassigned(String isassigned) {
		this.isassigned = isassigned;
	}

	public String getIsconfig() {
		return isconfig;
	}

	public void setIsconfig(String isconfig) {
		this.isconfig = isconfig;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Integer getOltpid() {
		return oltpid;
	}

	public void setOltpid(Integer oltpid) {
		this.oltpid = oltpid;
	}

	public String getOltpname() {
		return oltpname;
	}

	public void setOltpname(String oltpname) {
		this.oltpname = oltpname;
	}

	public String getOltpipaddr() {
		return oltpipaddr;
	}

	public void setOltpipaddr(String oltpipaddr) {
		this.oltpipaddr = oltpipaddr;
	}

	public Long getOltpport() {
		return oltpport;
	}

	public void setOltpport(Long oltpport) {
		this.oltpport = oltpport;
	}

	public String getOltpusername() {
		return oltpusername;
	}

	public void setOltpusername(String oltpusername) {
		this.oltpusername = oltpusername;
	}

	public String getOltppasswd() {
		return oltppasswd;
	}

	public void setOltppasswd(String oltppasswd) {
		this.oltppasswd = oltppasswd;
	}

	public String getOltpschema() {
		return oltpschema;
	}

	public void setOltpschema(String oltpschema) {
		this.oltpschema = oltpschema;
	}

	public String getOltpref() {
		return oltpref;
	}

	public void setOltpref(String oltpref) {
		this.oltpref = oltpref;
	}

	public String getOltpinuse() {
		return oltpinuse;
	}

	public void setOltpinuse(String oltpinuse) {
		this.oltpinuse = oltpinuse;
	}

}
