/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Taasexectracking {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Taasexectracking_PK_SEQ")
	@SequenceGenerator(sequenceName = "Taasexectracking_PK_SEQ", allocationSize = 1, name = "Taasexectracking_PK_SEQ")
	private Long taasexectrackingid;
	private String instancename;
	private String projectname;
	private String oltpip;
	private Integer oltpport;
	private String oltpusername;
	private Long created;
	private String isdeleted;
	private Long deletiontime;

	public Long getTaasexectrackingid() {
		return taasexectrackingid;
	}

	public void setTaasexectrackingid(Long taasexectrackingid) {
		this.taasexectrackingid = taasexectrackingid;
	}

	public String getInstancename() {
		return instancename;
	}

	public void setInstancename(String instancename) {
		this.instancename = instancename;
	}

	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}

	public String getOltpip() {
		return oltpip;
	}

	public void setOltpip(String oltpip) {
		this.oltpip = oltpip;
	}

	public Integer getOltpport() {
		return oltpport;
	}

	public void setOltpport(Integer oltpport) {
		this.oltpport = oltpport;
	}

	public String getOltpusername() {
		return oltpusername;
	}

	public void setOltpusername(String oltpusername) {
		this.oltpusername = oltpusername;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public String getIsdeleted() {
		return isdeleted;
	}

	public void setIsdeleted(String isdeleted) {
		this.isdeleted = isdeleted;
	}

	public Long getDeletiontime() {
		return deletiontime;
	}

	public void setDeletiontime(Long deletiontime) {
		this.deletiontime = deletiontime;
	}

}
