/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Configfeaturekey {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Configfeaturekey_PK_SEQ")
	@SequenceGenerator(sequenceName = "Configfeaturekey_PK_SEQ", allocationSize = 1, name = "Configfeaturekey_PK_SEQ")
	private Long configfeaturekeyid;
	private String instanceref;
	private String configvalue;
	private String configkey;
	private String configgroupid;
	private Long updatetime;
	private Long reverttime;

	public Long getConfigfeaturekeyid() {
		return configfeaturekeyid;
	}

	public void setConfigfeaturekeyid(Long configfeaturekeyid) {
		this.configfeaturekeyid = configfeaturekeyid;
	}

	public String getInstanceref() {
		return instanceref;
	}

	public void setInstanceref(String instanceref) {
		this.instanceref = instanceref;
	}

	public String getConfigvalue() {
		return configvalue;
	}

	public void setConfigvalue(String configvalue) {
		this.configvalue = configvalue;
	}

	public String getConfigkey() {
		return configkey;
	}

	public void setConfigkey(String configkey) {
		this.configkey = configkey;
	}

	public String getConfiggroupid() {
		return configgroupid;
	}

	public void setConfiggroupid(String configgroupid) {
		this.configgroupid = configgroupid;
	}

	public Long getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Long updatetime) {
		this.updatetime = updatetime;
	}

	public Long getReverttime() {
		return reverttime;
	}

	public void setReverttime(Long reverttime) {
		this.reverttime = reverttime;
	}

}
