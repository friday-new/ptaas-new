/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

public class AttributeData {

	private Long attributeCobConfigId;
	private Long attributeId;
	private Long cobrandId;
	private String configs;

	public Long getAttributeCobConfigId() {
		return attributeCobConfigId;
	}

	public void setAttributeCobConfigId(Long attributeCobConfigId) {
		this.attributeCobConfigId = attributeCobConfigId;
	}

	public Long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}

	public Long getCobrandId() {
		return cobrandId;
	}

	public void setCobrandId(Long cobrandId) {
		this.cobrandId = cobrandId;
	}

	public String getConfigs() {
		return configs;
	}

	public void setConfigs(String configs) {
		this.configs = configs;
	}

}
