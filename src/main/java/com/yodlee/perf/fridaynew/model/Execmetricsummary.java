/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Execmetricsummary {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXECMETRICSUMMARYID_PK_SEQ")
	@SequenceGenerator(sequenceName = "EXECMETRICSUMMARYID_PK_SEQ", allocationSize = 1, name = "EXECMETRICSUMMARYID_PK_SEQ")
	private Long execmetricsummaryid;
	private String testcaseid;
	private String exectracking;
	private String objectname;
	private Long created;
	private Long tcstarttime;
	private Long tcendtime;
	private Long zabbixelementid;
	private String zabbixreportlink;
	private String awrlink;
	private Long cpucount;
	private Long memorycount;
	private Long interval;
	private Long metricsstart;
	private Long metricsend;
	private String vmcpu;
	private String memory;
	private String processcpu;
	private String gcthroughput;
	private String heapused;
	private String tps;
	private String percentageerror;
	private String vmcpuverdict;
	private String memoryverdict;
	private String processcpuverdict;
	private String gcthroughputverdict;
	private String heapusedverdict;
	private String tpsverdict;
	private String finalverdict;
	private String build;
	private Long testcaseverdictid;
	private String branch;

	public Long getTestcaseverdictid() {
		return testcaseverdictid;
	}

	public void setTestcaseverdictid(Long testcaseverdictid) {
		this.testcaseverdictid = testcaseverdictid;
	}

	public Long getExecmetricsummaryid() {
		return execmetricsummaryid;
	}

	public void setExecmetricsummaryid(Long execmetricsummaryid) {
		this.execmetricsummaryid = execmetricsummaryid;
	}

	public String getTestcaseid() {
		return testcaseid;
	}

	public void setTestcaseid(String testcaseid) {
		this.testcaseid = testcaseid;
	}

	public String getExectracking() {
		return exectracking;
	}

	public void setExectracking(String exectracking) {
		this.exectracking = exectracking;
	}

	public String getObjectname() {
		return objectname;
	}

	public void setObjectname(String objectname) {
		this.objectname = objectname;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Long getTcstarttime() {
		return tcstarttime;
	}

	public void setTcstarttime(Long tcstarttime) {
		this.tcstarttime = tcstarttime;
	}

	public Long getTcendtime() {
		return tcendtime;
	}

	public void setTcendtime(Long tcendtime) {
		this.tcendtime = tcendtime;
	}

	public Long getZabbixelementid() {
		return zabbixelementid;
	}

	public void setZabbixelementid(Long zabbixelementid) {
		this.zabbixelementid = zabbixelementid;
	}

	public String getZabbixreportlink() {
		return zabbixreportlink;
	}

	public void setZabbixreportlink(String zabbixreportlink) {
		this.zabbixreportlink = zabbixreportlink;
	}

	public String getAwrlink() {
		return awrlink;
	}

	public void setAwrlink(String awrlink) {
		this.awrlink = awrlink;
	}

	public Long getCpucount() {
		return cpucount;
	}

	public void setCpucount(Long cpucount) {
		this.cpucount = cpucount;
	}

	public Long getMemorycount() {
		return memorycount;
	}

	public void setMemorycount(Long memorycount) {
		this.memorycount = memorycount;
	}

	public Long getInterval() {
		return interval;
	}

	public void setInterval(Long interval) {
		this.interval = interval;
	}

	public Long getMetricsstart() {
		return metricsstart;
	}

	public void setMetricsstart(Long metricsstart) {
		this.metricsstart = metricsstart;
	}

	public Long getMetricsend() {
		return metricsend;
	}

	public void setMetricsend(Long metricsend) {
		this.metricsend = metricsend;
	}

	public String getVmcpu() {
		return vmcpu;
	}

	public void setVmcpu(String vmcpu) {
		this.vmcpu = vmcpu;
	}

	public String getMemory() {
		return memory;
	}

	public void setMemory(String memory) {
		this.memory = memory;
	}

	public String getProcesscpu() {
		return processcpu;
	}

	public void setProcesscpu(String processcpu) {
		this.processcpu = processcpu;
	}

	public String getGcthroughput() {
		return gcthroughput;
	}

	public void setGcthroughput(String gcthroughput) {
		this.gcthroughput = gcthroughput;
	}

	public String getHeapused() {
		return heapused;
	}

	public void setHeapused(String heapused) {
		this.heapused = heapused;
	}

	public String getTps() {
		return tps;
	}

	public void setTps(String tps) {
		this.tps = tps;
	}

	public String getPercentageerror() {
		return percentageerror;
	}

	public void setPercentageerror(String percentageerror) {
		this.percentageerror = percentageerror;
	}

	public String getBuild() {
		return build;
	}

	public void setBuild(String build) {
		this.build = build;
	}

	public String getVmcpuverdict() {
		return vmcpuverdict;
	}

	public void setVmcpuverdict(String vmcpuverdict) {
		this.vmcpuverdict = vmcpuverdict;
	}

	public String getMemoryverdict() {
		return memoryverdict;
	}

	public void setMemoryverdict(String memoryverdict) {
		this.memoryverdict = memoryverdict;
	}

	public String getProcesscpuverdict() {
		return processcpuverdict;
	}

	public void setProcesscpuverdict(String processcpuverdict) {
		this.processcpuverdict = processcpuverdict;
	}

	public String getGcthroughputverdict() {
		return gcthroughputverdict;
	}

	public void setGcthroughputverdict(String gcthroughputverdict) {
		this.gcthroughputverdict = gcthroughputverdict;
	}

	public String getHeapusedverdict() {
		return heapusedverdict;
	}

	public void setHeapusedverdict(String heapusedverdict) {
		this.heapusedverdict = heapusedverdict;
	}

	public String getTpsverdict() {
		return tpsverdict;
	}

	public void setTpsverdict(String tpsverdict) {
		this.tpsverdict = tpsverdict;
	}

	public String getFinalverdict() {
		return finalverdict;
	}

	public void setFinalverdict(String finalverdict) {
		this.finalverdict = finalverdict;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

}
