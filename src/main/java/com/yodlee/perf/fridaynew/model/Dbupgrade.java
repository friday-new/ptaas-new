/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Dbupgrade {

	@Id
	private Long dbupgradeid;
	private String dbname;
	private String username;
	private String password;
	private String objectref;
	private String objectinuse;

	private String beforeversion;
	private String afterversion;
	private String requireddbs;
	private String importdeltadietdata;
	private String buildpath;
	private String runpath;

	private String oltpdbdetails;
	private String configdbdetails;
	private String payanydbdetails;
	private String yccdbdetails;
	private String achdbdetails;
	private String bayesiandbdetails;
	private String authdbdetails;

	private String resultdata1;
	private String resultdata2;
	private String resultdata3;
	private String resultdata4;
	private String resultdata5;

	public Long getDbupgradeid() {
		return dbupgradeid;
	}

	public void setDbupgradeid(Long dbupgradeid) {
		this.dbupgradeid = dbupgradeid;
	}

	public String getDbname() {
		return dbname;
	}

	public void setDbname(String dbname) {
		this.dbname = dbname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getObjectref() {
		return objectref;
	}

	public void setObjectref(String objectref) {
		this.objectref = objectref;
	}

	public String getObjectinuse() {
		return objectinuse;
	}

	public void setObjectinuse(String objectinuse) {
		this.objectinuse = objectinuse;
	}

	public String getBeforeversion() {
		return beforeversion;
	}

	public void setBeforeversion(String beforeversion) {
		this.beforeversion = beforeversion;
	}

	public String getAfterversion() {
		return afterversion;
	}

	public void setAfterversion(String afterversion) {
		this.afterversion = afterversion;
	}

	public String getRequireddbs() {
		return requireddbs;
	}

	public void setRequireddbs(String requireddbs) {
		this.requireddbs = requireddbs;
	}

	public String getImportdeltadietdata() {
		return importdeltadietdata;
	}

	public void setImportdeltadietdata(String importdeltadietdata) {
		this.importdeltadietdata = importdeltadietdata;
	}

	public String getBuildpath() {
		return buildpath;
	}

	public void setBuildpath(String buildpath) {
		this.buildpath = buildpath;
	}

	public String getRunpath() {
		return runpath;
	}

	public void setRunpath(String runpath) {
		this.runpath = runpath;
	}

	public String getOltpdbdetails() {
		return oltpdbdetails;
	}

	public void setOltpdbdetails(String oltpdbdetails) {
		this.oltpdbdetails = oltpdbdetails;
	}

	public String getConfigdbdetails() {
		return configdbdetails;
	}

	public void setConfigdbdetails(String configdbdetails) {
		this.configdbdetails = configdbdetails;
	}

	public String getPayanydbdetails() {
		return payanydbdetails;
	}

	public void setPayanydbdetails(String payanydbdetails) {
		this.payanydbdetails = payanydbdetails;
	}

	public String getYccdbdetails() {
		return yccdbdetails;
	}

	public void setYccdbdetails(String yccdbdetails) {
		this.yccdbdetails = yccdbdetails;
	}

	public String getAchdbdetails() {
		return achdbdetails;
	}

	public void setAchdbdetails(String achdbdetails) {
		this.achdbdetails = achdbdetails;
	}

	public String getBayesiandbdetails() {
		return bayesiandbdetails;
	}

	public void setBayesiandbdetails(String bayesiandbdetails) {
		this.bayesiandbdetails = bayesiandbdetails;
	}

	public String getAuthdbdetails() {
		return authdbdetails;
	}

	public void setAuthdbdetails(String authdbdetails) {
		this.authdbdetails = authdbdetails;
	}

	public String getResultdata1() {
		return resultdata1;
	}

	public void setResultdata1(String resultdata1) {
		this.resultdata1 = resultdata1;
	}

	public String getResultdata2() {
		return resultdata2;
	}

	public void setResultdata2(String resultdata2) {
		this.resultdata2 = resultdata2;
	}

	public String getResultdata3() {
		return resultdata3;
	}

	public void setResultdata3(String resultdata3) {
		this.resultdata3 = resultdata3;
	}

	public String getResultdata4() {
		return resultdata4;
	}

	public void setResultdata4(String resultdata4) {
		this.resultdata4 = resultdata4;
	}

	public String getResultdata5() {
		return resultdata5;
	}

	public void setResultdata5(String resultdata5) {
		this.resultdata5 = resultdata5;
	}

}
