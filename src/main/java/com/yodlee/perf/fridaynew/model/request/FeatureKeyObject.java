/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.request;

import javax.validation.constraints.NotNull;

//@ApiModel(description = "Feature Key Object is used to change the values of few feature tables in oltp")
public class FeatureKeyObject {

	// @ApiModelProperty(notes = "Oltp instance reference")
	@NotNull
	private String oltpRef;
	// @ApiModelProperty(notes = "Table name like
	// param_acl,param_key,cobrand_acl_value,cob_param")
	@NotNull
	private String tableName;
	// @ApiModelProperty(notes = "New value which needs to be updated")
	private String keyValue;
	// @ApiModelProperty(notes = "param_key_id or param_acl_id of which needs to be
	// changed")
	@NotNull
	private Long keyId;
	// @ApiModelProperty(notes = "Cobrand_id for cobrand_acl_value or cob_param")
	private Long cobrandId;

	public String getOltpRef() {
		return oltpRef;
	}

	public void setOltpRef(String oltpRef) {
		this.oltpRef = oltpRef;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}

	public Long getKeyId() {
		return keyId;
	}

	public void setKeyId(Long keyId) {
		this.keyId = keyId;
	}

	public Long getCobrandId() {
		return cobrandId;
	}

	public void setCobrandId(Long cobrandId) {
		this.cobrandId = cobrandId;
	}

}
