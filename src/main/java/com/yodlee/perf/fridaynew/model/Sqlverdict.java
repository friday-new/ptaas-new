/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Sqlverdict {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sqlverdictid_PK_SEQ")
	@SequenceGenerator(sequenceName = "sqlverdictid_PK_SEQ", allocationSize = 1, name = "sqlverdictid_PK_SEQ")
	private Long sqlverdictid;
	private String tablename;

	public Long getSqlverdictid() {
		return sqlverdictid;
	}

	public void setSqlverdictid(Long sqlverdictid) {
		this.sqlverdictid = sqlverdictid;
	}

	public String getTablename() {
		return tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}

}
