/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.response;

import java.util.Map;

//@ApiModel(description = "Component Response object is used to declare the status of the component")
public class ComponentResponse {

	// @ApiModelProperty(notes = "false is not started, true is started")
	boolean started;
	// @ApiModelProperty(notes = "Current status of the component")
	Map<String, Integer> status;

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public Map<String, Integer> getStatus() {
		return status;
	}

	public void setStatus(Map<String, Integer> status) {
		this.status = status;
	}

}
