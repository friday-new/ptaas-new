/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.request;

import javax.validation.constraints.NotNull;

//@ApiModel(description = "Awr Request object is used for generating AWR Report")
public class AwrRequest {

	// @ApiModelProperty(notes = "Oltp Instance Reference")
	@NotNull
	private String oltpref;
	// @ApiModelProperty(notes = "Path at which report to be stored")
	@NotNull
	private String reportpath;
	// @ApiModelProperty(notes = "start snap id of generating AWR")
	@NotNull
	private Integer startid;
	// @ApiModelProperty(notes = "end snap id of generating AWR")
	@NotNull
	private Integer endid;

	public String getOltpref() {
		return oltpref;
	}

	public void setOltpref(String oltpref) {
		this.oltpref = oltpref;
	}

	public String getReportpath() {
		return reportpath;
	}

	public void setReportpath(String reportpath) {
		this.reportpath = reportpath;
	}

	public Integer getStartid() {
		return startid;
	}

	public void setStartid(Integer startid) {
		this.startid = startid;
	}

	public Integer getEndid() {
		return endid;
	}

	public void setEndid(Integer endid) {
		this.endid = endid;
	}

}
