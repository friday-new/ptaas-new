/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import java.io.IOException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.yodlee.perf.fridaynew.model.response.ComponentResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@Entity
public class Simulator implements InstanceRelated {

	@Id
	private Long simulatorId;
	private String simulatorName;
	private String simulatorIp;
	private Long simulatorPort;
	private String dockerRepositoryName;
	private String username;
	private String password;
	private String dockerVip;
	private Long dockerVipPort;
	private String simulatorInstanceRef;
	private String simulatorInUse;
	private String protocol;

	public Long getSimulatorId() {
		return simulatorId;
	}

	public void setSimulatorId(Long simulatorId) {
		this.simulatorId = simulatorId;
	}

	public String getSimulatorName() {
		return simulatorName;
	}

	public void setSimulatorName(String simulatorName) {
		this.simulatorName = simulatorName;
	}

	public String getSimulatorIp() {
		return simulatorIp;
	}

	public void setSimulatorIp(String simulatorIp) {
		this.simulatorIp = simulatorIp;
	}

	public Long getSimulatorPort() {
		return simulatorPort;
	}

	public void setSimulatorPort(Long simulatorPort) {
		this.simulatorPort = simulatorPort;
	}

	public String getDockerRepositoryName() {
		return dockerRepositoryName;
	}

	public void setDockerRepositoryName(String dockerRepositoryName) {
		this.dockerRepositoryName = dockerRepositoryName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDockerVip() {
		return dockerVip;
	}

	public void setDockerVip(String dockerVip) {
		this.dockerVip = dockerVip;
	}

	public Long getDockerVipPort() {
		return dockerVipPort;
	}

	public void setDockerVipPort(Long dockerVipPort) {
		this.dockerVipPort = dockerVipPort;
	}

	public String getSimulatorInstanceRef() {
		return simulatorInstanceRef;
	}

	public void setSimulatorInstanceRef(String simulatorInstanceRef) {
		this.simulatorInstanceRef = simulatorInstanceRef;
	}

	public String getSimulatorInUse() {
		return simulatorInUse;
	}

	public void setSimulatorInUse(String simulatorInUse) {
		this.simulatorInUse = simulatorInUse;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	@Override
	public ResponseEntity<Object> startComponent(Integer containerNumber) {

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(getUsername());
		byte[] decryptedPassword = decoder.decode(getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		try {
			Runtime.getRuntime()
					.exec("/backup/Users/caradhya/RootUserFiles/SimulatorStartComponent.sh " + userName + " "
							+ getSimulatorIp() + " " + passWord + " " + getDockerVipPort() + " " + getSimulatorPort()
							+ " " + getDockerRepositoryName());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> copyLogs(String logLocation) {

		return null;
	}

	@Override
	public ResponseEntity<Object> logPurge() {

		return null;
	}

	@Override
	public ResponseEntity<Object> stopComponent() {

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(getUsername());
		byte[] decryptedPassword = decoder.decode(getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		try {
			Runtime.getRuntime().exec("/backup/Users/caradhya/RootUserFiles/SimulatorStopComponent.sh " + userName + " "
					+ getSimulatorIp() + " " + passWord + " " + getDockerRepositoryName());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public ResponseEntity<Object> checkComponent(String jsonLog) {

		ComponentResponse componentResponse = new ComponentResponse();
		componentResponse.setStarted(false);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Map> actuatorResponse = null;

		try {
			actuatorResponse = restTemplate.getForEntity(
					getProtocol() + "://" + getDockerVip() + ":" + getDockerVipPort() + "/actuator/health", Map.class);
		} catch (Exception e) {
			actuatorResponse = null;
		}

		if (actuatorResponse == null)
			componentResponse.setStarted(false);
		else {
			if (actuatorResponse.getBody().get("status").equals("UP"))
				componentResponse.setStarted(true);
			else
				componentResponse.setStarted(false);
		}

		return new ResponseEntity<>(componentResponse, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> reStartComponent() {

		return null;
	}

}
