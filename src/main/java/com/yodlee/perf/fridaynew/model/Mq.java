/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.lang.Nullable;

@Entity
public class Mq {

	@Id
	private Integer mqid;
	private String mqobjname;
	private String mqipaddr;
	private Long mqport;
	private String mqname;
	@Nullable
	private String mqref;
	private String mqinuse;

	public Integer getMqid() {
		return mqid;
	}

	public void setMqid(Integer mqid) {
		this.mqid = mqid;
	}

	public String getMqobjname() {
		return mqobjname;
	}

	public void setMqobjname(String mqobjname) {
		this.mqobjname = mqobjname;
	}

	public String getMqipaddr() {
		return mqipaddr;
	}

	public void setMqipaddr(String mqipaddr) {
		this.mqipaddr = mqipaddr;
	}

	public Long getMqport() {
		return mqport;
	}

	public void setMqport(Long mqport) {
		this.mqport = mqport;
	}

	public String getMqname() {
		return mqname;
	}

	public void setMqname(String mqname) {
		this.mqname = mqname;
	}

	public String getMqref() {
		return mqref;
	}

	public void setMqref(String mqref) {
		this.mqref = mqref;
	}

	public String getMqinuse() {
		return mqinuse;
	}

	public void setMqinuse(String mqinuse) {
		this.mqinuse = mqinuse;
	}

}
