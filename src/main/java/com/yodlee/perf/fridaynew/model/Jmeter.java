/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Jmeter {

	@Id
	private Integer instanceid;
	private String instancename;
	private String serviceip;
	private Integer serviceport;
	private String username;
	private String password;
	private String homefolder;

	public Integer getInstanceid() {
		return instanceid;
	}

	public void setInstanceid(Integer instanceid) {
		this.instanceid = instanceid;
	}

	public String getInstancename() {
		return instancename;
	}

	public void setInstancename(String instancename) {
		this.instancename = instancename;
	}

	public String getServiceip() {
		return serviceip;
	}

	public void setServiceip(String serviceip) {
		this.serviceip = serviceip;
	}

	public int getServiceport() {
		return serviceport;
	}

	public void setServiceport(int serviceport) {
		this.serviceport = serviceport;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHomefolder() {
		return homefolder;
	}

	public void setHomefolder(String homefolder) {
		this.homefolder = homefolder;
	}

}
