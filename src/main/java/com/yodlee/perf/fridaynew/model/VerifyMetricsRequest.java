/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import java.util.List;

import javax.validation.constraints.NotNull;

//@ApiModel(description = "Verify Metrics Request for verifying the execution metrics")
public class VerifyMetricsRequest {

	// @ApiModelProperty(notes = "Testcase id")
	@NotNull
	private String testcaseid;
	// @ApiModelProperty(notes = "Execution Tracking id")
	@NotNull
	private String uniqueid;
	// @ApiModelProperty(notes = "Instance Reference for verifying the instance
	// level")
	@NotNull
	private String instanceref;
	// @ApiModelProperty(notes = "maximum 2 values it can take")
	private List<String> interval;
	// @ApiModelProperty(notes = "Branch in which the test case got executed")
	private String branch;

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getTestcaseid() {
		return testcaseid;
	}

	public void setTestcaseid(String testcaseid) {
		this.testcaseid = testcaseid;
	}

	public String getUniqueid() {
		return uniqueid;
	}

	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}

	public String getInstanceref() {
		return instanceref;
	}

	public void setInstanceref(String instanceref) {
		this.instanceref = instanceref;
	}

	public List<String> getInterval() {
		return interval;
	}

	public void setInterval(List<String> interval) {
		this.interval = interval;
	}

}
