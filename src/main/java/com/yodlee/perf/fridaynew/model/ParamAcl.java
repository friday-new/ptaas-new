/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import java.sql.Date;

public class ParamAcl {

	private Long paramAclId;
	private String aclName;
	private String descripion;
	private Integer isDeprecated;
	private String aclValue;
	private Long aclValueTypeId;
	private Integer canOverrideValue;
	private Date rowCreated;
	private Date rowLastUpdated;
	private Long productCatalogId;
	private Long aclCategoryId;
	private Long conflictResolutionTypeId;
	private String minValue;
	private String maxValue;
	private Long addedVersion;
	private Long deprecatedVersion;
	private Long gpmStatusId;
	private String mcKey;
	private Long paramAclTypeId;
	private String retrievalHierarchy;

	public Long getParamAclId() {
		return paramAclId;
	}

	public void setParamAclId(Long paramAclId) {
		this.paramAclId = paramAclId;
	}

	public String getAclName() {
		return aclName;
	}

	public void setAclName(String aclName) {
		this.aclName = aclName;
	}

	public String getDescripion() {
		return descripion;
	}

	public void setDescripion(String descripion) {
		this.descripion = descripion;
	}

	public Integer getIsDeprecated() {
		return isDeprecated;
	}

	public void setIsDeprecated(Integer isDeprecated) {
		this.isDeprecated = isDeprecated;
	}

	public String getAclValue() {
		return aclValue;
	}

	public void setAclValue(String aclValue) {
		this.aclValue = aclValue;
	}

	public Long getAclValueTypeId() {
		return aclValueTypeId;
	}

	public void setAclValueTypeId(Long aclValueTypeId) {
		this.aclValueTypeId = aclValueTypeId;
	}

	public Integer getCanOverrideValue() {
		return canOverrideValue;
	}

	public void setCanOverrideValue(Integer canOverrideValue) {
		this.canOverrideValue = canOverrideValue;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowLastUpdated() {
		return rowLastUpdated;
	}

	public void setRowLastUpdated(Date rowLastUpdated) {
		this.rowLastUpdated = rowLastUpdated;
	}

	public Long getProductCatalogId() {
		return productCatalogId;
	}

	public void setProductCatalogId(Long productCatalogId) {
		this.productCatalogId = productCatalogId;
	}

	public Long getAclCategoryId() {
		return aclCategoryId;
	}

	public void setAclCategoryId(Long aclCategoryId) {
		this.aclCategoryId = aclCategoryId;
	}

	public Long getConflictResolutionTypeId() {
		return conflictResolutionTypeId;
	}

	public void setConflictResolutionTypeId(Long conflictResolutionTypeId) {
		this.conflictResolutionTypeId = conflictResolutionTypeId;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public Long getAddedVersion() {
		return addedVersion;
	}

	public void setAddedVersion(Long addedVersion) {
		this.addedVersion = addedVersion;
	}

	public Long getDeprecatedVersion() {
		return deprecatedVersion;
	}

	public void setDeprecatedVersion(Long deprecatedVersion) {
		this.deprecatedVersion = deprecatedVersion;
	}

	public Long getGpmStatusId() {
		return gpmStatusId;
	}

	public void setGpmStatusId(Long gpmStatusId) {
		this.gpmStatusId = gpmStatusId;
	}

	public String getMcKey() {
		return mcKey;
	}

	public void setMcKey(String mcKey) {
		this.mcKey = mcKey;
	}

	public Long getParamAclTypeId() {
		return paramAclTypeId;
	}

	public void setParamAclTypeId(Long paramAclTypeId) {
		this.paramAclTypeId = paramAclTypeId;
	}

	public String getRetrievalHierarchy() {
		return retrievalHierarchy;
	}

	public void setRetrievalHierarchy(String retrievalHierarchy) {
		this.retrievalHierarchy = retrievalHierarchy;
	}

}
