/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.request;

import javax.validation.constraints.NotNull;

//@ApiModel(description = "Create Execution Folder object is used for creating the Execution folder based on the execution tracking Id")
public class CreateExecFolder {

	// @ApiModelProperty(notes = "Predefined Version in which the test case is gets
	// executed")
	@NotNull
	private String version;
	// @ApiModelProperty(notes = "Predefined Portfolio in which the test case is
	// gets executed")
	@NotNull
	private String portfolio;
	// @ApiModelProperty(notes = "Testcase ID, this should not contain the special
	// charaters apart from - or _")
	@NotNull
	private String testcaseid;
	// @ApiModelProperty(notes = "cobrand or subbrand")
	@NotNull
	private String environment;
	// @ApiModelProperty(notes = "Unique id for tracking purposes")
	@NotNull
	private String uniqueId;
	// @ApiModelProperty(notes = "User in which the test case get Executed")
	@NotNull
	private String user;
	// @ApiModelProperty(notes = "Jmeter instance name which is used to execute the
	// test case")
	@NotNull
	private String jmeterinstancename;
	// @ApiModelProperty(notes = "Jtl complete path in which the test case is being
	// executed")
	@NotNull
	private String jtlfilepath;

	public String getJmeterinstancename() {
		return jmeterinstancename;
	}

	public void setJmeterinstancename(String jmeterinstancename) {
		this.jmeterinstancename = jmeterinstancename;
	}

	public String getJtlfilepath() {
		return jtlfilepath;
	}

	public void setJtlfilepath(String jtlfilepath) {
		this.jtlfilepath = jtlfilepath;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(String portfolio) {
		this.portfolio = portfolio;
	}

	public String getTestcaseid() {
		return testcaseid;
	}

	public void setTestcaseid(String testcaseid) {
		this.testcaseid = testcaseid;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
