/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import java.sql.Date;

public class CobrandAclValue {

	private Long cobrandAclValueId;
	private Long cobrandId;
	private Long paramAclId;
	private String aclValue;
	private Date rowCreated;
	private Date rowLastUpdated;
	private String minValue;
	private String maxValue;
	private String overrideValue;
	private Date validUntil;
	private Long gpmStatusId;
	private String appId;

	public Long getCobrandAclValueId() {
		return cobrandAclValueId;
	}

	public void setCobrandAclValueId(Long cobrandAclValueId) {
		this.cobrandAclValueId = cobrandAclValueId;
	}

	public Long getCobrandId() {
		return cobrandId;
	}

	public void setCobrandId(Long cobrandId) {
		this.cobrandId = cobrandId;
	}

	public Long getParamAclId() {
		return paramAclId;
	}

	public void setParamAclId(Long paramAclId) {
		this.paramAclId = paramAclId;
	}

	public String getAclValue() {
		return aclValue;
	}

	public void setAclValue(String aclValue) {
		this.aclValue = aclValue;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowLastUpdated() {
		return rowLastUpdated;
	}

	public void setRowLastUpdated(Date rowLastUpdated) {
		this.rowLastUpdated = rowLastUpdated;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public String getOverrideValue() {
		return overrideValue;
	}

	public void setOverrideValue(String overrideValue) {
		this.overrideValue = overrideValue;
	}

	public Date getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public Long getGpmStatusId() {
		return gpmStatusId;
	}

	public void setGpmStatusId(Long gpmStatusId) {
		this.gpmStatusId = gpmStatusId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

}
