/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.request;

import javax.validation.constraints.NotNull;

public class TestcaseverdictRequest {

	@NotNull
	private ProcessCpuVerdict processCpuVerdict;
	@NotNull
	private VmCpuVerdict vmCpuVerdict;
	@NotNull
	private String build;
	@NotNull
	private String instanceType;
	@NotNull
	private TpsVerdict tpsVerdict;
	private Long testCaseVerdictId;
	@NotNull
	private String description;
	@NotNull
	private HeapUsedVerdict heapUsedVerdict;
	@NotNull
	private Long interval;
	@NotNull
	private String branch;
	@NotNull
	private MemoryVerdict memoryVerdict;
	@NotNull
	private String testCaseId;

	public ProcessCpuVerdict getProcessCpuVerdict() {
		return processCpuVerdict;
	}

	public void setProcessCpuVerdict(ProcessCpuVerdict processCpuVerdict) {
		this.processCpuVerdict = processCpuVerdict;
	}

	public VmCpuVerdict getVmCpuVerdict() {
		return vmCpuVerdict;
	}

	public void setVmCpuVerdict(VmCpuVerdict vmCpuVerdict) {
		this.vmCpuVerdict = vmCpuVerdict;
	}

	public String getBuild() {
		return build;
	}

	public void setBuild(String build) {
		this.build = build;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	public TpsVerdict getTpsVerdict() {
		return tpsVerdict;
	}

	public void setTpsVerdict(TpsVerdict tpsVerdict) {
		this.tpsVerdict = tpsVerdict;
	}

	public Long getTestCaseVerdictId() {
		return testCaseVerdictId;
	}

	public void setTestCaseVerdictId(Long testCaseVerdictId) {
		this.testCaseVerdictId = testCaseVerdictId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public HeapUsedVerdict getHeapUsedVerdict() {
		return heapUsedVerdict;
	}

	public void setHeapUsedVerdict(HeapUsedVerdict heapUsedVerdict) {
		this.heapUsedVerdict = heapUsedVerdict;
	}

	public Long getInterval() {
		return interval;
	}

	public void setInterval(Long interval) {
		this.interval = interval;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public MemoryVerdict getMemoryVerdict() {
		return memoryVerdict;
	}

	public void setMemoryVerdict(MemoryVerdict memoryVerdict) {
		this.memoryVerdict = memoryVerdict;
	}

	public String getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}

}
