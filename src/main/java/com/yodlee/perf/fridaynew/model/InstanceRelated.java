/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import org.springframework.http.ResponseEntity;

public interface InstanceRelated {

	ResponseEntity<Object> startComponent(Integer containerNumber);

	ResponseEntity<Object> copyLogs(String logLocation);

	ResponseEntity<Object> logPurge();

	ResponseEntity<Object> stopComponent();

	ResponseEntity<Object> checkComponent(String jsonLog);

	ResponseEntity<Object> reStartComponent();

}
