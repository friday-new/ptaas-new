/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.response;

//@ApiModel(description = "Execution Folder Response provides Path, URL and verdict of the Execution")
public class ExecFolderRsp {

	// @ApiModelProperty(notes = "Path of the Test Execution")
	private String folderPath;
	// @ApiModelProperty(notes = "TestResults.txt URL for the Execution")
	private String testResultsUrl;
	// @ApiModelProperty(notes = "TestResults.html URL for the Execution")
	private String metricSummary;
	// @ApiModelProperty(notes = "Verdict of the Execution")
	private String verdict;

	public String getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public String getTestResultsUrl() {
		return testResultsUrl;
	}

	public void setTestResultsUrl(String testResultsUrl) {
		this.testResultsUrl = testResultsUrl;
	}

	public String getMetricSummary() {
		return metricSummary;
	}

	public void setMetricSummary(String metricSummary) {
		this.metricSummary = metricSummary;
	}

	public String getVerdict() {
		return verdict;
	}

	public void setVerdict(String verdict) {
		this.verdict = verdict;
	}

}
