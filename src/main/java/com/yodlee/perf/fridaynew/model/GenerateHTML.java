/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class GenerateHTML {

	static String[] tableHeaders = { "Metric", "Average", "Min", "Max", "Nintyfivepercentile", "Nintypercentile",
			"verdict" };

	public static void generateSummary(String filePath, String uniqueId) {
		JsonObject jsonObject = getSummaryReport(uniqueId);

		String testSummary = generateTestSummary(jsonObject.get("testsummary").getAsJsonObject());
		String links = generateLinks(jsonObject.get("links").getAsJsonObject());
		String lines = "";
		String line = null;
		try {
			FileReader fr = new FileReader(filePath);
			BufferedReader br = new BufferedReader(fr);

			while ((line = br.readLine()) != null) {
				lines += line;
			}
			br.close();
			String newString = lines.substring(0, lines.indexOf("<header>") + 8) + testSummary + links
					+ lines.substring(lines.indexOf("</header>"));

			FileWriter fw = new FileWriter(filePath);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(newString);
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void generateMetrics(String filePath, String uniqueId, String instanceRef, String testCaseId,
			Long interval) {

		JsonObject jsonObject = getMetricReport(uniqueId, instanceRef, testCaseId, interval);

		String componentSummary = generateComponentSummary(jsonObject.get("componentsummary").getAsJsonObject(),
				jsonObject.get("componentlink").getAsJsonObject());
		String metrics = generateMetrics(jsonObject.get("metrics").getAsJsonObject());
		String lines = "";
		String line = null;
		try {
			FileReader fr = new FileReader(filePath);
			BufferedReader br = new BufferedReader(fr);

			while ((line = br.readLine()) != null) {
				lines += line;
			}
			br.close();
			String newString = lines.substring(0, lines.lastIndexOf("</section>") + 10) + "<section id=\""
					+ jsonObject.get("componentsummary").getAsJsonObject().get("objname").getAsString() + "_metric\">"
					+ componentSummary + metrics + "</section>" + lines.substring(lines.lastIndexOf("</section>") + 10);

			FileWriter fw = new FileWriter(filePath);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(newString);
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String generateMetrics(JsonObject metrics) {
		String tableHeaderStr = "<tr>";

		String tableBodyStr = "";
		List<String> keys = metrics.entrySet().stream().map(i -> i.getKey())
				.collect(Collectors.toCollection(ArrayList::new));

		for (int i = 0; i < tableHeaders.length; i++) {
			tableHeaderStr += "<th>" + tableHeaders[i] + "</th>";

		}

		for (int i = 0; i < keys.size(); i++) {
			tableBodyStr += " <tr> <td>" + keys.get(i) + "</td>";
			JsonObject jsonObj = metrics.get(keys.get(i)).getAsJsonObject();

			for (int j = 1; j < tableHeaders.length; j++) {
				tableBodyStr += " <td class=\"status-"
						+ jsonObj.get(tableHeaders[j]).getAsJsonObject().get("verdict").getAsString().toLowerCase()
						+ "\">" + jsonObj.get(tableHeaders[j]).getAsJsonObject().get("value").getAsString() + "</td>";
			}
		}
		tableHeaderStr += "</tr>";
		String metricsStr = "<table style=\"width: 100%\">" + tableHeaderStr + tableBodyStr + "  </table>";
		return metricsStr;
	}

	private static String generateComponentSummary(JsonObject componentSummary, JsonObject componentLink) {
		String componentSummaryStr = "<h1>" + componentSummary.get("objname").getAsString()
				+ "&nbsp;&nbsp;&nbsp;Metrics</h1> <table style=\"width: 100%\"> <tr> <td colspan=\"2\"> Object Name : "
				+ componentSummary.get("objname").getAsString() + "</td> <td colspan=\"2\"> Object Reference : "
				+ componentSummary.get("objref").getAsString() + "</td> <td colspan=\"2\"> Build : "
				+ componentSummary.get("build").getAsString() + "</td> </tr> <tr> <td colspan=\"2\"><a href=\""
				+ componentLink.get("zabbixlink").getAsString()
				+ "\" target=\"_blank\">Zabbix Link</a></td> <td colspan=\"2\"><a href=\""
				+ componentLink.get("specification").getAsString()
				+ "\" target=\"_blank\">Specification</a></td> <td colspan=\"2\"> Jvm Throughput : "
				+ componentLink.get("jvmThroughput").getAsString() + "</td> </tr> </table>";
		return componentSummaryStr;
	}

	public static String generateTestSummary(JsonObject testSummary) {

		JsonObject tps = testSummary.get("tpsVerdict").getAsJsonObject();
		String tpsValue = tps.get("value").getAsString();
		String tpsVerdict = tps.get("verdict").getAsString().toLowerCase();

		String testSummaryStr = "<section id=\"test-summary\"> <h1>Test summary</h1> <table> <tr> <td>Test case</td> <td>"
				+ testSummary.get("testCaseId").getAsString()
				+ "</td> </tr> <tr> <td>Final Verdict</td> <td class=\"status-"
				+ testSummary.get("finalVerdict").getAsString().toLowerCase() + "\">"
				+ testSummary.get("finalVerdict").getAsString()
				+ "</td> </tr> <tr> <td>Component Metrics Verdict</td> <td class=\"status-"
				+ testSummary.get("componentMetricsVerdict").getAsString().toLowerCase() + "\">"
				+ testSummary.get("componentMetricsVerdict").getAsString()
				+ "</td> </tr> <tr> <td>TPS Verdict</td> <td class=\"status-" + tpsVerdict + "\">" + tpsValue
				+ "</td> </tr>	<tr> <td>Sql Verdict</td> <td class=\"status-"
				+ testSummary.get("sqlVerdict").getAsString().toLowerCase() + "\">"
				+ testSummary.get("sqlVerdict").getAsString() + "</td></tr> <tr> <td>Error Percentage</td> <td>"
				+ testSummary.get("errorPercentage").getAsString() + "</td></tr> <tr> <td>Description</td> <td>"
				+ testSummary.get("description").getAsString() + "</td></tr> </table> </section>";
		return testSummaryStr;

	}

	public static String generateLinks(JsonObject links) {
		String linkStr = "<section id=\"links\"> <h1>Links</h1> <table>";
		List<String> keys = links.entrySet().stream().map(i -> i.getKey())
				.collect(Collectors.toCollection(ArrayList::new));
		for (int i = 0; i < keys.size(); i++) {
			linkStr += "<tr> <td><a href=\"" + links.get(keys.get(i)).getAsString() + "\" target=\"_blank\">"
					+ keys.get(i) + "</a></td> </tr> ";
		}
		linkStr += "</table> </section>";
		return linkStr;
	}

	public static void createFile(String filePath) {

		String initHTML = "<!doctype html><html><head> <title> Test case summery report </title> <style> table { min-width: 25%; } table, th, td { border: 1px solid black; border-collapse: collapse; } th, td { padding: 10px; text-align: center; } table.pattern tr:nth-child(even) { background-color: #eee; } table.pattern tr:nth-child(odd) { background-color: #fff; } table.pattern th { background-color: black; color: white; } .status-fail { background-color: red; color: white } .status-pass { background-color: green; color: white } </style></head><body><header></header><section id=\"sample\"></section></body></html>";
		File f = new File(filePath);
		if (!f.exists()) {
			try {
				f.createNewFile();
				FileOutputStream writer = new FileOutputStream(f);
				writer.write(initHTML.getBytes());
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public static JsonObject getSummaryReport(String uniqueId) {

		RestTemplate restTemplate = new RestTemplate();
		String json = restTemplate.getForObject("http://localhost:8183/getSummary/" + uniqueId, String.class);
		JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
		// JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
		return jsonObject;
	}

	public static JsonObject getMetricReport(String uniqueId, String instanceRef, String testCaseId, Long interval) {

		RestTemplate restTemplate = new RestTemplate();
		Map<String, String> args = new HashMap<String, String>();
		args.put("uniqueId", uniqueId);
		args.put("testCaseId", testCaseId);
		args.put("instanceRef", instanceRef);
		args.put("interval", interval.toString());
		String json = restTemplate.postForObject("http://localhost:8183/generateMetricsData/", args, String.class);
		JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
		// JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
		return jsonObject;
	}

}
