/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.request;

public class ResiliencyRequest {

	private String sourceInstanceRef;
	private String destinationInstanceRef;

	public String getSourceInstanceRef() {
		return sourceInstanceRef;
	}

	public void setSourceInstanceRef(String sourceInstanceRef) {
		this.sourceInstanceRef = sourceInstanceRef;
	}

	public String getDestinationInstanceRef() {
		return destinationInstanceRef;
	}

	public void setDestinationInstanceRef(String destinationInstanceRef) {
		this.destinationInstanceRef = destinationInstanceRef;
	}

}
