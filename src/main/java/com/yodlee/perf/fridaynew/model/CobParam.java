/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import java.sql.Date;

public class CobParam {

	private Long cobParamId;
	private String paramValue;
	private Long cobrandId;
	private Long paramKeyId;
	private String appId;
	private Integer isPropertyBagParam;
	private Long segmentsId;
	private Date rowCreated;
	private Date rowLastUpdated;

	public Long getCobParamId() {
		return cobParamId;
	}

	public void setCobParamId(Long cobParamId) {
		this.cobParamId = cobParamId;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public Long getCobrandId() {
		return cobrandId;
	}

	public void setCobrandId(Long cobrandId) {
		this.cobrandId = cobrandId;
	}

	public Long getParamKeyId() {
		return paramKeyId;
	}

	public void setParamKeyId(Long paramKeyId) {
		this.paramKeyId = paramKeyId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Integer getIsPropertyBagParam() {
		return isPropertyBagParam;
	}

	public void setIsPropertyBagParam(Integer isPropertyBagParam) {
		this.isPropertyBagParam = isPropertyBagParam;
	}

	public Long getSegmentsId() {
		return segmentsId;
	}

	public void setSegmentsId(Long segmentsId) {
		this.segmentsId = segmentsId;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowLastUpdated() {
		return rowLastUpdated;
	}

	public void setRowLastUpdated(Date rowLastUpdated) {
		this.rowLastUpdated = rowLastUpdated;
	}

}
