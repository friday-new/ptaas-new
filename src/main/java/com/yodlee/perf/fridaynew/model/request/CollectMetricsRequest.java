/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.request;

import javax.validation.constraints.NotNull;

//@ApiModel(description = "Collect Metrics Request object is used for collecting the metrics for an instance")
public class CollectMetricsRequest {

	// @ApiModelProperty(notes = "Test case ID")
	@NotNull
	private String testcaseid;
	// @ApiModelProperty(notes = "Execution Tracking ID")
	@NotNull
	private String uniqueid;
	// @ApiModelProperty(notes = "Instance Reference")
	@NotNull
	private String instanceref;
	// @ApiModelProperty(notes = "Start time in secs after what time of test
	// starting to be calculted")
	private Integer deltastarttime;

	// @ApiModelProperty(notes = "Interval based on the testcase")
	private Integer interval;
	// @ApiModelProperty(notes = "Duration for which the metrics to be calculted")
	private Integer duration;

	public Integer getDeltastarttime() {
		return deltastarttime;
	}

	public void setDeltastarttime(Integer deltastarttime) {
		this.deltastarttime = deltastarttime;
	}

	public String getTestcaseid() {
		return testcaseid;
	}

	public void setTestcaseid(String testcaseid) {
		this.testcaseid = testcaseid;
	}

	public String getUniqueid() {
		return uniqueid;
	}

	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}

	public String getInstanceref() {
		return instanceref;
	}

	public void setInstanceref(String instanceref) {
		this.instanceref = instanceref;
	}

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

}
