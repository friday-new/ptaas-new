/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.request;

import javax.validation.constraints.NotNull;

//@ApiModel(description = "Zabbix Report Request for generating the Zabbix Report")
public class ZabbixReportReq {

	// @ApiModelProperty(notes = "Instance Reference for which zabbix report to be
	// generated")
	@NotNull
	private String instanceRef;
	// @ApiModelProperty(notes = "Start Time from which Zabbix data to be captured")
	@NotNull
	private String startTime;
	// @ApiModelProperty(notes = "End Time from which Zabbix data to be captured")
	@NotNull
	private String endTime;
	// @ApiModelProperty(notes = "Destination or Execution folder in which data to
	// be copied")
	@NotNull
	private String destination;
	// @ApiModelProperty(notes = "AWR Report path for an testcase")
	private String awrPath;
	// @ApiModelProperty(notes = "Oltp instance Reference")
	private String oltpRef;
	// @ApiModelProperty(notes = "Interval based on the testcase")
	private Integer interval;

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getOltpRef() {
		return oltpRef;
	}

	public void setOltpRef(String oltpRef) {
		this.oltpRef = oltpRef;
	}

	public String getAwrPath() {
		return awrPath;
	}

	public void setAwrPath(String awrPath) {
		this.awrPath = awrPath;
	}

	public String getInstanceRef() {
		return instanceRef;
	}

	public void setInstanceRef(String instanceRef) {
		this.instanceRef = instanceRef;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
