/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "ChangeGathererModel", description = "Change Gatherer Model object is used for changing the default values of gatherer config file")
public class ChangeGathererModel {

	@Schema(name = "object", required = true, description = "Instance reference	of the	Gatherer object", example = "Gatherer_192_168_136_93_1_1234567890111")
	@NotNull
	private String object;
	@Schema(name = "key", required = true, description = "Key which needs to bechanged", example = "someKey")
	@NotNull
	private String key;
	@Schema(name = "value", required = false, description = "New value forupdating the file", example = "someValue")
	private String value;
	@Schema(name = "filename", required = false, description = "Properties Filename", example = "filename")
	private String filename;
	@Schema(name = "folderPath", required = false, description = "Folder name inwhich properties File present", example = "somefolderName")
	private String folderPath;
	@Schema(name = "oltpRef", required = false, description = "Oltp reference forchecking thestart date if not given", example = "Oltp_192_168_136_93_1_1234567890111")
	private String oltpRef;
	@Schema(name = "loginName", required = false, description = "User pattrenprefix for checking thestart date", example = "PtaasUser_")
	private String loginName;
	@Schema(name = "siteId", required = false, description = "site Id for theuser pattern", example = "16375")
	private Long siteId;

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getOltpRef() {
		return oltpRef;
	}

	public void setOltpRef(String oltpRef) {
		this.oltpRef = oltpRef;
	}

	public String getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

}
