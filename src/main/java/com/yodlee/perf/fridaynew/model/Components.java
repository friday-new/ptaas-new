/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yodlee.perf.fridaynew.model.response.ComponentResponse;
import com.yodlee.perf.fridaynew.service.ComponentsRelated;
import com.yodlee.perf.fridaynew.service.ServiceClass;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

@Entity
public class Components implements InstanceRelated {

	@Id
	private Integer instanceid;
	private String instancename;
	private String serviceip;
	@Nullable
	private Long serviceport;
	@Nullable
	private Integer jbossinstance;
	@Nullable
	private String containerid;
	@Nullable
	private String startuppath;
	@Nullable
	private String startupfilename;
	@Nullable
	private String stopfilepath;
	@Nullable
	private String stopfilefilename;
	private String serverlogpath;
	private String serverlogpattern;
	private String accesslogpath;
	private String accesslogpattern;
	private String corelogpath;
	private String corelogpattern;
	private String jvmlogpath;
	private String jvmlogpattern;
	private String username;
	private String password;
	@Nullable
	private String vip;
	@Nullable
	private Long viport;
	private Integer zabbixelementid;
	@Nullable
	private String instanceref;
	private String instanceinuse;
	@Nullable
	private String sutversion;
	private String isdocker;
	@Nullable
	private Integer controlport;
	private String projectname;
	private String clustername;
	private String namespaceid;
	private String activepool;
	private String dockerid;
	private String zabbixhostname;
	private String protocol;
	private String service;
	private String gctype;

	public String getGctype() {
		return gctype;
	}

	public void setGctype(String gctype) {
		this.gctype = gctype;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getZabbixhostname() {
		return zabbixhostname;
	}

	public void setZabbixhostname(String zabbixhostname) {
		this.zabbixhostname = zabbixhostname;
	}

	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}

	public String getClustername() {
		return clustername;
	}

	public void setClustername(String clustername) {
		this.clustername = clustername;
	}

	public String getNamespaceid() {
		return namespaceid;
	}

	public void setNamespaceid(String namespaceid) {
		this.namespaceid = namespaceid;
	}

	public String getActivepool() {
		return activepool;
	}

	public void setActivepool(String activepool) {
		this.activepool = activepool;
	}

	public String getDockerid() {
		return dockerid;
	}

	public void setDockerid(String dockerid) {
		this.dockerid = dockerid;
	}

	public Integer getControlport() {
		return controlport;
	}

	public void setControlport(Integer controlport) {
		this.controlport = controlport;
	}

	public Integer getInstanceid() {
		return instanceid;
	}

	public void setInstanceid(Integer instanceid) {
		this.instanceid = instanceid;
	}

	public String getInstancename() {
		return instancename;
	}

	public void setInstancename(String instancename) {
		this.instancename = instancename;
	}

	public String getServiceip() {
		return serviceip;
	}

	public void setServiceip(String serviceip) {
		this.serviceip = serviceip;
	}

	public Long getServiceport() {
		return serviceport;
	}

	public void setServiceport(Long serviceport) {
		this.serviceport = serviceport;
	}

	public Integer getJbossinstance() {
		return jbossinstance;
	}

	public void setJbossinstance(Integer jbossinstance) {
		this.jbossinstance = jbossinstance;
	}

	public String getContainerid() {
		return containerid;
	}

	public void setContainerid(String containerid) {
		this.containerid = containerid;
	}

	public String getStartuppath() {
		return startuppath;
	}

	public void setStartuppath(String startuppath) {
		this.startuppath = startuppath;
	}

	public String getStartupfilename() {
		return startupfilename;
	}

	public void setStartupfilename(String startupfilename) {
		this.startupfilename = startupfilename;
	}

	public String getStopfilepath() {
		return stopfilepath;
	}

	public void setStopfilepath(String stopfilepath) {
		this.stopfilepath = stopfilepath;
	}

	public String getStopfilefilename() {
		return stopfilefilename;
	}

	public void setStopfilefilename(String stopfilefilename) {
		this.stopfilefilename = stopfilefilename;
	}

	public String getServerlogpath() {
		return serverlogpath;
	}

	public void setServerlogpath(String serverlogpath) {
		this.serverlogpath = serverlogpath;
	}

	public String getServerlogpattern() {
		return serverlogpattern;
	}

	public void setServerlogpattern(String serverlogpattern) {
		this.serverlogpattern = serverlogpattern;
	}

	public String getAccesslogpath() {
		return accesslogpath;
	}

	public void setAccesslogpath(String accesslogpath) {
		this.accesslogpath = accesslogpath;
	}

	public String getAccesslogpattern() {
		return accesslogpattern;
	}

	public void setAccesslogpattern(String accesslogpattern) {
		this.accesslogpattern = accesslogpattern;
	}

	public String getCorelogpath() {
		return corelogpath;
	}

	public void setCorelogpath(String corelogpath) {
		this.corelogpath = corelogpath;
	}

	public String getCorelogpattern() {
		return corelogpattern;
	}

	public void setCorelogpattern(String corelogpattern) {
		this.corelogpattern = corelogpattern;
	}

	public String getJvmlogpath() {
		return jvmlogpath;
	}

	public void setJvmlogpath(String jvmlogpath) {
		this.jvmlogpath = jvmlogpath;
	}

	public String getJvmlogpattern() {
		return jvmlogpattern;
	}

	public void setJvmlogpattern(String jvmlogpattern) {
		this.jvmlogpattern = jvmlogpattern;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getVip() {
		return vip;
	}

	public void setVip(String vip) {
		this.vip = vip;
	}

	public Long getViport() {
		return viport;
	}

	public void setViport(Long viport) {
		this.viport = viport;
	}

	public Integer getZabbixelementid() {
		return zabbixelementid;
	}

	public void setZabbixelementid(Integer zabbixelementid) {
		this.zabbixelementid = zabbixelementid;
	}

	public String getInstanceref() {
		return instanceref;
	}

	public void setInstanceref(String instanceref) {
		this.instanceref = instanceref;
	}

	public String getInstanceinuse() {
		return instanceinuse;
	}

	public void setInstanceinuse(String instanceinuse) {
		this.instanceinuse = instanceinuse;
	}

	public String getSutversion() {
		return sutversion;
	}

	public void setSutversion(String sutversion) {
		this.sutversion = sutversion;
	}

	public String getIsdocker() {
		return isdocker;
	}

	public void setIsdocker(String isdocker) {
		this.isdocker = isdocker;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	@Override
	public ResponseEntity<Object> startComponent(Integer containerNumber) {

		String finalString = "Completed";

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(getUsername());
		byte[] decryptedPassword = decoder.decode(getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		ComponentsRelated componentsRelated = new ComponentsRelated();

		if (!componentsRelated.startOrStopComponent(userName, getServiceip(), passWord, getStartuppath(),
				getStartupfilename(), getIsdocker(), getProjectname(), getClustername(), getNamespaceid(),
				getActivepool(), getDockerid(), "Start", containerNumber, getService()).equalsIgnoreCase("done"))
			finalString = "Couldn't Start " + getInstancename();

		Map<String, String> map = new HashMap<String, String>();
		map.put("message", finalString);

		if (!finalString.equalsIgnoreCase("Completed"))
			return new ResponseEntity<>(map, HttpStatus.FORBIDDEN);
		else
			return new ResponseEntity<>(map, HttpStatus.OK);

	}

	@Override
	public ResponseEntity<Object> checkComponent(String jsonLog) {

		ComponentResponse componentResponse = new ComponentResponse();

		List<Map<String, String>> myList = new ArrayList<Map<String, String>>();

		myList = new Gson().fromJson(jsonLog, new TypeToken<List<Map<String, String>>>() {
		}.getType());

		Map<String, String> myMap = new HashMap<String, String>();

		for (Map<String, String> dummy : myList) {
			myMap.put(dummy.get("key"), dummy.get("value"));
		}

		List<String> listOffKeys = new ArrayList<String>();

		for (String key : myMap.keySet()) {
			listOffKeys.add(key);
		}
		Map<String, Integer> map = new HashMap<String, Integer>();

		ServiceClass serviceClass = new ServiceClass();

		String testString = getInstanceref();

		String[] str = testString.split("_");

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(getUsername());
		byte[] decryptedPassword = decoder.decode(getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		for (String string : listOffKeys) {
			map.put(string, serviceClass.checkComponent(userName, getServiceip(), passWord, getServerlogpath(),
					getServerlogpattern(), myMap.get(string), Long.parseLong(str[str.length - 1])));
		}
		boolean b = false;

		if (!map.containsValue(0))
			b = true;
		componentResponse.setStarted(b);
		componentResponse.setStatus(map);

		return new ResponseEntity<>(componentResponse, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> copyLogs(String logLocation) {

		String completeString = "Completed";

		ComponentsRelated componentsRelated = new ComponentsRelated();

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(getUsername());
		byte[] decryptedPassword = decoder.decode(getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		if (getAccesslogpath() != null) {
			if (!componentsRelated.zipLogs(userName, getServiceip(), passWord, getAccesslogpath(),
					getAccesslogpattern(), "accesslog", getInstancename(), getIsdocker()).equalsIgnoreCase("done"))
				completeString = "Couldn't gzip " + getInstancename() + " accesslogs";
		}
		if (getServerlogpath() != null) {
			if (!componentsRelated.zipLogs(userName, getServiceip(), passWord, getServerlogpath(),
					getServerlogpattern(), "serverlog", getInstancename(), getIsdocker()).equalsIgnoreCase("done"))
				completeString = "Couldn't gzip " + getInstancename() + " serverlogs";
		}
		if (getCorelogpath() != null) {
			if (!componentsRelated.zipLogs(userName, getServiceip(), passWord, getCorelogpath(), getCorelogpattern(),
					"corelog", getInstancename(), getIsdocker()).equalsIgnoreCase("done"))
				completeString = "Couldn't gzip " + getInstancename() + " corelogs";
		}
		if (getJvmlogpath() != null) {
			if (!componentsRelated.zipLogs(userName, getServiceip(), passWord, getJvmlogpath(), getJvmlogpattern(),
					"jvmcorelog", getInstancename(), getIsdocker()).equalsIgnoreCase("done"))
				completeString = "Couldn't gzip " + getInstancename() + " jvmlogs";
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("message", completeString);

		if (!completeString.equalsIgnoreCase("Completed"))
			return new ResponseEntity<>(map, HttpStatus.FORBIDDEN);

		else {
			if (getAccesslogpath() != null) {
				componentsRelated.copyFile(userName, getServiceip(), passWord,
						getAccesslogpath() + "/" + getInstancename() + "_accesslog.tar.gz", logLocation);
			}
			if (getServerlogpath() != null) {
				componentsRelated.copyFile(userName, getServiceip(), passWord,
						getServerlogpath() + "/" + getInstancename() + "_serverlog.tar.gz", logLocation);
			}
			if (getCorelogpath() != null) {
				componentsRelated.copyFile(userName, getServiceip(), passWord,
						getCorelogpath() + "/" + getInstancename() + "_corelog.tar.gz", logLocation);
			}
			if (getJvmlogpath() != null) {
				componentsRelated.copyFile(userName, getServiceip(), passWord,
						getJvmlogpath() + "/" + getInstancename() + "_jvmcorelog.tar.gz", logLocation);
			}

			return new ResponseEntity<>(map, HttpStatus.OK);
		}
	}

	@Override
	public ResponseEntity<Object> logPurge() {

		ComponentsRelated componentsRelated = new ComponentsRelated();

		String temp = "Completed";

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(getUsername());
		byte[] decryptedPassword = decoder.decode(getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		if (getAccesslogpath() != null) {
			if (!componentsRelated.purgeLogs(userName, getServiceip(), passWord, getAccesslogpath(),
					getAccesslogpattern(), getIsdocker()).equalsIgnoreCase("done"))
				temp = "Couldn't purge " + getInstancename() + " accesslogs";
		}
		if (getServerlogpath() != null) {
			if (!componentsRelated.purgeLogs(userName, getServiceip(), passWord, getServerlogpath(),
					getServerlogpattern(), getIsdocker()).equalsIgnoreCase("done"))
				temp = "Couldn't purge " + getInstancename() + " serverlogs";
		}
		if (getCorelogpath() != null) {
			if (!componentsRelated
					.purgeLogs(userName, getServiceip(), passWord, getCorelogpath(), getCorelogpattern(), getIsdocker())
					.equalsIgnoreCase("done"))
				temp = "Couldn't purge " + getInstancename() + " corelogs";
		}
		if (getJvmlogpath() != null) {
			if (!componentsRelated
					.purgeLogs(userName, getServiceip(), passWord, getJvmlogpath(), getJvmlogpattern(), getIsdocker())
					.equalsIgnoreCase("done"))
				temp = "Couldn't purge " + getInstancename() + " jvmlogs";
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("message", temp);

		if (!temp.equalsIgnoreCase("Completed"))
			return new ResponseEntity<>(map, HttpStatus.FORBIDDEN);
		else
			return new ResponseEntity<>(map, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> stopComponent() {
		String finalString = "Completed";

		ComponentsRelated componentsRelated = new ComponentsRelated();

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(getUsername());
		byte[] decryptedPassword = decoder.decode(getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		if (!componentsRelated.startOrStopComponent(userName, getServiceip(), passWord, getStopfilepath(),
				getStopfilefilename(), getIsdocker(), getProjectname(), getClustername(), getNamespaceid(),
				getActivepool(), getDockerid(), "Stop", 0, getService()).equalsIgnoreCase("done"))
			finalString = "Couldn't Stop " + getInstancename();

		Map<String, String> map = new HashMap<String, String>();
		map.put("message", finalString);

		if (!finalString.equalsIgnoreCase("Completed"))
			return new ResponseEntity<>(map, HttpStatus.FORBIDDEN);
		else
			return new ResponseEntity<>(map, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> reStartComponent() {
		String finalString = "Completed";

		ComponentsRelated componentsRelated = new ComponentsRelated();

		Decoder decoder = Base64.getDecoder();
		byte[] decryptedUserName = decoder.decode(getUsername());
		byte[] decryptedPassword = decoder.decode(getPassword());

		String userName = new String(decryptedUserName);
		String passWord = new String(decryptedPassword);

		if (!componentsRelated.startOrStopComponent(userName, getServiceip(), passWord, getStopfilepath(),
				getStopfilefilename(), getIsdocker(), getProjectname(), getClustername(), getNamespaceid(),
				getActivepool(), getDockerid(), "Restart", 0, getService()).equalsIgnoreCase("done"))
			finalString = "Couldn't Restart " + getInstancename();

		Map<String, String> map = new HashMap<String, String>();
		map.put("message", finalString);

		if (!finalString.equalsIgnoreCase("Completed"))
			return new ResponseEntity<>(map, HttpStatus.FORBIDDEN);
		else
			return new ResponseEntity<>(map, HttpStatus.OK);
	}

}
