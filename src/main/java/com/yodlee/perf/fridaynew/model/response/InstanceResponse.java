/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.response;

//@ApiModel(description = "Details about the Instance")
public class InstanceResponse {

	// @ApiModelProperty(notes = "The instance ref for running the test")
	private String instanceref;
	// @ApiModelProperty(notes = "The Service IP of the instance")
	private String serviceip;
	// @ApiModelProperty(notes = "The Service PORT of the instance")
	private Long serviceport;
	// @ApiModelProperty(notes = "The VIP IP of the instance")
	private String vipip;
	// @ApiModelProperty(notes = "The VIP PORT of the instance")
	private Long vipport;
	// @ApiModelProperty(notes = "The PROTOCOL of the instance")
	private String protocol;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getInstanceref() {
		return instanceref;
	}

	public void setInstanceref(String instanceref) {
		this.instanceref = instanceref;
	}

	public String getServiceip() {
		return serviceip;
	}

	public void setServiceip(String serviceip) {
		this.serviceip = serviceip;
	}

	public Long getServiceport() {
		return serviceport;
	}

	public void setServiceport(Long serviceport) {
		this.serviceport = serviceport;
	}

	public String getVipip() {
		return vipip;
	}

	public void setVipip(String vipip) {
		this.vipip = vipip;
	}

	public Long getVipport() {
		return vipport;
	}

	public void setVipport(Long vipport) {
		this.vipport = vipport;
	}

}
