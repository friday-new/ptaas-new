/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class References {

	@Id
	private Integer refid;
	private String name;
	private String iscomponent;
	private String componentrelated;
	private Long configmoduleid;

	public Long getConfigmoduleid() {
		return configmoduleid;
	}

	public void setConfigmoduleid(Long configmoduleid) {
		this.configmoduleid = configmoduleid;
	}

	public String getIscomponent() {
		return iscomponent;
	}

	public void setIscomponent(String iscomponent) {
		this.iscomponent = iscomponent;
	}

	public String getComponentrelated() {
		return componentrelated;
	}

	public void setComponentrelated(String componentrelated) {
		this.componentrelated = componentrelated;
	}

	public Integer getRefid() {
		return refid;
	}

	public void setRefid(Integer refid) {
		this.refid = refid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
