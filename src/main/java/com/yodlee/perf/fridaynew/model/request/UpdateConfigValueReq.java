/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.request;

import javax.validation.constraints.NotNull;

//@ApiModel(description = "Update Config Value Request is used to change the Config DB values")
public class UpdateConfigValueReq {

	// @ApiModelProperty(notes = "config Group Id of which the value to be changed")
	@NotNull
	private Long configGroupId;
	// @ApiModelProperty(notes = "config key of which the value to be changed")
	@NotNull
	private String configKey;
	// @ApiModelProperty(notes = "New config value which needs to be changed")
	@NotNull
	private String configValue;
	// @ApiModelProperty(notes = "Config db reference")
	@NotNull
	private String oltpRef;

	public Long getConfigGroupId() {
		return configGroupId;
	}

	public void setConfigGroupId(Long configGroupId) {
		this.configGroupId = configGroupId;
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getOltpRef() {
		return oltpRef;
	}

	public void setOltpRef(String oltpRef) {
		this.oltpRef = oltpRef;
	}

}
