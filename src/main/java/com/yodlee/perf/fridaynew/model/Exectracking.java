/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Exectracking {

	@Id
	private String exectrackingid;
	private String execfolder;
	private Integer isjtlcopied;
	private String jmeterref;
	private String jtllocation;
	private Long created;
	private String testcaseid;

	public String getTestcaseid() {
		return testcaseid;
	}

	public void setTestcaseid(String testcaseid) {
		this.testcaseid = testcaseid;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Integer getIsjtlcopied() {
		return isjtlcopied;
	}

	public void setIsjtlcopied(Integer isjtlcopied) {
		this.isjtlcopied = isjtlcopied;
	}

	public String getJmeterref() {
		return jmeterref;
	}

	public void setJmeterref(String jmeterref) {
		this.jmeterref = jmeterref;
	}

	public String getJtllocation() {
		return jtllocation;
	}

	public void setJtllocation(String jtllocation) {
		this.jtllocation = jtllocation;
	}

	public String getExectrackingid() {
		return exectrackingid;
	}

	public void setExectrackingid(String exectrackingid) {
		this.exectrackingid = exectrackingid;
	}

	public String getExecfolder() {
		return execfolder;
	}

	public void setExecfolder(String execfolder) {
		this.execfolder = execfolder;
	}

}
