/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.request;

import javax.validation.constraints.NotNull;

//@ApiModel(description = "Copy Logs Request object is used for copying the logs to the execution folder")
public class CopyLogsRequest {

	// @ApiModelProperty(notes = "Instance Reference for copying the logs")
	@NotNull
	private String instanceRef;
	// @ApiModelProperty(notes = "Log location to copy the logs")
	@NotNull
	private String logLocation;

	public String getInstanceRef() {
		return instanceRef;
	}

	public void setInstanceRef(String instanceRef) {
		this.instanceRef = instanceRef;
	}

	public String getLogLocation() {
		return logLocation;
	}

	public void setLogLocation(String logLocation) {
		this.logLocation = logLocation;
	}

}
