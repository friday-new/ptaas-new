/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

public class ParamKey {

	private Long paramKeyId;
	private String paramKeyName;
	private String defaultValue;
	private Long version;
	private Integer isSubbrandable;
	private String bundleName;
	private String description;

	public Long getParamKeyId() {
		return paramKeyId;
	}

	public void setParamKeyId(Long paramKeyId) {
		this.paramKeyId = paramKeyId;
	}

	public String getParamKeyName() {
		return paramKeyName;
	}

	public void setParamKeyName(String paramKeyName) {
		this.paramKeyName = paramKeyName;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Integer getIsSubbrandable() {
		return isSubbrandable;
	}

	public void setIsSubbrandable(Integer isSubbrandable) {
		this.isSubbrandable = isSubbrandable;
	}

	public String getBundleName() {
		return bundleName;
	}

	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
