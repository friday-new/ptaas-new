/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Testcaseverdict {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TESTCASEVERDICTID_PK_SEQ")
	@SequenceGenerator(sequenceName = "TESTCASEVERDICTID_PK_SEQ", allocationSize = 1, name = "TESTCASEVERDICTID_PK_SEQ")
	private Long testcaseverdictid;
	private String testcaseid;
	private Long active;
	private String branch;
	private String instancename;
	private Long created;
	private Long lastupdated;
	private String vmcpuverdict;
	private String memoryverdict;
	private String processcpuverdict;
	private String gcthroughputverdict;
	private String pausetimeverdict;
	private String gccountverdict;
	private String heapusedverdict;
	private String tpsverdict;
	private String description;
	private Long interval;
	private String build;

	public Long getInterval() {
		return interval;
	}

	public void setInterval(Long interval) {
		this.interval = interval;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getTestcaseverdictid() {
		return testcaseverdictid;
	}

	public void setTestcaseverdictid(Long testcaseverdictid) {
		this.testcaseverdictid = testcaseverdictid;
	}

	public String getTestcaseid() {
		return testcaseid;
	}

	public void setTestcaseid(String testcaseid) {
		this.testcaseid = testcaseid;
	}

	public Long getActive() {
		return active;
	}

	public void setActive(Long active) {
		this.active = active;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getInstancename() {
		return instancename;
	}

	public void setInstancename(String instancename) {
		this.instancename = instancename;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Long getLastupdated() {
		return lastupdated;
	}

	public void setLastupdated(Long lastupdated) {
		this.lastupdated = lastupdated;
	}

	public String getVmcpuverdict() {
		return vmcpuverdict;
	}

	public void setVmcpuverdict(String vmcpuverdict) {
		this.vmcpuverdict = vmcpuverdict;
	}

	public String getMemoryverdict() {
		return memoryverdict;
	}

	public void setMemoryverdict(String memoryverdict) {
		this.memoryverdict = memoryverdict;
	}

	public String getProcesscpuverdict() {
		return processcpuverdict;
	}

	public void setProcesscpuverdict(String processcpuverdict) {
		this.processcpuverdict = processcpuverdict;
	}

	public String getGcthroughputverdict() {
		return gcthroughputverdict;
	}

	public void setGcthroughputverdict(String gcthroughputverdict) {
		this.gcthroughputverdict = gcthroughputverdict;
	}

	public String getPausetimeverdict() {
		return pausetimeverdict;
	}

	public void setPausetimeverdict(String pausetimeverdict) {
		this.pausetimeverdict = pausetimeverdict;
	}

	public String getGccountverdict() {
		return gccountverdict;
	}

	public void setGccountverdict(String gccountverdict) {
		this.gccountverdict = gccountverdict;
	}

	public String getHeapusedverdict() {
		return heapusedverdict;
	}

	public void setHeapusedverdict(String heapusedverdict) {
		this.heapusedverdict = heapusedverdict;
	}

	public String getTpsverdict() {
		return tpsverdict;
	}

	public void setTpsverdict(String tpsverdict) {
		this.tpsverdict = tpsverdict;
	}

	public String getBuild() {
		return build;
	}

	public void setBuild(String build) {
		this.build = build;
	}

}
