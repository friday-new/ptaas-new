/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.lang.Nullable;

@Entity
@Table(name = "update_feature_key")
public class UpdateFeatureKey {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UPDATE_FEATURE_KEY_PK_SEQ")
	@SequenceGenerator(sequenceName = "UPDATE_FEATURE_KEY_PK_SEQ", allocationSize = 1, name = "UPDATE_FEATURE_KEY_PK_SEQ")
	@Column(name = "update_feature_key_id")
	private Long updateFeatureKeyId;
	@Column(name = "instance_ref")
	private String instanceRef;
	@Column(name = "table_name")
	private String tableName;
	@Nullable
	@Column(name = "value")
	private String value;
	@Column(name = "key_id")
	private Long keyId;
	@Column(name = "update_time")
	private Long updateTime;
	@Column(name = "revert_time")
	private Long revertTime;

	public Long getUpdateFeatureKeyId() {
		return updateFeatureKeyId;
	}

	public void setUpdateFeatureKeyId(Long updateFeatureKeyId) {
		this.updateFeatureKeyId = updateFeatureKeyId;
	}

	public String getInstanceRef() {
		return instanceRef;
	}

	public void setInstanceRef(String instanceRef) {
		this.instanceRef = instanceRef;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getKeyId() {
		return keyId;
	}

	public void setKeyId(Long keyId) {
		this.keyId = keyId;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	public Long getRevertTime() {
		return revertTime;
	}

	public void setRevertTime(Long revertTime) {
		this.revertTime = revertTime;
	}

}
