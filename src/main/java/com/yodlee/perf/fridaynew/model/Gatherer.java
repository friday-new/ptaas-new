/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

@Entity
public class Gatherer implements InstanceRelated {
	@Id
	private Integer gid;
	private String gobjname;
	private String gipaddr;
	private Long gport;
	private String guser;
	private String gpasswd;
	private String gpath;
	@Nullable
	private String gref;
	private String ginuse;
	private String gconfpath;

	public String getGconfpath() {
		return gconfpath;
	}

	public void setGconfpath(String gconfpath) {
		this.gconfpath = gconfpath;
	}

	public Integer getGid() {
		return gid;
	}

	public void setGid(Integer gid) {
		this.gid = gid;
	}

	public String getGobjname() {
		return gobjname;
	}

	public void setGobjname(String gobjname) {
		this.gobjname = gobjname;
	}

	public String getGipaddr() {
		return gipaddr;
	}

	public void setGipaddr(String gipaddr) {
		this.gipaddr = gipaddr;
	}

	public Long getGport() {
		return gport;
	}

	public void setGport(Long gport) {
		this.gport = gport;
	}

	public String getGuser() {
		return guser;
	}

	public void setGuser(String guser) {
		this.guser = guser;
	}

	public String getGpasswd() {
		return gpasswd;
	}

	public void setGpasswd(String gpasswd) {
		this.gpasswd = gpasswd;
	}

	public String getGpath() {
		return gpath;
	}

	public void setGpath(String gpath) {
		this.gpath = gpath;
	}

	public String getGref() {
		return gref;
	}

	public void setGref(String gref) {
		this.gref = gref;
	}

	public String getGinuse() {
		return ginuse;
	}

	public void setGinuse(String ginuse) {
		this.ginuse = ginuse;
	}

	@Override
	public ResponseEntity<Object> startComponent(Integer containerNumber) {

		try {
			Runtime.getRuntime().exec("/backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/GathererStartOrStop.sh "
					+ getGuser() + " " + getGipaddr() + " " + getGpasswd() + " " + getGpath() + " start_gatherer.sh");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("message", "Completed");
		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> copyLogs(String logLocation) {
		return null;
	}

	@Override
	public ResponseEntity<Object> logPurge() {
		return null;
	}

	@Override
	public ResponseEntity<Object> stopComponent() {

		try {
			Runtime.getRuntime().exec("/backup/Users/caradhya/RootUserFiles/CacheRefreshFiles/GathererStartOrStop.sh "
					+ getGuser() + " " + getGipaddr() + " " + getGpasswd() + " " + getGpath() + " kill_gatherer.sh");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("message", "Completed");
		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> checkComponent(String jsonLog) {
		return null;
	}

	@Override
	public ResponseEntity<Object> reStartComponent() {
		return null;
	}

}
