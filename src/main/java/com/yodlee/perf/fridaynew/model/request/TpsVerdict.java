/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.model.request;

import java.util.List;

public class TpsVerdict {

	private List<String> tps;
	private List<String> avgRspTime;

	public List<String> getTps() {
		return tps;
	}

	public void setTps(List<String> tps) {
		this.tps = tps;
	}

	public List<String> getAvgRspTime() {
		return avgRspTime;
	}

	public void setAvgRspTime(List<String> avgRspTime) {
		this.avgRspTime = avgRspTime;
	}

}
