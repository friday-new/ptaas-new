/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */

package com.yodlee.perf.fridaynew.configuration;

import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

@Configuration
//@formatter:off
@OpenAPIDefinition(info = @Info(
								title = "PTAAS Project for Perf", 
								version = "3.0", 
								description = "PTAAS stands for Performance Test As A Service which is used to execute the "
										+ "testcases by doing the pre configuration before test and collect all the perf realted data", 
								termsOfService = "https://sp.corp.yodlee.com/op/ops/Performance/TrainingMaterial/Documents%20Repository/"
										+ "PTSM/PTSM-User-Manual.docx?csf=1&e=yFRtx2", 
								license = @License(
										name = "Apache2.0", 
										url = "https://www.apache.org/licenses/LICENSE-2.0"), 
								contact = @Contact(
										email = "ltirounavoucarassou@yodlee.com", 
										name = "Latchoumanan Tirounavoucarassou", 
										url = "https://sp.corp.yodlee.com/op/ops/Performance/default.aspx")))
//@formatter:on
public class SwaggerConfig {

}