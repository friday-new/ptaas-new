/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */

package com.yodlee.perf.fridaynew.configuration;

import com.yodlee.perf.fridaynew.controller.ControllerClass;
import com.yodlee.perf.fridaynew.service.ServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class SchedularConfiguration {

	@Autowired
	public ServiceInterface serviceInterface;

	@Scheduled(cron = "0 0 */1 * * *")
	void deleteJenkinJobs() {

		ControllerClass.setMetod(serviceInterface.getAllComponents());
	}

}
