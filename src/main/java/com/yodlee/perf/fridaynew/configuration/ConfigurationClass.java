/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */

package com.yodlee.perf.fridaynew.configuration;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@Configuration
@RefreshScope
public class ConfigurationClass {

	@Value("${Portfolio}")
	private List<String> Portfolio;
	@Value("${Version}")
	private List<String> Version;
	@Value("${PrintLog}")
	private boolean PrintLog;

	public List<String> getPortfolio() {
		return Portfolio;
	}

	public void setPortfolio(List<String> portfolio) {
		Portfolio = portfolio;
	}

	public List<String> getVersion() {
		return Version;
	}

	public void setVersion(List<String> version) {
		Version = version;
	}

	public boolean isPrintLog() {
		return PrintLog;
	}

	public void setPrintLog(boolean printLog) {
		PrintLog = printLog;
	}

}
