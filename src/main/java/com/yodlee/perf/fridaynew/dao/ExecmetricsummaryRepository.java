/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.dao;

import java.util.List;

import com.yodlee.perf.fridaynew.model.Execmetricsummary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExecmetricsummaryRepository extends JpaRepository<Execmetricsummary, Long> {

	List<Execmetricsummary> findByTestcaseidIgnoreCaseAndExectrackingIgnoreCaseAndObjectnameIgnoreCase(
			String testcaseid, String exectracking, String objectname);

	Execmetricsummary findByTestcaseidIgnoreCaseAndExectrackingIgnoreCaseAndObjectnameIgnoreCaseAndInterval(
			String testcaseid, String exectracking, String objectname, Long interval);

	List<Execmetricsummary> findByExectrackingIgnoreCase(String exectracking);

}
