/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.dao;

import java.util.List;

import com.yodlee.perf.fridaynew.model.Testcaseverdict;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestcaseverdictRepository extends JpaRepository<Testcaseverdict, Long> {

	Testcaseverdict findByTestcaseidIgnoreCaseAndInstancenameAndIntervalAndActiveAndBranchIgnoreCase(String testcaseid,
			String instancename, Long interval, Long active, String branch);

	List<Testcaseverdict> findByTestcaseidIgnoreCaseAndInstancenameAndActive(String testcaseid, String instancename,
			Long active);

	List<Testcaseverdict> findByTestcaseidIgnoreCaseAndActive(String testcaseid, Long active);

}
