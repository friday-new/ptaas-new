/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.dao;

import java.util.List;

import com.yodlee.perf.fridaynew.model.Oltp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OltpRepository extends JpaRepository<Oltp, Integer> {

	Oltp findByoltpname(String oltpname);

	Oltp findByoltpref(String oltpref);

	List<Oltp> findByoltpschema(String oltpschema);

	List<Oltp> findByOltpipaddrAndOltpportAndProjectnameAndOltpusernameAndIsassignedAndOltpinuse(String oltpipaddr,
			Integer oltpport, String projectname, String oltpusername, String isassigned, String oltpinuse);

}
