/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.dao;

import com.yodlee.perf.fridaynew.model.Exectracking;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExectrackingRepository extends JpaRepository<Exectracking, String> {

	Exectracking findByExecfolderContainingIgnoreCase(String execfolder);

}
