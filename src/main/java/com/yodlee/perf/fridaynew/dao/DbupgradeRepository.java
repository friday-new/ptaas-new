/*
 * Copyright (c) 2021 Yodlee, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information of Yodlee, Inc. 
 * Use is subject to license terms
 * Created on Fri Jun 04 2021
 * author : caradhya
 * 
 */
package com.yodlee.perf.fridaynew.dao;

import com.yodlee.perf.fridaynew.model.Dbupgrade;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DbupgradeRepository extends JpaRepository<Dbupgrade, Long> {

	Dbupgrade findByDbname(String dbname);

	Dbupgrade findByObjectref(String objectref);

}
